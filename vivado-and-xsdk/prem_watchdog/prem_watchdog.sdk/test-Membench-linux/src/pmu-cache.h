/*
 * pmu-cache.h
 *
 *  Created on: May 7, 2018
 *      Author: barysmax
 */

#ifndef SRC_PMU_CACHE_H_
#define SRC_PMU_CACHE_H_

#include <stdint.h>

void pmu_init(uint32_t cpu);
void pmu_cachemiss_reset(uint32_t cpu);
uint32_t pmu_cachemiss_read(uint32_t cpu);

#endif /* SRC_PMU_CACHE_H_ */
