/*
 * pmu-cache.c
 *
 *  Created on: May 7, 2018
 *      Author: barysmax
 */

#include "pmu-cache.h"
#include "coresight-addrs.h"

#include "csaccess.h"
#include "cs_reg_access.h"
#include "csregisters.h"

cs_device_t _pmu[4];

void pmu_init(uint32_t cpu)
{
	_pmu[cpu] = cs_device_register(pmu_addr[cpu]);
	cs_pmu_t pmu_s;
	pmu_s.eventtypes[0] = 0x17;
	pmu_s.mask = 0x00000001;
	cs_pmu_write_status(_pmu[cpu], CS_PMU_EVENTTYPES | CS_PMU_ENABLE, &pmu_s);
}

void pmu_cachemiss_reset(uint32_t cpu)
{
	cs_device_write_only(_pmu[cpu], CS_PMEVCNTR(0,0), 0U);
}

uint32_t pmu_cachemiss_read(uint32_t cpu)
{
	return cs_device_read(_pmu[0], CS_PMEVCNTR32(0));
}



