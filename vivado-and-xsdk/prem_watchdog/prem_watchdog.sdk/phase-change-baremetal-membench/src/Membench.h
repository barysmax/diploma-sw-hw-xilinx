/*
 * Membench.h
 *
 *  Created on: Apr 25, 2018
 *      Author: barysmax
 */

#ifndef SRC_MEMBENCH_H_
#define SRC_MEMBENCH_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define MAX_WSS (4*0x100000)

typedef struct
{
	size_t count;
	size_t bits;
}cpu_set_t;

struct cfg {
	bool sequential;
	unsigned size;
	unsigned num_threads;
	unsigned read_count;
	cpu_set_t cpu_set;
	bool write;
	unsigned ofs;
	bool use_cycles; /* instead of ns */
	unsigned repeats;
	bool cache_miss;
	bool cache_miss_pl;
};

void do_benchmark(struct cfg * cfg);

#endif /* SRC_MEMBENCH_H_ */
