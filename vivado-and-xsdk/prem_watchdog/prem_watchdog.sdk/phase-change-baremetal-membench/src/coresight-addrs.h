/*
 * coresight-addrs.h
 *
 *  Created on: May 7, 2018
 *      Author: barysmax
 */

#ifndef SRC_CORESIGHT_ADDRS_H_
#define SRC_CORESIGHT_ADDRS_H_

static const uint64_t pmu_addr[4] = {
	0xFEC30000,
	0xFED30000,
	0xFEE30000,
	0xFEF30000
};

static const uint64_t prem_count_addr[1] = {
	0xA0000000
};

static const uint64_t etm_addr[4] = {
	0xFEC40000,
	0xFED40000,
	0xFEE40000,
	0xFEF40000
};

static const uint64_t cti_a53_addr[4] = {
	0xFEC20000,
	0xFED20000,
	0xFEE20000,
	0xFEF20000,
};

static const uint64_t cti_soc1_addr = 0xFE9A0000;

#endif /* SRC_CORESIGHT_ADDRS_H_ */
