/*
 * pl_counter.h
 *
 *  Created on: May 7, 2018
 *      Author: barysmax
 */

#ifndef SRC_PREM_COUNTER_H_
#define SRC_PREM_COUNTER_H_

#include <stdint.h>

#define PMUBUS_EVENT (21U + 4U)

#define PREM_PHASE_CONF 0b00
#define PREM_PHASE_PREF 0b01
#define PREM_PHASE_COMP 0b10
#define PREM_PHASE_WB   0b11

#define ETM_EVENTS_BUFF_NUM 8U

typedef uint32_t prem_phase_t;

typedef struct prem_conf {
	uint32_t lim_prefetch;
	uint32_t lim_compute;
	uint32_t lim_writeback;
} prem_conf_s;

void __attribute__((optimize("O0"))) prem_configure(uint32_t cpu, prem_conf_s * config);
void prem_set_phase(uint32_t cpu, prem_phase_t phase);
void prem_print_state(uint32_t cpu);

void cs_prem_count_init();
void cs_prem_count_percpu_init(uint32_t cpu);
void _deprecated_pl_count_reset(uint32_t cpu);
uint32_t _deprecated_pl_count_read(uint32_t cpu);

#endif /* SRC_PREM_COUNTER_H_ */
