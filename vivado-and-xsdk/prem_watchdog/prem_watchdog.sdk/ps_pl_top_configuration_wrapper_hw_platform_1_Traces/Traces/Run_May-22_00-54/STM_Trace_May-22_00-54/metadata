/* CTF */
typealias integer {
    size = 32;
} := uint32_t;
trace {
    major = 1;
    minor = 8;
    byte_order = be;
    packet.header := struct {
        uint32_t magic;
        uint32_t stream_id;
    };
};
env {
	domain = "freertos";
};
clock {
    name = my_clock;
    freq = 1000000000;
    offset = 1526940000000000000;
};
typealias integer {
    size = 64;
    map = clock.my_clock.value;
} := my_clock_int_t;

stream {
    id = 0;
    packet.context := struct {
        uint32_t packet_size;
        uint32_t content_size;
    };
      event.header := struct {
		uint32_t id;
        my_clock_int_t timestamp;
    };
};event {
    id = 0;
    name = task_switched_in;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
    };
};
event {
    id = 1;
    name = increase_tick_count;
    fields := struct {
        string master;
        string channel;
        string count;
    };
};
event {
    id = 2;
    name = low_power_idle_begin;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 3;
    name = low_power_idle_end;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 4;
    name = task_switched_out;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
    };
};
event {
    id = 5;
    name = task_priority_inherit;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
        string inherited_priority;
    };
};
event {
    id = 6;
    name = task_priority_disinherit;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
        string original_priority;
    };
};
event {
    id = 7;
    name = blocking_on_queue_receive;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 8;
    name = blocking_on_queue_send;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 9;
    name = moved_task_to_ready_state;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
    };
};
event {
    id = 10;
    name = queue_create;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
        string queue_size;
    };
};
event {
    id = 11;
    name = queue_create_failed;
    fields := struct {
        string master;
        string channel;
        string queue_type;
    };
};
event {
    id = 12;
    name = create_mutex;
    fields := struct {
        string master;
        string channel;
        string mutex_pointer;
        string number;
    };
};
event {
    id = 13;
    name = create_mutex_failed;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 14;
    name = give_mutex_recursive;
    fields := struct {
        string master;
        string channel;
        string mutex_pointer;
    };
};
event {
    id = 15;
    name = give_mutex_recursive_failed;
    fields := struct {
        string master;
        string channel;
        string mutex_pointer;
    };
};
event {
    id = 16;
    name = take_mutex_recursive;
    fields := struct {
        string master;
        string channel;
        string mutex_pointer;
    };
};
event {
    id = 17;
    name = take_mutex_recursive_failed;
    fields := struct {
        string master;
        string channel;
        string mutex_pointer;
    };
};
event {
    id = 18;
    name = create_counting_semaphore;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 19;
    name = create_counting_semaphore_failed;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 20;
    name = queue_send;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 21;
    name = queue_send_failed;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 22;
    name = queue_receive;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 23;
    name = queue_peek;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 24;
    name = queue_peek_from_isr;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 25;
    name = queue_receive_failed;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 26;
    name = queue_send_from_isr;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 27;
    name = queue_send_from_isr_failed;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 28;
    name = queue_receive_from_isr;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 29;
    name = queue_receive_from_isr_failed;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 30;
    name = queue_peek_from_isr_failed;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 31;
    name = queue_delete;
    fields := struct {
        string master;
        string channel;
        string queue_pointer;
    };
};
event {
    id = 32;
    name = task_create;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
        string priority;
        string name;
    };
};
event {
    id = 33;
    name = task_create_failed;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 34;
    name = task_delete;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
    };
};
event {
    id = 35;
    name = task_delay_until;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 36;
    name = task_delay;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 37;
    name = task_priority_set;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
        string new_priority;
    };
};
event {
    id = 38;
    name = task_suspend;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
    };
};
event {
    id = 39;
    name = task_resume;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
    };
};
event {
    id = 40;
    name = task_resume_from_isr;
    fields := struct {
        string master;
        string channel;
        string task_pointer;
    };
};
event {
    id = 41;
    name = task_increment_tick;
    fields := struct {
        string master;
        string channel;
        string tick_count;
    };
};
event {
    id = 42;
    name = timer_create;
    fields := struct {
        string master;
        string channel;
        string timer_pointer;
        string timer_number;
    };
};
event {
    id = 43;
    name = timer_create_failed;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 44;
    name = timer_command_send;
    fields := struct {
        string master;
        string channel;
        string timer_pointer;
        string message_id;
    };
};
event {
    id = 45;
    name = timer_expired;
    fields := struct {
        string master;
        string channel;
        string timer_pointer;
    };
};
event {
    id = 46;
    name = timer_command_received;
    fields := struct {
        string master;
        string channel;
        string timer_pointer;
        string message_id;
    };
};
event {
    id = 47;
    name = malloc;
    fields := struct {
        string master;
        string channel;
        string address;
        string size;
    };
};
event {
    id = 48;
    name = free;
    fields := struct {
        string master;
        string channel;
        string address;
        string size;
    };
};
event {
    id = 49;
    name = event_group_create;
    fields := struct {
        string master;
        string channel;
        string event_group_pointer;
    };
};
event {
    id = 50;
    name = event_group_create_failed;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 51;
    name = event_group_sync_block;
    fields := struct {
        string master;
        string channel;
        string event_group_pointer;
    };
};
event {
    id = 52;
    name = event_group_sync_end;
    fields := struct {
        string master;
        string channel;
        string event_group_pointer;
    };
};
event {
    id = 53;
    name = event_group_wait_bits_block;
    fields := struct {
        string master;
        string channel;
        string event_group_pointer;
    };
};
event {
    id = 54;
    name = event_group_wait_bits_end;
    fields := struct {
        string master;
        string channel;
        string event_group_pointer;
    };
};
event {
    id = 55;
    name = event_group_clear_bits;
    fields := struct {
        string master;
        string channel;
        string event_group_pointer;
        string bits_to_clear;
    };
};
event {
    id = 56;
    name = event_group_clear_bits_from_isr;
    fields := struct {
        string master;
        string channel;
        string event_group_pointer;
        string bits_to_clear;
    };
};
event {
    id = 57;
    name = event_group_set_bits;
    fields := struct {
        string master;
        string channel;
        string event_group_pointer;
        string bits_to_clear;
    };
};
event {
    id = 58;
    name = event_group_set_bits_from_isr;
    fields := struct {
        string master;
        string channel;
        string event_group_pointer;
        string bits_to_clear;
    };
};
event {
    id = 59;
    name = event_group_delete;
    fields := struct {
        string master;
        string channel;
        string event_group_pointer;
    };
};
event {
    id = 60;
    name = pend_func_call;
    fields := struct {
        string master;
        string channel;
        string function_pointer;
        string parameter;
    };
};
event {
    id = 61;
    name = pend_func_call_from_isr;
    fields := struct {
        string master;
        string channel;
        string function_pointer;
        string queue_name;
    };
};
event {
    id = 62;
    name = queue_registry_add;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 63;
    name = task_notify_take_block;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 64;
    name = task_notify_take;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 65;
    name = task_notify_wait_block;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 66;
    name = task_notify_wait;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 67;
    name = task_notify;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 68;
    name = task_notify_from_isr;
    fields := struct {
        string master;
        string channel;
    };
};
event {
    id = 69;
    name = task_notify_give_from_isr;
    fields := struct {
        string master;
        string channel;
    };
};
