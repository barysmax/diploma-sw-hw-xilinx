library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;

entity prem_mem_cntr_v1_0_S00_AXI is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Width of S_AXI data bus
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		-- Width of S_AXI address bus
		C_S_AXI_ADDR_WIDTH	: integer	:= 4
	);
	port (
		-- Users to add ports here
        U_PMU_TRIGIN    : in std_logic;
        U_PMU_TRIGOUT   : out std_logic;
        U_LIMIT_IRQ     : out std_logic;
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global Clock Signal
		S_AXI_ACLK	: in std_logic;
		-- Global Reset Signal. This Signal is Active LOW
		S_AXI_ARESETN	: in std_logic;
		-- Write address (issued by master, acceped by Slave)
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Write channel Protection type. This signal indicates the
    		-- privilege and security level of the transaction, and whether
    		-- the transaction is a data access or an instruction access.
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		-- Write address valid. This signal indicates that the master signaling
    		-- valid write address and control information.
		S_AXI_AWVALID	: in std_logic;
		-- Write address ready. This signal indicates that the slave is ready
    		-- to accept an address and associated control signals.
		S_AXI_AWREADY	: out std_logic;
		-- Write data (issued by master, acceped by Slave)
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Write strobes. This signal indicates which byte lanes hold
    		-- valid data. There is one write strobe bit for each eight
    		-- bits of the write data bus.
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		-- Write valid. This signal indicates that valid write
    		-- data and strobes are available.
		S_AXI_WVALID	: in std_logic;
		-- Write ready. This signal indicates that the slave
    		-- can accept the write data.
		S_AXI_WREADY	: out std_logic;
		-- Write response. This signal indicates the status
    		-- of the write transaction.
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		-- Write response valid. This signal indicates that the channel
    		-- is signaling a valid write response.
		S_AXI_BVALID	: out std_logic;
		-- Response ready. This signal indicates that the master
    		-- can accept a write response.
		S_AXI_BREADY	: in std_logic;
		-- Read address (issued by master, acceped by Slave)
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Protection type. This signal indicates the privilege
    		-- and security level of the transaction, and whether the
    		-- transaction is a data access or an instruction access.
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		-- Read address valid. This signal indicates that the channel
    		-- is signaling valid read address and control information.
		S_AXI_ARVALID	: in std_logic;
		-- Read address ready. This signal indicates that the slave is
    		-- ready to accept an address and associated control signals.
		S_AXI_ARREADY	: out std_logic;
		-- Read data (issued by slave)
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Read response. This signal indicates the status of the
    		-- read transfer.
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		-- Read valid. This signal indicates that the channel is
    		-- signaling the required read data.
		S_AXI_RVALID	: out std_logic;
		-- Read ready. This signal indicates that the master can
    		-- accept the read data and response information.
		S_AXI_RREADY	: in std_logic
	);
end prem_mem_cntr_v1_0_S00_AXI;

architecture arch_imp of prem_mem_cntr_v1_0_S00_AXI is

	-- AXI4LITE signals
	signal axi_awaddr	: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_awready	: std_logic;
	signal axi_wready	: std_logic;
	signal axi_bresp	: std_logic_vector(1 downto 0);
	signal axi_bvalid	: std_logic;
	signal axi_araddr	: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_arready	: std_logic;
	signal axi_rdata	: std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal axi_rresp	: std_logic_vector(1 downto 0);
	signal axi_rvalid	: std_logic;

	-- Example-specific design signals
	-- local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	-- ADDR_LSB is used for addressing 32/64 bit registers/memories
	-- ADDR_LSB = 2 for 32 bits (n downto 2)
	-- ADDR_LSB = 3 for 64 bits (n downto 3)
	constant ADDR_LSB  : integer := (C_S_AXI_DATA_WIDTH/32)+ 1;
	constant OPT_MEM_ADDR_BITS : integer := 1;
	------------------------------------------------
	---- Signals for user logic register space example
--  	signal counter_pref_val	 : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '1');
--	signal counter_pref_en	 : std_logic;
--    signal counter_pref_rst	 : std_logic;


--    signal counter_comp_val  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0):= (others => '1');
--    signal counter_comp_en	 : std_logic;
--    signal counter_comp_rst  : std_logic;


--    signal counter_wb_val    : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0):= (others => '1');
--	signal counter_wb_en	 : std_logic;
--    signal counter_wb_rst    : std_logic;

--    signal counters_allow_rst : std_logic;

    constant PREF_CNTR: integer := 0;
    constant COMP_CNTR: integer := 1;
    constant WB_CNTR: integer   := 2;
    signal counter_en : std_logic_vector(2 downto 0);
	--------------------------------------------------
	---- Number of Slave Registers 4
	signal phase_reg	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal counter_pref_reg	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal counter_comp_reg	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal counter_wb_reg	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg_rden	: std_logic;
	signal slv_reg_wren	: std_logic;
	signal reg_data_out	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal byte_index	: integer;
	signal aw_en	: std_logic;

begin
	-- I/O Connections assignments

	S_AXI_AWREADY	<= axi_awready;
	S_AXI_WREADY	<= axi_wready;
	S_AXI_BRESP	<= axi_bresp;
	S_AXI_BVALID	<= axi_bvalid;
	S_AXI_ARREADY	<= axi_arready;
	S_AXI_RDATA	<= axi_rdata;
	S_AXI_RRESP	<= axi_rresp;
	S_AXI_RVALID	<= axi_rvalid;
	-- Implement axi_awready generation
	-- axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	-- de-asserted when reset is low.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      axi_awready <= '0';
	      aw_en <= '1';
	    else
	      if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1' and aw_en = '1') then
	        -- slave is ready to accept write address when
	        -- there is a valid write address and write data
	        -- on the write address and data bus. This design
	        -- expects no outstanding transactions.
	        axi_awready <= '1';
	        elsif (S_AXI_BREADY = '1' and axi_bvalid = '1') then
	            aw_en <= '1';
	        	axi_awready <= '0';
	      else
	        axi_awready <= '0';
	      end if;
	    end if;
	  end if;
	end process;

	-- Implement axi_awaddr latching
	-- This process is used to latch the address when both
	-- S_AXI_AWVALID and S_AXI_WVALID are valid.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      axi_awaddr <= (others => '0');
	    else
	      if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1' and aw_en = '1') then
	        -- Write Address latching
	        axi_awaddr <= S_AXI_AWADDR;
	      end if;
	    end if;
	  end if;
	end process;

	-- Implement axi_wready generation
	-- axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is
	-- de-asserted when reset is low.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      axi_wready <= '0';
	    else
	      if (axi_wready = '0' and S_AXI_WVALID = '1' and S_AXI_AWVALID = '1' and aw_en = '1') then
	          -- slave is ready to accept write data when
	          -- there is a valid write address and write data
	          -- on the write address and data bus. This design
	          -- expects no outstanding transactions.
	          axi_wready <= '1';
	      else
	        axi_wready <= '0';
	      end if;
	    end if;
	  end if;
	end process;

	-- Implement memory mapped register select and write logic generation
	-- The write data is accepted and written to memory mapped registers when
	-- axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	-- select byte enables of slave registers while writing.
	-- These registers are cleared when reset (active low) is applied.
	-- Slave register write enable is asserted when valid address and data are available
	-- and the slave is ready to accept the write address and write data.
	slv_reg_wren <= axi_wready and S_AXI_WVALID and axi_awready and S_AXI_AWVALID ;

	process (S_AXI_ACLK)
	variable loc_addr :std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
	variable trigack_pending : std_logic_vector(2 downto 0) := b"000";
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      phase_reg <= (others => '0');
	      counter_pref_reg <= (others => '1');
	      counter_comp_reg <= (others => '1');
	      counter_wb_reg <= (others => '1');
	    else
	      loc_addr := axi_awaddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
	      if (slv_reg_wren = '1') then
	        case loc_addr is
	          when b"00" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes
	                -- slave registor 0
	                phase_reg(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"01" =>
	                for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                      if ( S_AXI_WSTRB(byte_index) = '1' and phase_reg(1 downto 0) = "00") then
                        -- Respective byte enables are asserted as per write strobes
                        -- slave registor 1
                        counter_pref_reg(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                      end if;
                    end loop;
	          when b"10" =>
	                for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                      if ( S_AXI_WSTRB(byte_index) = '1' and phase_reg(1 downto 0) = "00") then
                        -- Respective byte enables are asserted as per write strobes
                        -- slave registor 2
                        counter_comp_reg(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                      end if;
                    end loop;
	          when b"11" =>
                    for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                      if ( S_AXI_WSTRB(byte_index) = '1' and phase_reg(1 downto 0) = "00") then
                        -- Respective byte enables are asserted as per write strobes
                        -- slave registor 3
                        counter_wb_reg(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                      end if;
                    end loop;
	          when others =>
	            phase_reg <= phase_reg;
	            counter_pref_reg <= counter_pref_reg;
	            counter_comp_reg <= counter_comp_reg;
	            counter_wb_reg <= counter_wb_reg;
	        end case;
	      end if;
	    end if;
	    --subtractor logic--
        if counter_en(PREF_CNTR) = '1' then
            if trigack_pending(PREF_CNTR) = '0' then
                counter_pref_reg <= counter_pref_reg - 1;
                trigack_pending(PREF_CNTR) := '1';
            end if;
        else
            trigack_pending(PREF_CNTR) := '0';
        end if;

        if counter_en(COMP_CNTR) = '1' then
           if trigack_pending(COMP_CNTR) = '0' then
               counter_comp_reg <= counter_comp_reg - 1;
               trigack_pending(COMP_CNTR) := '1';
           end if;
        else
           trigack_pending(COMP_CNTR) := '0';
        end if;

        if counter_en(WB_CNTR) = '1' then
           if trigack_pending(WB_CNTR) = '0' then
               counter_wb_reg <= counter_wb_reg - 1;
               trigack_pending(WB_CNTR) := '1';
           end if;
        else
           trigack_pending(WB_CNTR) := '0';
        end if;
    end if;
	end process;

	-- Implement write response logic generation
	-- The write response and response valid signals are asserted by the slave
	-- when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.
	-- This marks the acceptance of address and indicates the status of
	-- write transaction.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      axi_bvalid  <= '0';
	      axi_bresp   <= "00"; --need to work more on the responses
	    else
	      if (axi_awready = '1' and S_AXI_AWVALID = '1' and axi_wready = '1' and S_AXI_WVALID = '1' and axi_bvalid = '0'  ) then
	        axi_bvalid <= '1';
	        axi_bresp  <= "00";
	      elsif (S_AXI_BREADY = '1' and axi_bvalid = '1') then   --check if bready is asserted while bvalid is high)
	        axi_bvalid <= '0';                                 -- (there is a possibility that bready is always asserted high)
	      end if;
	    end if;
	  end if;
	end process;

	-- Implement axi_arready generation
	-- axi_arready is asserted for one S_AXI_ACLK clock cycle when
	-- S_AXI_ARVALID is asserted. axi_awready is
	-- de-asserted when reset (active low) is asserted.
	-- The read address is also latched when S_AXI_ARVALID is
	-- asserted. axi_araddr is reset to zero on reset assertion.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      axi_arready <= '0';
	      axi_araddr  <= (others => '1');
	    else
	      if (axi_arready = '0' and S_AXI_ARVALID = '1') then
	        -- indicates that the slave has acceped the valid read address
	        axi_arready <= '1';
	        -- Read Address latching
	        axi_araddr  <= S_AXI_ARADDR;
	      else
	        axi_arready <= '0';
	      end if;
	    end if;
	  end if;
	end process;

	-- Implement axi_arvalid generation
	-- axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_ARVALID and axi_arready are asserted. The slave registers
	-- data are available on the axi_rdata bus at this instance. The
	-- assertion of axi_rvalid marks the validity of read data on the
	-- bus and axi_rresp indicates the status of read transaction.axi_rvalid
	-- is deasserted on reset (active low). axi_rresp and axi_rdata are
	-- cleared to zero on reset (active low).
	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      axi_rvalid <= '0';
	      axi_rresp  <= "00";
	    else
	      if (axi_arready = '1' and S_AXI_ARVALID = '1' and axi_rvalid = '0') then
	        -- Valid read data is available at the read data bus
	        axi_rvalid <= '1';
	        axi_rresp  <= "00"; -- 'OKAY' response
	      elsif (axi_rvalid = '1' and S_AXI_RREADY = '1') then
	        -- Read data is accepted by the master
	        axi_rvalid <= '0';
	      end if;
	    end if;
	  end if;
	end process;

	-- Implement memory mapped register select and read logic generation
	-- Slave register read enable is asserted when valid address is available
	-- and the slave is ready to accept the read address.
	slv_reg_rden <= axi_arready and S_AXI_ARVALID and (not axi_rvalid) ;

	process (phase_reg, counter_pref_reg, counter_comp_reg, counter_wb_reg, axi_araddr, S_AXI_ARESETN, slv_reg_rden)
	variable loc_addr :std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
	begin
	    -- Address decoding for reading registers
	    loc_addr := axi_araddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
	    case loc_addr is
	      when b"00" =>
	        reg_data_out <= phase_reg;
	      when b"01" =>
	        reg_data_out <= counter_pref_reg;
	      when b"10" =>
	        reg_data_out <= counter_comp_reg;
	      when b"11" =>
	        reg_data_out <= counter_wb_reg;
	      when others =>
	        reg_data_out  <= (others => '0');
	    end case;
	end process;

	-- Output register or memory read data
	process( S_AXI_ACLK ) is
	begin
	  if (rising_edge (S_AXI_ACLK)) then
	    if ( S_AXI_ARESETN = '0' ) then
	      axi_rdata  <= (others => '0');
	    else
	      if (slv_reg_rden = '1') then
	        -- When there is a valid read address (S_AXI_ARVALID) with
	        -- acceptance of read address by the slave (axi_arready),
	        -- output the read dada
	        -- Read address mux
	          axi_rdata <= reg_data_out;     -- register read data
	      end if;
	    end if;
	  end if;
	end process;


	-- Add user logic here
	process (phase_reg(1 downto 0), U_PMU_TRIGIN) is
	begin
	   case phase_reg(1 downto 0) is
           when b"01" =>
                counter_en(PREF_CNTR) <= U_PMU_TRIGIN;
           when b"10" =>
                counter_en(COMP_CNTR) <= U_PMU_TRIGIN;
           when b"11" =>
                counter_en(WB_CNTR)   <= U_PMU_TRIGIN;
           when b"00" =>
                for cntr in 0 to 2 loop
                    counter_en(cntr) <= '0';
                end loop;
           when others =>
                counter_en <= counter_en;
           end case;
    end process;

--	process (counter_pref_en, S_AXI_ACLK, counter_pref_val, counter_pref_rst, counter_pref_reg) is
--    variable trigack_pending : std_logic := '0';
--    begin
--       if rising_edge(S_AXI_ACLK) then
--            if counter_pref_rst = '0' then
--                if (counter_pref_en = '1') then
--                    if (trigack_pending = '0') then
--                        counter_pref_val <= counter_pref_val - 1;
--                        trigack_pending := '1';
--                    end if;
--                else
--                    trigack_pending := '0';
--                end if;
--            else
--                counter_pref_val <= counter_pref_reg;
--            end if;
--       end if;
--    end process;

--    process (counter_comp_en, S_AXI_ACLK, counter_comp_val, counter_comp_rst, counter_comp_reg) is
--    variable trigack_pending : std_logic := '0';
--    begin
--       if rising_edge(S_AXI_ACLK) then
--            if counter_comp_rst = '0' then
--                if (counter_comp_en = '1') then
--                    if (trigack_pending = '0') then
--                        counter_comp_val <= counter_comp_val - 1;
--                        trigack_pending := '1';
--                    end if;
--                else
--                    trigack_pending := '0';
--                end if;
--            else
--                counter_comp_val <= counter_comp_reg;
--            end if;
--       end if;
--    end process;

--    process (counter_wb_en, S_AXI_ACLK, counter_wb_val, counter_wb_rst, counter_wb_reg) is
--    variable trigack_pending : std_logic := '0';
--    begin
--       if rising_edge(S_AXI_ACLK) then
--            if counter_wb_rst = '0' then
--                if (counter_wb_en = '1') then
--                    if (trigack_pending = '0') then
--                        counter_wb_val <= counter_wb_val - 1;
--                        trigack_pending := '1';
--                    end if;
--                else
--                    trigack_pending := '0';
--                end if;
--            else
--                counter_wb_val <= counter_wb_reg;
--            end if;
--       end if;
--    end process;

    process (phase_reg(1 downto 0), counter_pref_reg, counter_comp_reg, counter_wb_reg) is
    begin
        if phase_reg(1 downto 0) /= b"00" and
            (counter_pref_reg = (counter_pref_reg'range => '0') or
            counter_comp_reg = (counter_comp_reg'range => '0') or
            counter_wb_reg = (counter_wb_reg'range => '0')) then

            U_LIMIT_IRQ <= '1';
        else
            U_LIMIT_IRQ <= '0';
        end if;
    end process;

    U_PMU_TRIGOUT <= U_PMU_TRIGIN;
	-- User logic ends

end arch_imp;
