// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
// Date        : Sat May 19 17:03:10 2018
// Host        : matejjoe running 64-bit Debian GNU/Linux 9.2 (stretch)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ ps_pl_top_configuration_prem_mem_counter_axi_0_0_sim_netlist.v
// Design      : ps_pl_top_configuration_prem_mem_counter_axi_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu9eg-ffvb1156-2-i-es2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_prem_mem_counter_axi_v1_0
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_prem_mem_counter_axi_v1_0_S00_AXI prem_mem_counter_axi_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_prem_mem_counter_axi_v1_0_S00_AXI
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire axi_arready0;
  wire [4:2]axi_awaddr;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_1_n_0 ;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_1_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_1_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[12]_i_1_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_1_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_1_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_1_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[16]_i_1_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_1_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_1_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_1_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[1]_i_1_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[20]_i_1_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_1_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_1_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_1_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[24]_i_1_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_1_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_1_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_1_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[28]_i_1_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_1_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[2]_i_1_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[30]_i_1_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_2_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_1_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_1_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_1_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_1_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_1_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[8]_i_1_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_1_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire [31:0]comp_limit;
  wire \comp_limit[15]_i_1_n_0 ;
  wire \comp_limit[23]_i_1_n_0 ;
  wire \comp_limit[31]_i_1_n_0 ;
  wire \comp_limit[7]_i_1_n_0 ;
  wire [31:7]p_1_in;
  wire [31:0]phase;
  wire [31:0]pref_limit;
  wire \pref_limit[15]_i_1_n_0 ;
  wire \pref_limit[23]_i_1_n_0 ;
  wire \pref_limit[31]_i_1_n_0 ;
  wire \pref_limit[7]_i_1_n_0 ;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [2:0]sel0;
  wire slv_reg_rden;
  wire slv_reg_wren;
  wire [31:0]wb_limit;
  wire \wb_limit[15]_i_1_n_0 ;
  wire \wb_limit[23]_i_1_n_0 ;
  wire \wb_limit[31]_i_1_n_0 ;
  wire \wb_limit[7]_i_1_n_0 ;
  wire write_finish;
  wire write_finish_i_1_n_0;

  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .S(axi_awready_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(axi_awaddr[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(axi_awaddr[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(axi_awaddr[4]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(axi_awready_i_1_n_0));
  LUT3 #(
    .INIT(8'h3A)) 
    axi_bvalid_i_1
       (.I0(write_finish),
        .I1(s00_axi_bready),
        .I2(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(wb_limit[0]),
        .I1(comp_limit[0]),
        .I2(sel0[1]),
        .I3(pref_limit[0]),
        .I4(sel0[0]),
        .I5(phase[0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_2 
       (.I0(wb_limit[10]),
        .I1(comp_limit[10]),
        .I2(sel0[1]),
        .I3(pref_limit[10]),
        .I4(sel0[0]),
        .I5(phase[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_2 
       (.I0(wb_limit[11]),
        .I1(comp_limit[11]),
        .I2(sel0[1]),
        .I3(pref_limit[11]),
        .I4(sel0[0]),
        .I5(phase[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_2 
       (.I0(wb_limit[12]),
        .I1(comp_limit[12]),
        .I2(sel0[1]),
        .I3(pref_limit[12]),
        .I4(sel0[0]),
        .I5(phase[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_2 
       (.I0(wb_limit[13]),
        .I1(comp_limit[13]),
        .I2(sel0[1]),
        .I3(pref_limit[13]),
        .I4(sel0[0]),
        .I5(phase[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_2 
       (.I0(wb_limit[14]),
        .I1(comp_limit[14]),
        .I2(sel0[1]),
        .I3(pref_limit[14]),
        .I4(sel0[0]),
        .I5(phase[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_2 
       (.I0(wb_limit[15]),
        .I1(comp_limit[15]),
        .I2(sel0[1]),
        .I3(pref_limit[15]),
        .I4(sel0[0]),
        .I5(phase[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(wb_limit[16]),
        .I1(comp_limit[16]),
        .I2(sel0[1]),
        .I3(pref_limit[16]),
        .I4(sel0[0]),
        .I5(phase[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(wb_limit[17]),
        .I1(comp_limit[17]),
        .I2(sel0[1]),
        .I3(pref_limit[17]),
        .I4(sel0[0]),
        .I5(phase[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_2 
       (.I0(wb_limit[18]),
        .I1(comp_limit[18]),
        .I2(sel0[1]),
        .I3(pref_limit[18]),
        .I4(sel0[0]),
        .I5(phase[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_2 
       (.I0(wb_limit[19]),
        .I1(comp_limit[19]),
        .I2(sel0[1]),
        .I3(pref_limit[19]),
        .I4(sel0[0]),
        .I5(phase[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_2 
       (.I0(wb_limit[1]),
        .I1(comp_limit[1]),
        .I2(sel0[1]),
        .I3(pref_limit[1]),
        .I4(sel0[0]),
        .I5(phase[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_2 
       (.I0(wb_limit[20]),
        .I1(comp_limit[20]),
        .I2(sel0[1]),
        .I3(pref_limit[20]),
        .I4(sel0[0]),
        .I5(phase[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(wb_limit[21]),
        .I1(comp_limit[21]),
        .I2(sel0[1]),
        .I3(pref_limit[21]),
        .I4(sel0[0]),
        .I5(phase[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_2 
       (.I0(wb_limit[22]),
        .I1(comp_limit[22]),
        .I2(sel0[1]),
        .I3(pref_limit[22]),
        .I4(sel0[0]),
        .I5(phase[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_2 
       (.I0(wb_limit[23]),
        .I1(comp_limit[23]),
        .I2(sel0[1]),
        .I3(pref_limit[23]),
        .I4(sel0[0]),
        .I5(phase[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(wb_limit[24]),
        .I1(comp_limit[24]),
        .I2(sel0[1]),
        .I3(pref_limit[24]),
        .I4(sel0[0]),
        .I5(phase[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(wb_limit[25]),
        .I1(comp_limit[25]),
        .I2(sel0[1]),
        .I3(pref_limit[25]),
        .I4(sel0[0]),
        .I5(phase[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(wb_limit[26]),
        .I1(comp_limit[26]),
        .I2(sel0[1]),
        .I3(pref_limit[26]),
        .I4(sel0[0]),
        .I5(phase[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[27]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(wb_limit[27]),
        .I1(comp_limit[27]),
        .I2(sel0[1]),
        .I3(pref_limit[27]),
        .I4(sel0[0]),
        .I5(phase[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(wb_limit[28]),
        .I1(comp_limit[28]),
        .I2(sel0[1]),
        .I3(pref_limit[28]),
        .I4(sel0[0]),
        .I5(phase[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(wb_limit[29]),
        .I1(comp_limit[29]),
        .I2(sel0[1]),
        .I3(pref_limit[29]),
        .I4(sel0[0]),
        .I5(phase[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_2 
       (.I0(wb_limit[2]),
        .I1(comp_limit[2]),
        .I2(sel0[1]),
        .I3(pref_limit[2]),
        .I4(sel0[0]),
        .I5(phase[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[30]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(wb_limit[30]),
        .I1(comp_limit[30]),
        .I2(sel0[1]),
        .I3(pref_limit[30]),
        .I4(sel0[0]),
        .I5(phase[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[31]_i_2 
       (.I0(\axi_rdata[31]_i_3_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_3 
       (.I0(wb_limit[31]),
        .I1(comp_limit[31]),
        .I2(sel0[1]),
        .I3(pref_limit[31]),
        .I4(sel0[0]),
        .I5(phase[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_2 
       (.I0(wb_limit[3]),
        .I1(comp_limit[3]),
        .I2(sel0[1]),
        .I3(pref_limit[3]),
        .I4(sel0[0]),
        .I5(phase[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(wb_limit[4]),
        .I1(comp_limit[4]),
        .I2(sel0[1]),
        .I3(pref_limit[4]),
        .I4(sel0[0]),
        .I5(phase[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(wb_limit[5]),
        .I1(comp_limit[5]),
        .I2(sel0[1]),
        .I3(pref_limit[5]),
        .I4(sel0[0]),
        .I5(phase[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(wb_limit[6]),
        .I1(comp_limit[6]),
        .I2(sel0[1]),
        .I3(pref_limit[6]),
        .I4(sel0[0]),
        .I5(phase[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_2 
       (.I0(wb_limit[7]),
        .I1(comp_limit[7]),
        .I2(sel0[1]),
        .I3(pref_limit[7]),
        .I4(sel0[0]),
        .I5(phase[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_2 
       (.I0(wb_limit[8]),
        .I1(comp_limit[8]),
        .I2(sel0[1]),
        .I3(pref_limit[8]),
        .I4(sel0[0]),
        .I5(phase[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_2 
       (.I0(wb_limit[9]),
        .I1(comp_limit[9]),
        .I2(sel0[1]),
        .I3(pref_limit[9]),
        .I4(sel0[0]),
        .I5(phase[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[0]_i_1_n_0 ),
        .Q(s00_axi_rdata[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[10]_i_1_n_0 ),
        .Q(s00_axi_rdata[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[11]_i_1_n_0 ),
        .Q(s00_axi_rdata[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[12]_i_1_n_0 ),
        .Q(s00_axi_rdata[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[13]_i_1_n_0 ),
        .Q(s00_axi_rdata[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[14]_i_1_n_0 ),
        .Q(s00_axi_rdata[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[15]_i_1_n_0 ),
        .Q(s00_axi_rdata[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[16]_i_1_n_0 ),
        .Q(s00_axi_rdata[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[17]_i_1_n_0 ),
        .Q(s00_axi_rdata[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[18]_i_1_n_0 ),
        .Q(s00_axi_rdata[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[19]_i_1_n_0 ),
        .Q(s00_axi_rdata[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[1]_i_1_n_0 ),
        .Q(s00_axi_rdata[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[20]_i_1_n_0 ),
        .Q(s00_axi_rdata[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[21]_i_1_n_0 ),
        .Q(s00_axi_rdata[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[22]_i_1_n_0 ),
        .Q(s00_axi_rdata[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[23]_i_1_n_0 ),
        .Q(s00_axi_rdata[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[24]_i_1_n_0 ),
        .Q(s00_axi_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[25]_i_1_n_0 ),
        .Q(s00_axi_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[26]_i_1_n_0 ),
        .Q(s00_axi_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[27]_i_1_n_0 ),
        .Q(s00_axi_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[28]_i_1_n_0 ),
        .Q(s00_axi_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[29]_i_1_n_0 ),
        .Q(s00_axi_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[2]_i_1_n_0 ),
        .Q(s00_axi_rdata[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[30]_i_1_n_0 ),
        .Q(s00_axi_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[31]_i_2_n_0 ),
        .Q(s00_axi_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[3]_i_1_n_0 ),
        .Q(s00_axi_rdata[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[4]_i_1_n_0 ),
        .Q(s00_axi_rdata[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[5]_i_1_n_0 ),
        .Q(s00_axi_rdata[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[6]_i_1_n_0 ),
        .Q(s00_axi_rdata[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[7]_i_1_n_0 ),
        .Q(s00_axi_rdata[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[8]_i_1_n_0 ),
        .Q(s00_axi_rdata[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(\axi_rdata[9]_i_1_n_0 ),
        .Q(s00_axi_rdata[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \comp_limit[15]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[1]),
        .I4(axi_awaddr[3]),
        .O(\comp_limit[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \comp_limit[23]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[2]),
        .I4(axi_awaddr[3]),
        .O(\comp_limit[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \comp_limit[31]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[3]),
        .I4(axi_awaddr[3]),
        .O(\comp_limit[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \comp_limit[7]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[0]),
        .I4(axi_awaddr[3]),
        .O(\comp_limit[7]_i_1_n_0 ));
  FDRE \comp_limit_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(comp_limit[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(comp_limit[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(comp_limit[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(comp_limit[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(comp_limit[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(comp_limit[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(comp_limit[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(comp_limit[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(comp_limit[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(comp_limit[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(comp_limit[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(comp_limit[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(comp_limit[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(comp_limit[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(comp_limit[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(comp_limit[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(comp_limit[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(comp_limit[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(comp_limit[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(comp_limit[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(comp_limit[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(comp_limit[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(comp_limit[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(comp_limit[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(comp_limit[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(comp_limit[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(comp_limit[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(comp_limit[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(comp_limit[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(comp_limit[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(comp_limit[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \comp_limit_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(comp_limit[9]),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h00000020)) 
    \phase[15]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(s00_axi_wstrb[1]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[3]),
        .O(p_1_in[15]));
  LUT5 #(
    .INIT(32'h00000020)) 
    \phase[23]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(s00_axi_wstrb[2]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[3]),
        .O(p_1_in[23]));
  LUT5 #(
    .INIT(32'h00000020)) 
    \phase[31]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(s00_axi_wstrb[3]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[3]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \phase[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_WREADY),
        .I3(S_AXI_AWREADY),
        .O(slv_reg_wren));
  LUT5 #(
    .INIT(32'h00000020)) 
    \phase[7]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(s00_axi_wstrb[0]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[3]),
        .O(p_1_in[7]));
  FDRE \phase_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(phase[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(phase[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(phase[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(phase[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(phase[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(phase[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(phase[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(phase[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(phase[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(phase[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(phase[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(phase[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(phase[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(phase[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(phase[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(phase[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(phase[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(phase[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(phase[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(phase[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(phase[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(phase[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(phase[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(phase[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(phase[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(phase[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(phase[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(phase[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(phase[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(phase[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(phase[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \phase_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(phase[9]),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \pref_limit[15]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\pref_limit[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \pref_limit[23]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\pref_limit[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \pref_limit[31]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\pref_limit[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \pref_limit[7]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\pref_limit[7]_i_1_n_0 ));
  FDRE \pref_limit_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(pref_limit[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(pref_limit[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(pref_limit[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(pref_limit[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(pref_limit[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(pref_limit[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(pref_limit[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(pref_limit[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(pref_limit[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(pref_limit[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(pref_limit[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(pref_limit[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(pref_limit[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(pref_limit[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(pref_limit[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(pref_limit[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(pref_limit[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(pref_limit[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(pref_limit[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(pref_limit[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(pref_limit[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(pref_limit[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(pref_limit[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(pref_limit[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(pref_limit[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(pref_limit[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(pref_limit[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(pref_limit[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(pref_limit[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(pref_limit[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(pref_limit[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \pref_limit_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(pref_limit[9]),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h20000000)) 
    \wb_limit[15]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\wb_limit[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \wb_limit[23]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\wb_limit[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \wb_limit[31]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\wb_limit[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \wb_limit[7]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\wb_limit[7]_i_1_n_0 ));
  FDRE \wb_limit_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(wb_limit[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(wb_limit[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(wb_limit[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(wb_limit[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(wb_limit[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(wb_limit[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(wb_limit[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(wb_limit[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(wb_limit[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(wb_limit[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(wb_limit[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(wb_limit[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(wb_limit[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(wb_limit[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(wb_limit[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(wb_limit[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(wb_limit[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(wb_limit[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(wb_limit[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(wb_limit[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(wb_limit[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(wb_limit[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(wb_limit[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(wb_limit[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(wb_limit[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(wb_limit[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(wb_limit[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(wb_limit[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(wb_limit[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(wb_limit[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(wb_limit[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \wb_limit_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(wb_limit[9]),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h8000FFFF80000000)) 
    write_finish_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_WREADY),
        .I3(S_AXI_AWREADY),
        .I4(s00_axi_aresetn),
        .I5(write_finish),
        .O(write_finish_i_1_n_0));
  FDRE write_finish_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(write_finish_i_1_n_0),
        .Q(write_finish),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "ps_pl_top_configuration_prem_mem_counter_axi_0_0,prem_mem_counter_axi_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "prem_mem_counter_axi_v1_0,Vivado 2018.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (pmu_trigin,
    pmu_trigout,
    limit_irq,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  input pmu_trigin;
  output pmu_trigout;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 limit_irq INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME limit_irq, SENSITIVITY LEVEL_HIGH, PortWidth 1" *) output limit_irq;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 7, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 99990000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN ps_pl_top_configuration_zynq_ultra_ps_e_0_0_pl_clk0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [4:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [4:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 99990000, PHASE 0.000, CLK_DOMAIN ps_pl_top_configuration_zynq_ultra_ps_e_0_0_pl_clk0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign limit_irq = \<const0> ;
  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_prem_mem_counter_axi_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
