// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
// Date        : Sat May 19 17:53:51 2018
// Host        : matejjoe running 64-bit Debian GNU/Linux 9.2 (stretch)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ ps_pl_top_configuration_prem_mem_counter_axi_0_0_sim_netlist.v
// Design      : ps_pl_top_configuration_prem_mem_counter_axi_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu9eg-ffvb1156-2-i-es2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_prem_mem_counter_axi_v1_0
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    limit_irq,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_awvalid,
    s00_axi_wvalid,
    pmu_trigin,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output limit_irq;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input pmu_trigin;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire limit_irq;
  wire pmu_trigin;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_prem_mem_counter_axi_v1_0_S00_AXI prem_mem_counter_axi_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .limit_irq(limit_irq),
        .pmu_trigin(pmu_trigin),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_prem_mem_counter_axi_v1_0_S00_AXI
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    limit_irq,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb,
    s00_axi_awvalid,
    s00_axi_wvalid,
    pmu_trigin,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output limit_irq;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input pmu_trigin;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire \ADDER_EN[0].ctr[0][0]_i_3_n_0 ;
  wire \ADDER_EN[0].ctr[0][0]_i_4_n_0 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_0 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_1 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_10 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_11 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_12 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_13 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_14 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_15 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_2 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_3 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_5 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_6 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_7 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_8 ;
  wire \ADDER_EN[0].ctr_reg[0][0]_i_2_n_9 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_0 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_1 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_10 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_11 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_12 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_13 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_14 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_15 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_2 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_3 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_5 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_6 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_7 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_8 ;
  wire \ADDER_EN[0].ctr_reg[0][16]_i_1_n_9 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_1 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_10 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_11 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_12 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_13 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_14 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_15 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_2 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_3 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_5 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_6 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_7 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_8 ;
  wire \ADDER_EN[0].ctr_reg[0][24]_i_1_n_9 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_0 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_1 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_10 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_11 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_12 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_13 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_14 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_15 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_2 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_3 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_5 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_6 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_7 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_8 ;
  wire \ADDER_EN[0].ctr_reg[0][8]_i_1_n_9 ;
  wire [31:0]\ADDER_EN[0].ctr_reg[0]_2 ;
  wire \ADDER_EN[0].trigack_pending_i_1_n_0 ;
  wire \ADDER_EN[0].trigack_pending_i_2_n_0 ;
  wire \ADDER_EN[1].ctr[1][0]_i_3_n_0 ;
  wire \ADDER_EN[1].ctr[1][0]_i_4_n_0 ;
  wire \ADDER_EN[1].ctr[1][0]_i_5_n_0 ;
  wire \ADDER_EN[1].ctr[1][0]_i_6_n_0 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_0 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_1 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_10 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_11 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_12 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_13 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_14 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_15 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_2 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_3 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_5 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_6 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_7 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_8 ;
  wire \ADDER_EN[1].ctr_reg[1][0]_i_2_n_9 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_0 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_1 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_10 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_11 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_12 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_13 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_14 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_15 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_2 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_3 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_5 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_6 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_7 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_8 ;
  wire \ADDER_EN[1].ctr_reg[1][16]_i_1_n_9 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_1 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_10 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_11 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_12 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_13 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_14 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_15 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_2 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_3 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_5 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_6 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_7 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_8 ;
  wire \ADDER_EN[1].ctr_reg[1][24]_i_1_n_9 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_0 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_1 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_10 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_11 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_12 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_13 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_14 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_15 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_2 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_3 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_5 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_6 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_7 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_8 ;
  wire \ADDER_EN[1].ctr_reg[1][8]_i_1_n_9 ;
  wire [31:0]\ADDER_EN[1].ctr_reg[1]_0 ;
  wire \ADDER_EN[1].trigack_pending_i_1_n_0 ;
  wire \ADDER_EN[1].trigack_pending_i_2_n_0 ;
  wire \ADDER_EN[1].trigack_pending_i_3_n_0 ;
  wire \ADDER_EN[1].trigack_pending_reg_n_0 ;
  wire \ADDER_EN[2].ctr[2][0]_i_3_n_0 ;
  wire \ADDER_EN[2].ctr[2][0]_i_4_n_0 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_0 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_1 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_10 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_11 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_12 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_13 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_14 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_15 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_2 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_3 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_5 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_6 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_7 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_8 ;
  wire \ADDER_EN[2].ctr_reg[2][0]_i_2_n_9 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_0 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_1 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_10 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_11 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_12 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_13 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_14 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_15 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_2 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_3 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_5 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_6 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_7 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_8 ;
  wire \ADDER_EN[2].ctr_reg[2][16]_i_1_n_9 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_1 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_10 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_11 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_12 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_13 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_14 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_15 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_2 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_3 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_5 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_6 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_7 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_8 ;
  wire \ADDER_EN[2].ctr_reg[2][24]_i_1_n_9 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_0 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_1 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_10 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_11 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_12 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_13 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_14 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_15 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_2 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_3 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_5 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_6 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_7 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_8 ;
  wire \ADDER_EN[2].ctr_reg[2][8]_i_1_n_9 ;
  wire [31:0]\ADDER_EN[2].ctr_reg[2]_1 ;
  wire \ADDER_EN[2].trigack_pending_i_1_n_0 ;
  wire \ADDER_EN[2].trigack_pending_i_2_n_0 ;
  wire \ADDER_EN[2].trigack_pending_reg_n_0 ;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire axi_arready0;
  wire [4:2]axi_awaddr;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[31]_i_4_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire [31:0]comp_limit;
  wire \comp_limit[15]_i_1_n_0 ;
  wire \comp_limit[23]_i_1_n_0 ;
  wire \comp_limit[31]_i_1_n_0 ;
  wire \comp_limit[7]_i_1_n_0 ;
  wire gtOp;
  wire gtOp_carry__0_i_10_n_0;
  wire gtOp_carry__0_i_11_n_0;
  wire gtOp_carry__0_i_12_n_0;
  wire gtOp_carry__0_i_13_n_0;
  wire gtOp_carry__0_i_14_n_0;
  wire gtOp_carry__0_i_15_n_0;
  wire gtOp_carry__0_i_16_n_0;
  wire gtOp_carry__0_i_1_n_0;
  wire gtOp_carry__0_i_2_n_0;
  wire gtOp_carry__0_i_3_n_0;
  wire gtOp_carry__0_i_4_n_0;
  wire gtOp_carry__0_i_5_n_0;
  wire gtOp_carry__0_i_6_n_0;
  wire gtOp_carry__0_i_7_n_0;
  wire gtOp_carry__0_i_8_n_0;
  wire gtOp_carry__0_i_9_n_0;
  wire gtOp_carry__0_n_0;
  wire gtOp_carry__0_n_1;
  wire gtOp_carry__0_n_2;
  wire gtOp_carry__0_n_3;
  wire gtOp_carry__0_n_5;
  wire gtOp_carry__0_n_6;
  wire gtOp_carry__0_n_7;
  wire gtOp_carry_i_10_n_0;
  wire gtOp_carry_i_11_n_0;
  wire gtOp_carry_i_12_n_0;
  wire gtOp_carry_i_13_n_0;
  wire gtOp_carry_i_14_n_0;
  wire gtOp_carry_i_15_n_0;
  wire gtOp_carry_i_16_n_0;
  wire gtOp_carry_i_1_n_0;
  wire gtOp_carry_i_2_n_0;
  wire gtOp_carry_i_3_n_0;
  wire gtOp_carry_i_4_n_0;
  wire gtOp_carry_i_5_n_0;
  wire gtOp_carry_i_6_n_0;
  wire gtOp_carry_i_7_n_0;
  wire gtOp_carry_i_8_n_0;
  wire gtOp_carry_i_9_n_0;
  wire gtOp_carry_n_0;
  wire gtOp_carry_n_1;
  wire gtOp_carry_n_2;
  wire gtOp_carry_n_3;
  wire gtOp_carry_n_5;
  wire gtOp_carry_n_6;
  wire gtOp_carry_n_7;
  wire \gtOp_inferred__0/i__carry__0_n_0 ;
  wire \gtOp_inferred__0/i__carry__0_n_1 ;
  wire \gtOp_inferred__0/i__carry__0_n_2 ;
  wire \gtOp_inferred__0/i__carry__0_n_3 ;
  wire \gtOp_inferred__0/i__carry__0_n_5 ;
  wire \gtOp_inferred__0/i__carry__0_n_6 ;
  wire \gtOp_inferred__0/i__carry__0_n_7 ;
  wire \gtOp_inferred__0/i__carry_n_0 ;
  wire \gtOp_inferred__0/i__carry_n_1 ;
  wire \gtOp_inferred__0/i__carry_n_2 ;
  wire \gtOp_inferred__0/i__carry_n_3 ;
  wire \gtOp_inferred__0/i__carry_n_5 ;
  wire \gtOp_inferred__0/i__carry_n_6 ;
  wire \gtOp_inferred__0/i__carry_n_7 ;
  wire \gtOp_inferred__1/i__carry__0_n_1 ;
  wire \gtOp_inferred__1/i__carry__0_n_2 ;
  wire \gtOp_inferred__1/i__carry__0_n_3 ;
  wire \gtOp_inferred__1/i__carry__0_n_5 ;
  wire \gtOp_inferred__1/i__carry__0_n_6 ;
  wire \gtOp_inferred__1/i__carry__0_n_7 ;
  wire \gtOp_inferred__1/i__carry_n_0 ;
  wire \gtOp_inferred__1/i__carry_n_1 ;
  wire \gtOp_inferred__1/i__carry_n_2 ;
  wire \gtOp_inferred__1/i__carry_n_3 ;
  wire \gtOp_inferred__1/i__carry_n_5 ;
  wire \gtOp_inferred__1/i__carry_n_6 ;
  wire \gtOp_inferred__1/i__carry_n_7 ;
  wire i__carry__0_i_10__0_n_0;
  wire i__carry__0_i_10_n_0;
  wire i__carry__0_i_11__0_n_0;
  wire i__carry__0_i_11_n_0;
  wire i__carry__0_i_12__0_n_0;
  wire i__carry__0_i_12_n_0;
  wire i__carry__0_i_13__0_n_0;
  wire i__carry__0_i_13_n_0;
  wire i__carry__0_i_14__0_n_0;
  wire i__carry__0_i_14_n_0;
  wire i__carry__0_i_15__0_n_0;
  wire i__carry__0_i_15_n_0;
  wire i__carry__0_i_16__0_n_0;
  wire i__carry__0_i_16_n_0;
  wire i__carry__0_i_1__0_n_0;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2__0_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3__0_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4__0_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__0_i_5__0_n_0;
  wire i__carry__0_i_5_n_0;
  wire i__carry__0_i_6__0_n_0;
  wire i__carry__0_i_6_n_0;
  wire i__carry__0_i_7__0_n_0;
  wire i__carry__0_i_7_n_0;
  wire i__carry__0_i_8__0_n_0;
  wire i__carry__0_i_8_n_0;
  wire i__carry__0_i_9__0_n_0;
  wire i__carry__0_i_9_n_0;
  wire i__carry_i_10__0_n_0;
  wire i__carry_i_10_n_0;
  wire i__carry_i_11__0_n_0;
  wire i__carry_i_11_n_0;
  wire i__carry_i_12__0_n_0;
  wire i__carry_i_12_n_0;
  wire i__carry_i_13__0_n_0;
  wire i__carry_i_13_n_0;
  wire i__carry_i_14__0_n_0;
  wire i__carry_i_14_n_0;
  wire i__carry_i_15__0_n_0;
  wire i__carry_i_15_n_0;
  wire i__carry_i_16__0_n_0;
  wire i__carry_i_16_n_0;
  wire i__carry_i_1__0_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2__0_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3__0_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4__0_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5__0_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6__0_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7__0_n_0;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8__0_n_0;
  wire i__carry_i_8_n_0;
  wire i__carry_i_9__0_n_0;
  wire i__carry_i_9_n_0;
  wire limit_irq;
  wire limit_irq_INST_0_i_1_n_0;
  wire limit_irq_INST_0_i_2_n_0;
  wire limit_irq_INST_0_i_3_n_0;
  wire limit_irq_INST_0_i_4_n_0;
  wire limit_irq_INST_0_i_5_n_0;
  wire limit_irq_INST_0_i_6_n_0;
  wire limit_irq_INST_0_i_7_n_0;
  wire limit_irq_INST_0_i_8_n_0;
  wire limit_irq_INST_0_i_9_n_0;
  wire p_0_in;
  wire p_10_out;
  wire p_13_out;
  wire p_16_out;
  wire [31:7]p_1_in;
  wire [31:0]phase;
  wire pmu_trigin;
  wire [31:0]pref_limit;
  wire \pref_limit[15]_i_1_n_0 ;
  wire \pref_limit[23]_i_1_n_0 ;
  wire \pref_limit[31]_i_1_n_0 ;
  wire \pref_limit[7]_i_1_n_0 ;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [2:0]sel0;
  wire slv_reg_rden;
  wire slv_reg_wren;
  wire trigack_pending;
  wire [31:0]wb_limit;
  wire \wb_limit[15]_i_1_n_0 ;
  wire \wb_limit[23]_i_1_n_0 ;
  wire \wb_limit[31]_i_1_n_0 ;
  wire \wb_limit[7]_i_1_n_0 ;
  wire write_finish;
  wire write_finish_i_1_n_0;
  wire [3:3]\NLW_ADDER_EN[0].ctr_reg[0][0]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_ADDER_EN[0].ctr_reg[0][16]_i_1_CO_UNCONNECTED ;
  wire [7:3]\NLW_ADDER_EN[0].ctr_reg[0][24]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_ADDER_EN[0].ctr_reg[0][8]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_ADDER_EN[1].ctr_reg[1][0]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_ADDER_EN[1].ctr_reg[1][16]_i_1_CO_UNCONNECTED ;
  wire [7:3]\NLW_ADDER_EN[1].ctr_reg[1][24]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_ADDER_EN[1].ctr_reg[1][8]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_ADDER_EN[2].ctr_reg[2][0]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_ADDER_EN[2].ctr_reg[2][16]_i_1_CO_UNCONNECTED ;
  wire [7:3]\NLW_ADDER_EN[2].ctr_reg[2][24]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_ADDER_EN[2].ctr_reg[2][8]_i_1_CO_UNCONNECTED ;
  wire [3:3]NLW_gtOp_carry_CO_UNCONNECTED;
  wire [7:0]NLW_gtOp_carry_O_UNCONNECTED;
  wire [3:3]NLW_gtOp_carry__0_CO_UNCONNECTED;
  wire [7:0]NLW_gtOp_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_gtOp_inferred__0/i__carry_CO_UNCONNECTED ;
  wire [7:0]\NLW_gtOp_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_gtOp_inferred__0/i__carry__0_CO_UNCONNECTED ;
  wire [7:0]\NLW_gtOp_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:3]\NLW_gtOp_inferred__1/i__carry_CO_UNCONNECTED ;
  wire [7:0]\NLW_gtOp_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_gtOp_inferred__1/i__carry__0_CO_UNCONNECTED ;
  wire [7:0]\NLW_gtOp_inferred__1/i__carry__0_O_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h0004)) 
    \ADDER_EN[0].ctr[0][0]_i_1 
       (.I0(limit_irq_INST_0_i_2_n_0),
        .I1(\ADDER_EN[0].ctr[0][0]_i_3_n_0 ),
        .I2(limit_irq_INST_0_i_4_n_0),
        .I3(limit_irq_INST_0_i_3_n_0),
        .O(p_16_out));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \ADDER_EN[0].ctr[0][0]_i_3 
       (.I0(phase[0]),
        .I1(phase[1]),
        .I2(pmu_trigin),
        .I3(trigack_pending),
        .I4(phase[3]),
        .I5(phase[2]),
        .O(\ADDER_EN[0].ctr[0][0]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ADDER_EN[0].ctr[0][0]_i_4 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [0]),
        .O(\ADDER_EN[0].ctr[0][0]_i_4_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][0] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][0]_i_2_n_15 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [0]));
  CARRY8 \ADDER_EN[0].ctr_reg[0][0]_i_2 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\ADDER_EN[0].ctr_reg[0][0]_i_2_n_0 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_1 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_2 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_3 ,\NLW_ADDER_EN[0].ctr_reg[0][0]_i_2_CO_UNCONNECTED [3],\ADDER_EN[0].ctr_reg[0][0]_i_2_n_5 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_6 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\ADDER_EN[0].ctr_reg[0][0]_i_2_n_8 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_9 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_10 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_11 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_12 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_13 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_14 ,\ADDER_EN[0].ctr_reg[0][0]_i_2_n_15 }),
        .S({\ADDER_EN[0].ctr_reg[0]_2 [7:1],\ADDER_EN[0].ctr[0][0]_i_4_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][10] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][8]_i_1_n_13 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [10]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][11] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][8]_i_1_n_12 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [11]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][12] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][8]_i_1_n_11 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [12]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][13] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][8]_i_1_n_10 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [13]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][14] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][8]_i_1_n_9 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [14]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][15] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][8]_i_1_n_8 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [15]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][16] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][16]_i_1_n_15 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [16]));
  CARRY8 \ADDER_EN[0].ctr_reg[0][16]_i_1 
       (.CI(\ADDER_EN[0].ctr_reg[0][8]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\ADDER_EN[0].ctr_reg[0][16]_i_1_n_0 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_1 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_2 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_3 ,\NLW_ADDER_EN[0].ctr_reg[0][16]_i_1_CO_UNCONNECTED [3],\ADDER_EN[0].ctr_reg[0][16]_i_1_n_5 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_6 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ADDER_EN[0].ctr_reg[0][16]_i_1_n_8 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_9 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_10 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_11 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_12 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_13 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_14 ,\ADDER_EN[0].ctr_reg[0][16]_i_1_n_15 }),
        .S(\ADDER_EN[0].ctr_reg[0]_2 [23:16]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][17] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][16]_i_1_n_14 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [17]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][18] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][16]_i_1_n_13 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [18]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][19] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][16]_i_1_n_12 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [19]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][1] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][0]_i_2_n_14 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][20] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][16]_i_1_n_11 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [20]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][21] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][16]_i_1_n_10 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [21]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][22] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][16]_i_1_n_9 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [22]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][23] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][16]_i_1_n_8 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [23]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][24] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][24]_i_1_n_15 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [24]));
  CARRY8 \ADDER_EN[0].ctr_reg[0][24]_i_1 
       (.CI(\ADDER_EN[0].ctr_reg[0][16]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_ADDER_EN[0].ctr_reg[0][24]_i_1_CO_UNCONNECTED [7],\ADDER_EN[0].ctr_reg[0][24]_i_1_n_1 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_2 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_3 ,\NLW_ADDER_EN[0].ctr_reg[0][24]_i_1_CO_UNCONNECTED [3],\ADDER_EN[0].ctr_reg[0][24]_i_1_n_5 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_6 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ADDER_EN[0].ctr_reg[0][24]_i_1_n_8 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_9 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_10 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_11 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_12 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_13 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_14 ,\ADDER_EN[0].ctr_reg[0][24]_i_1_n_15 }),
        .S(\ADDER_EN[0].ctr_reg[0]_2 [31:24]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][25] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][24]_i_1_n_14 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [25]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][26] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][24]_i_1_n_13 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [26]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][27] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][24]_i_1_n_12 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [27]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][28] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][24]_i_1_n_11 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [28]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][29] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][24]_i_1_n_10 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [29]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][2] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][0]_i_2_n_13 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [2]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][30] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][24]_i_1_n_9 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [30]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][31] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][24]_i_1_n_8 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [31]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][3] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][0]_i_2_n_12 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [3]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][4] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][0]_i_2_n_11 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [4]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][5] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][0]_i_2_n_10 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [5]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][6] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][0]_i_2_n_9 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [6]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][7] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][0]_i_2_n_8 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [7]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][8] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][8]_i_1_n_15 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [8]));
  CARRY8 \ADDER_EN[0].ctr_reg[0][8]_i_1 
       (.CI(\ADDER_EN[0].ctr_reg[0][0]_i_2_n_0 ),
        .CI_TOP(1'b0),
        .CO({\ADDER_EN[0].ctr_reg[0][8]_i_1_n_0 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_1 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_2 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_3 ,\NLW_ADDER_EN[0].ctr_reg[0][8]_i_1_CO_UNCONNECTED [3],\ADDER_EN[0].ctr_reg[0][8]_i_1_n_5 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_6 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ADDER_EN[0].ctr_reg[0][8]_i_1_n_8 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_9 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_10 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_11 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_12 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_13 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_14 ,\ADDER_EN[0].ctr_reg[0][8]_i_1_n_15 }),
        .S(\ADDER_EN[0].ctr_reg[0]_2 [15:8]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].ctr_reg[0][9] 
       (.C(s00_axi_aclk),
        .CE(p_16_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[0].ctr_reg[0][8]_i_1_n_14 ),
        .Q(\ADDER_EN[0].ctr_reg[0]_2 [9]));
  LUT6 #(
    .INIT(64'hCCCCCFCECCCCC0CE)) 
    \ADDER_EN[0].trigack_pending_i_1 
       (.I0(\ADDER_EN[0].trigack_pending_i_2_n_0 ),
        .I1(pmu_trigin),
        .I2(limit_irq_INST_0_i_3_n_0),
        .I3(\ADDER_EN[1].trigack_pending_i_3_n_0 ),
        .I4(limit_irq_INST_0_i_2_n_0),
        .I5(trigack_pending),
        .O(\ADDER_EN[0].trigack_pending_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000100000)) 
    \ADDER_EN[0].trigack_pending_i_2 
       (.I0(\ADDER_EN[1].ctr[1][0]_i_6_n_0 ),
        .I1(trigack_pending),
        .I2(pmu_trigin),
        .I3(phase[1]),
        .I4(phase[0]),
        .I5(limit_irq_INST_0_i_4_n_0),
        .O(\ADDER_EN[0].trigack_pending_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ADDER_EN[0].trigack_pending_reg 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ADDER_EN[0].trigack_pending_i_1_n_0 ),
        .Q(trigack_pending),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h0004)) 
    \ADDER_EN[1].ctr[1][0]_i_1 
       (.I0(limit_irq_INST_0_i_2_n_0),
        .I1(\ADDER_EN[1].ctr[1][0]_i_4_n_0 ),
        .I2(limit_irq_INST_0_i_4_n_0),
        .I3(limit_irq_INST_0_i_3_n_0),
        .O(p_13_out));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ADDER_EN[1].ctr[1][0]_i_3 
       (.I0(limit_irq_INST_0_i_2_n_0),
        .I1(phase[1]),
        .I2(phase[0]),
        .I3(\ADDER_EN[1].ctr[1][0]_i_6_n_0 ),
        .I4(limit_irq_INST_0_i_4_n_0),
        .I5(limit_irq_INST_0_i_3_n_0),
        .O(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \ADDER_EN[1].ctr[1][0]_i_4 
       (.I0(phase[1]),
        .I1(phase[0]),
        .I2(pmu_trigin),
        .I3(\ADDER_EN[1].trigack_pending_reg_n_0 ),
        .I4(phase[3]),
        .I5(phase[2]),
        .O(\ADDER_EN[1].ctr[1][0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ADDER_EN[1].ctr[1][0]_i_5 
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [0]),
        .O(\ADDER_EN[1].ctr[1][0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \ADDER_EN[1].ctr[1][0]_i_6 
       (.I0(phase[2]),
        .I1(phase[3]),
        .O(\ADDER_EN[1].ctr[1][0]_i_6_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][0] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][0]_i_2_n_15 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [0]));
  CARRY8 \ADDER_EN[1].ctr_reg[1][0]_i_2 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\ADDER_EN[1].ctr_reg[1][0]_i_2_n_0 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_1 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_2 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_3 ,\NLW_ADDER_EN[1].ctr_reg[1][0]_i_2_CO_UNCONNECTED [3],\ADDER_EN[1].ctr_reg[1][0]_i_2_n_5 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_6 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\ADDER_EN[1].ctr_reg[1][0]_i_2_n_8 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_9 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_10 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_11 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_12 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_13 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_14 ,\ADDER_EN[1].ctr_reg[1][0]_i_2_n_15 }),
        .S({\ADDER_EN[1].ctr_reg[1]_0 [7:1],\ADDER_EN[1].ctr[1][0]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][10] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][8]_i_1_n_13 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [10]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][11] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][8]_i_1_n_12 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [11]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][12] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][8]_i_1_n_11 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [12]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][13] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][8]_i_1_n_10 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [13]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][14] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][8]_i_1_n_9 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [14]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][15] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][8]_i_1_n_8 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [15]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][16] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][16]_i_1_n_15 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [16]));
  CARRY8 \ADDER_EN[1].ctr_reg[1][16]_i_1 
       (.CI(\ADDER_EN[1].ctr_reg[1][8]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\ADDER_EN[1].ctr_reg[1][16]_i_1_n_0 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_1 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_2 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_3 ,\NLW_ADDER_EN[1].ctr_reg[1][16]_i_1_CO_UNCONNECTED [3],\ADDER_EN[1].ctr_reg[1][16]_i_1_n_5 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_6 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ADDER_EN[1].ctr_reg[1][16]_i_1_n_8 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_9 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_10 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_11 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_12 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_13 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_14 ,\ADDER_EN[1].ctr_reg[1][16]_i_1_n_15 }),
        .S(\ADDER_EN[1].ctr_reg[1]_0 [23:16]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][17] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][16]_i_1_n_14 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [17]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][18] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][16]_i_1_n_13 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [18]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][19] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][16]_i_1_n_12 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [19]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][1] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][0]_i_2_n_14 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][20] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][16]_i_1_n_11 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [20]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][21] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][16]_i_1_n_10 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [21]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][22] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][16]_i_1_n_9 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [22]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][23] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][16]_i_1_n_8 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [23]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][24] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][24]_i_1_n_15 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [24]));
  CARRY8 \ADDER_EN[1].ctr_reg[1][24]_i_1 
       (.CI(\ADDER_EN[1].ctr_reg[1][16]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_ADDER_EN[1].ctr_reg[1][24]_i_1_CO_UNCONNECTED [7],\ADDER_EN[1].ctr_reg[1][24]_i_1_n_1 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_2 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_3 ,\NLW_ADDER_EN[1].ctr_reg[1][24]_i_1_CO_UNCONNECTED [3],\ADDER_EN[1].ctr_reg[1][24]_i_1_n_5 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_6 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ADDER_EN[1].ctr_reg[1][24]_i_1_n_8 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_9 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_10 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_11 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_12 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_13 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_14 ,\ADDER_EN[1].ctr_reg[1][24]_i_1_n_15 }),
        .S(\ADDER_EN[1].ctr_reg[1]_0 [31:24]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][25] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][24]_i_1_n_14 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [25]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][26] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][24]_i_1_n_13 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [26]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][27] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][24]_i_1_n_12 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [27]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][28] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][24]_i_1_n_11 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [28]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][29] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][24]_i_1_n_10 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [29]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][2] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][0]_i_2_n_13 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [2]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][30] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][24]_i_1_n_9 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [30]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][31] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][24]_i_1_n_8 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [31]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][3] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][0]_i_2_n_12 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [3]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][4] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][0]_i_2_n_11 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [4]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][5] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][0]_i_2_n_10 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [5]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][6] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][0]_i_2_n_9 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [6]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][7] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][0]_i_2_n_8 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [7]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][8] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][8]_i_1_n_15 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [8]));
  CARRY8 \ADDER_EN[1].ctr_reg[1][8]_i_1 
       (.CI(\ADDER_EN[1].ctr_reg[1][0]_i_2_n_0 ),
        .CI_TOP(1'b0),
        .CO({\ADDER_EN[1].ctr_reg[1][8]_i_1_n_0 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_1 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_2 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_3 ,\NLW_ADDER_EN[1].ctr_reg[1][8]_i_1_CO_UNCONNECTED [3],\ADDER_EN[1].ctr_reg[1][8]_i_1_n_5 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_6 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ADDER_EN[1].ctr_reg[1][8]_i_1_n_8 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_9 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_10 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_11 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_12 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_13 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_14 ,\ADDER_EN[1].ctr_reg[1][8]_i_1_n_15 }),
        .S(\ADDER_EN[1].ctr_reg[1]_0 [15:8]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].ctr_reg[1][9] 
       (.C(s00_axi_aclk),
        .CE(p_13_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[1].ctr_reg[1][8]_i_1_n_14 ),
        .Q(\ADDER_EN[1].ctr_reg[1]_0 [9]));
  LUT6 #(
    .INIT(64'hCCCCCFCECCCCC0CE)) 
    \ADDER_EN[1].trigack_pending_i_1 
       (.I0(\ADDER_EN[1].trigack_pending_i_2_n_0 ),
        .I1(pmu_trigin),
        .I2(limit_irq_INST_0_i_3_n_0),
        .I3(\ADDER_EN[1].trigack_pending_i_3_n_0 ),
        .I4(limit_irq_INST_0_i_2_n_0),
        .I5(\ADDER_EN[1].trigack_pending_reg_n_0 ),
        .O(\ADDER_EN[1].trigack_pending_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000100000)) 
    \ADDER_EN[1].trigack_pending_i_2 
       (.I0(\ADDER_EN[1].ctr[1][0]_i_6_n_0 ),
        .I1(\ADDER_EN[1].trigack_pending_reg_n_0 ),
        .I2(pmu_trigin),
        .I3(phase[0]),
        .I4(phase[1]),
        .I5(limit_irq_INST_0_i_4_n_0),
        .O(\ADDER_EN[1].trigack_pending_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \ADDER_EN[1].trigack_pending_i_3 
       (.I0(phase[1]),
        .I1(phase[0]),
        .I2(phase[3]),
        .I3(phase[2]),
        .I4(limit_irq_INST_0_i_4_n_0),
        .O(\ADDER_EN[1].trigack_pending_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ADDER_EN[1].trigack_pending_reg 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ADDER_EN[1].trigack_pending_i_1_n_0 ),
        .Q(\ADDER_EN[1].trigack_pending_reg_n_0 ),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h0004)) 
    \ADDER_EN[2].ctr[2][0]_i_1 
       (.I0(limit_irq_INST_0_i_2_n_0),
        .I1(\ADDER_EN[2].ctr[2][0]_i_3_n_0 ),
        .I2(limit_irq_INST_0_i_4_n_0),
        .I3(limit_irq_INST_0_i_3_n_0),
        .O(p_10_out));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    \ADDER_EN[2].ctr[2][0]_i_3 
       (.I0(phase[0]),
        .I1(phase[1]),
        .I2(pmu_trigin),
        .I3(\ADDER_EN[2].trigack_pending_reg_n_0 ),
        .I4(phase[3]),
        .I5(phase[2]),
        .O(\ADDER_EN[2].ctr[2][0]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ADDER_EN[2].ctr[2][0]_i_4 
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [0]),
        .O(\ADDER_EN[2].ctr[2][0]_i_4_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][0] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][0]_i_2_n_15 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [0]));
  CARRY8 \ADDER_EN[2].ctr_reg[2][0]_i_2 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\ADDER_EN[2].ctr_reg[2][0]_i_2_n_0 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_1 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_2 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_3 ,\NLW_ADDER_EN[2].ctr_reg[2][0]_i_2_CO_UNCONNECTED [3],\ADDER_EN[2].ctr_reg[2][0]_i_2_n_5 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_6 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\ADDER_EN[2].ctr_reg[2][0]_i_2_n_8 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_9 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_10 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_11 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_12 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_13 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_14 ,\ADDER_EN[2].ctr_reg[2][0]_i_2_n_15 }),
        .S({\ADDER_EN[2].ctr_reg[2]_1 [7:1],\ADDER_EN[2].ctr[2][0]_i_4_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][10] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][8]_i_1_n_13 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [10]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][11] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][8]_i_1_n_12 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [11]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][12] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][8]_i_1_n_11 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [12]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][13] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][8]_i_1_n_10 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [13]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][14] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][8]_i_1_n_9 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [14]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][15] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][8]_i_1_n_8 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [15]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][16] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][16]_i_1_n_15 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [16]));
  CARRY8 \ADDER_EN[2].ctr_reg[2][16]_i_1 
       (.CI(\ADDER_EN[2].ctr_reg[2][8]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\ADDER_EN[2].ctr_reg[2][16]_i_1_n_0 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_1 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_2 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_3 ,\NLW_ADDER_EN[2].ctr_reg[2][16]_i_1_CO_UNCONNECTED [3],\ADDER_EN[2].ctr_reg[2][16]_i_1_n_5 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_6 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ADDER_EN[2].ctr_reg[2][16]_i_1_n_8 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_9 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_10 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_11 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_12 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_13 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_14 ,\ADDER_EN[2].ctr_reg[2][16]_i_1_n_15 }),
        .S(\ADDER_EN[2].ctr_reg[2]_1 [23:16]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][17] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][16]_i_1_n_14 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [17]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][18] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][16]_i_1_n_13 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [18]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][19] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][16]_i_1_n_12 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [19]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][1] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][0]_i_2_n_14 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][20] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][16]_i_1_n_11 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [20]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][21] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][16]_i_1_n_10 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [21]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][22] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][16]_i_1_n_9 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [22]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][23] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][16]_i_1_n_8 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [23]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][24] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][24]_i_1_n_15 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [24]));
  CARRY8 \ADDER_EN[2].ctr_reg[2][24]_i_1 
       (.CI(\ADDER_EN[2].ctr_reg[2][16]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_ADDER_EN[2].ctr_reg[2][24]_i_1_CO_UNCONNECTED [7],\ADDER_EN[2].ctr_reg[2][24]_i_1_n_1 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_2 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_3 ,\NLW_ADDER_EN[2].ctr_reg[2][24]_i_1_CO_UNCONNECTED [3],\ADDER_EN[2].ctr_reg[2][24]_i_1_n_5 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_6 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ADDER_EN[2].ctr_reg[2][24]_i_1_n_8 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_9 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_10 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_11 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_12 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_13 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_14 ,\ADDER_EN[2].ctr_reg[2][24]_i_1_n_15 }),
        .S(\ADDER_EN[2].ctr_reg[2]_1 [31:24]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][25] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][24]_i_1_n_14 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [25]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][26] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][24]_i_1_n_13 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [26]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][27] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][24]_i_1_n_12 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [27]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][28] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][24]_i_1_n_11 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [28]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][29] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][24]_i_1_n_10 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [29]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][2] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][0]_i_2_n_13 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [2]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][30] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][24]_i_1_n_9 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [30]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][31] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][24]_i_1_n_8 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [31]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][3] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][0]_i_2_n_12 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [3]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][4] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][0]_i_2_n_11 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [4]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][5] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][0]_i_2_n_10 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [5]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][6] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][0]_i_2_n_9 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [6]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][7] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][0]_i_2_n_8 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [7]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][8] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][8]_i_1_n_15 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [8]));
  CARRY8 \ADDER_EN[2].ctr_reg[2][8]_i_1 
       (.CI(\ADDER_EN[2].ctr_reg[2][0]_i_2_n_0 ),
        .CI_TOP(1'b0),
        .CO({\ADDER_EN[2].ctr_reg[2][8]_i_1_n_0 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_1 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_2 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_3 ,\NLW_ADDER_EN[2].ctr_reg[2][8]_i_1_CO_UNCONNECTED [3],\ADDER_EN[2].ctr_reg[2][8]_i_1_n_5 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_6 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\ADDER_EN[2].ctr_reg[2][8]_i_1_n_8 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_9 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_10 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_11 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_12 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_13 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_14 ,\ADDER_EN[2].ctr_reg[2][8]_i_1_n_15 }),
        .S(\ADDER_EN[2].ctr_reg[2]_1 [15:8]));
  FDCE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].ctr_reg[2][9] 
       (.C(s00_axi_aclk),
        .CE(p_10_out),
        .CLR(\ADDER_EN[1].ctr[1][0]_i_3_n_0 ),
        .D(\ADDER_EN[2].ctr_reg[2][8]_i_1_n_14 ),
        .Q(\ADDER_EN[2].ctr_reg[2]_1 [9]));
  LUT6 #(
    .INIT(64'hCCCCCFCECCCCC0CE)) 
    \ADDER_EN[2].trigack_pending_i_1 
       (.I0(\ADDER_EN[2].trigack_pending_i_2_n_0 ),
        .I1(pmu_trigin),
        .I2(limit_irq_INST_0_i_3_n_0),
        .I3(\ADDER_EN[1].trigack_pending_i_3_n_0 ),
        .I4(limit_irq_INST_0_i_2_n_0),
        .I5(\ADDER_EN[2].trigack_pending_reg_n_0 ),
        .O(\ADDER_EN[2].trigack_pending_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \ADDER_EN[2].trigack_pending_i_2 
       (.I0(\ADDER_EN[1].ctr[1][0]_i_6_n_0 ),
        .I1(\ADDER_EN[2].trigack_pending_reg_n_0 ),
        .I2(pmu_trigin),
        .I3(phase[1]),
        .I4(phase[0]),
        .I5(limit_irq_INST_0_i_4_n_0),
        .O(\ADDER_EN[2].trigack_pending_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ADDER_EN[2].trigack_pending_reg 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ADDER_EN[2].trigack_pending_i_1_n_0 ),
        .Q(\ADDER_EN[2].trigack_pending_reg_n_0 ),
        .R(1'b0));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .S(p_0_in));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .S(p_0_in));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .S(p_0_in));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(axi_awaddr[2]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(axi_awaddr[3]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(axi_awaddr[4]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(p_0_in));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(p_0_in));
  LUT3 #(
    .INIT(8'h3A)) 
    axi_bvalid_i_1
       (.I0(write_finish),
        .I1(s00_axi_bready),
        .I2(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[0]_i_2 
       (.I0(pref_limit[0]),
        .I1(wb_limit[0]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[0]),
        .I5(comp_limit[0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[0]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [0]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [0]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [0]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[10]_i_2 
       (.I0(pref_limit[10]),
        .I1(wb_limit[10]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[10]),
        .I5(comp_limit[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[10]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [10]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [10]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [10]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[11]_i_2 
       (.I0(pref_limit[11]),
        .I1(wb_limit[11]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[11]),
        .I5(comp_limit[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[11]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [11]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [11]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [11]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[12]_i_2 
       (.I0(pref_limit[12]),
        .I1(wb_limit[12]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[12]),
        .I5(comp_limit[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[12]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [12]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [12]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [12]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[13]_i_2 
       (.I0(pref_limit[13]),
        .I1(wb_limit[13]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[13]),
        .I5(comp_limit[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[13]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [13]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [13]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [13]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[14]_i_2 
       (.I0(pref_limit[14]),
        .I1(wb_limit[14]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[14]),
        .I5(comp_limit[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[14]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [14]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [14]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [14]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[15]_i_2 
       (.I0(pref_limit[15]),
        .I1(wb_limit[15]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[15]),
        .I5(comp_limit[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[15]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [15]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [15]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [15]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[16]_i_2 
       (.I0(pref_limit[16]),
        .I1(wb_limit[16]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[16]),
        .I5(comp_limit[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[16]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [16]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [16]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [16]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[17]_i_2 
       (.I0(pref_limit[17]),
        .I1(wb_limit[17]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[17]),
        .I5(comp_limit[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[17]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [17]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [17]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [17]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[18]_i_2 
       (.I0(pref_limit[18]),
        .I1(wb_limit[18]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[18]),
        .I5(comp_limit[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[18]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [18]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [18]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [18]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[19]_i_2 
       (.I0(pref_limit[19]),
        .I1(wb_limit[19]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[19]),
        .I5(comp_limit[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[19]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [19]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [19]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [19]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[1]_i_2 
       (.I0(pref_limit[1]),
        .I1(wb_limit[1]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[1]),
        .I5(comp_limit[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[1]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [1]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [1]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [1]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[20]_i_2 
       (.I0(pref_limit[20]),
        .I1(wb_limit[20]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[20]),
        .I5(comp_limit[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[20]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [20]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [20]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [20]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[21]_i_2 
       (.I0(pref_limit[21]),
        .I1(wb_limit[21]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[21]),
        .I5(comp_limit[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[21]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [21]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [21]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [21]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[22]_i_2 
       (.I0(pref_limit[22]),
        .I1(wb_limit[22]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[22]),
        .I5(comp_limit[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[22]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [22]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [22]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [22]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[23]_i_2 
       (.I0(pref_limit[23]),
        .I1(wb_limit[23]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[23]),
        .I5(comp_limit[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[23]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [23]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [23]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [23]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[24]_i_2 
       (.I0(pref_limit[24]),
        .I1(wb_limit[24]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[24]),
        .I5(comp_limit[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[24]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [24]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [24]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [24]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[25]_i_2 
       (.I0(pref_limit[25]),
        .I1(wb_limit[25]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[25]),
        .I5(comp_limit[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[25]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [25]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [25]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [25]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[26]_i_2 
       (.I0(pref_limit[26]),
        .I1(wb_limit[26]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[26]),
        .I5(comp_limit[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[26]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [26]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [26]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [26]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[27]_i_2 
       (.I0(pref_limit[27]),
        .I1(wb_limit[27]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[27]),
        .I5(comp_limit[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[27]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [27]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [27]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [27]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[28]_i_2 
       (.I0(pref_limit[28]),
        .I1(wb_limit[28]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[28]),
        .I5(comp_limit[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[28]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [28]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [28]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [28]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[29]_i_2 
       (.I0(pref_limit[29]),
        .I1(wb_limit[29]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[29]),
        .I5(comp_limit[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[29]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [29]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [29]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [29]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[2]_i_2 
       (.I0(pref_limit[2]),
        .I1(wb_limit[2]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[2]),
        .I5(comp_limit[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[2]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [2]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [2]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [2]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[30]_i_2 
       (.I0(pref_limit[30]),
        .I1(wb_limit[30]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[30]),
        .I5(comp_limit[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[30]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [30]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [30]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [30]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[31]_i_3 
       (.I0(pref_limit[31]),
        .I1(wb_limit[31]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[31]),
        .I5(comp_limit[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[31]_i_4 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [31]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [31]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [31]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[3]_i_2 
       (.I0(pref_limit[3]),
        .I1(wb_limit[3]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[3]),
        .I5(comp_limit[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[3]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [3]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [3]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [3]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[4]_i_2 
       (.I0(pref_limit[4]),
        .I1(wb_limit[4]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[4]),
        .I5(comp_limit[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[4]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [4]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [4]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [4]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[5]_i_2 
       (.I0(pref_limit[5]),
        .I1(wb_limit[5]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[5]),
        .I5(comp_limit[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[5]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [5]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [5]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [5]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[6]_i_2 
       (.I0(pref_limit[6]),
        .I1(wb_limit[6]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[6]),
        .I5(comp_limit[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[6]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [6]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [6]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [6]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[7]_i_2 
       (.I0(pref_limit[7]),
        .I1(wb_limit[7]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[7]),
        .I5(comp_limit[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[7]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [7]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [7]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [7]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[8]_i_2 
       (.I0(pref_limit[8]),
        .I1(wb_limit[8]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[8]),
        .I5(comp_limit[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[8]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [8]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [8]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [8]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[9]_i_2 
       (.I0(pref_limit[9]),
        .I1(wb_limit[9]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(phase[9]),
        .I5(comp_limit[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00CCF0AA)) 
    \axi_rdata[9]_i_3 
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [9]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [9]),
        .I2(\ADDER_EN[1].ctr_reg[1]_0 [9]),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(\axi_rdata[0]_i_3_n_0 ),
        .O(reg_data_out[0]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(\axi_rdata[10]_i_3_n_0 ),
        .O(reg_data_out[10]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(\axi_rdata[11]_i_3_n_0 ),
        .O(reg_data_out[11]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(\axi_rdata[12]_i_3_n_0 ),
        .O(reg_data_out[12]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(\axi_rdata[13]_i_3_n_0 ),
        .O(reg_data_out[13]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(\axi_rdata[14]_i_3_n_0 ),
        .O(reg_data_out[14]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata[15]_i_3_n_0 ),
        .O(reg_data_out[15]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(\axi_rdata[16]_i_3_n_0 ),
        .O(reg_data_out[16]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(\axi_rdata[17]_i_3_n_0 ),
        .O(reg_data_out[17]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(\axi_rdata[18]_i_3_n_0 ),
        .O(reg_data_out[18]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(\axi_rdata[19]_i_3_n_0 ),
        .O(reg_data_out[19]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(\axi_rdata[1]_i_3_n_0 ),
        .O(reg_data_out[1]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(\axi_rdata[20]_i_3_n_0 ),
        .O(reg_data_out[20]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(\axi_rdata[21]_i_3_n_0 ),
        .O(reg_data_out[21]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(\axi_rdata[22]_i_3_n_0 ),
        .O(reg_data_out[22]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(\axi_rdata[23]_i_3_n_0 ),
        .O(reg_data_out[23]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(\axi_rdata[24]_i_3_n_0 ),
        .O(reg_data_out[24]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(\axi_rdata[25]_i_3_n_0 ),
        .O(reg_data_out[25]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(\axi_rdata[26]_i_3_n_0 ),
        .O(reg_data_out[26]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(\axi_rdata[27]_i_3_n_0 ),
        .O(reg_data_out[27]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(\axi_rdata[28]_i_3_n_0 ),
        .O(reg_data_out[28]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(\axi_rdata[29]_i_3_n_0 ),
        .O(reg_data_out[29]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(\axi_rdata[2]_i_3_n_0 ),
        .O(reg_data_out[2]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(\axi_rdata[30]_i_3_n_0 ),
        .O(reg_data_out[30]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[31]_i_2 
       (.I0(\axi_rdata[31]_i_3_n_0 ),
        .I1(\axi_rdata[31]_i_4_n_0 ),
        .O(reg_data_out[31]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(\axi_rdata[3]_i_3_n_0 ),
        .O(reg_data_out[3]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(\axi_rdata[4]_i_3_n_0 ),
        .O(reg_data_out[4]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(\axi_rdata[5]_i_3_n_0 ),
        .O(reg_data_out[5]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(\axi_rdata[6]_i_3_n_0 ),
        .O(reg_data_out[6]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(\axi_rdata[7]_i_3_n_0 ),
        .O(reg_data_out[7]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(\axi_rdata[8]_i_3_n_0 ),
        .O(reg_data_out[8]),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(\axi_rdata[9]_i_3_n_0 ),
        .O(reg_data_out[9]),
        .S(sel0[2]));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'h02000000)) 
    \comp_limit[15]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[1]),
        .I4(axi_awaddr[3]),
        .O(\comp_limit[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \comp_limit[23]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[2]),
        .I4(axi_awaddr[3]),
        .O(\comp_limit[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \comp_limit[31]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[3]),
        .I4(axi_awaddr[3]),
        .O(\comp_limit[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \comp_limit[7]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[0]),
        .I4(axi_awaddr[3]),
        .O(\comp_limit[7]_i_1_n_0 ));
  FDRE \comp_limit_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(comp_limit[0]),
        .R(p_0_in));
  FDRE \comp_limit_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(comp_limit[10]),
        .R(p_0_in));
  FDRE \comp_limit_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(comp_limit[11]),
        .R(p_0_in));
  FDRE \comp_limit_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(comp_limit[12]),
        .R(p_0_in));
  FDRE \comp_limit_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(comp_limit[13]),
        .R(p_0_in));
  FDRE \comp_limit_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(comp_limit[14]),
        .R(p_0_in));
  FDRE \comp_limit_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(comp_limit[15]),
        .R(p_0_in));
  FDRE \comp_limit_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(comp_limit[16]),
        .R(p_0_in));
  FDRE \comp_limit_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(comp_limit[17]),
        .R(p_0_in));
  FDRE \comp_limit_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(comp_limit[18]),
        .R(p_0_in));
  FDRE \comp_limit_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(comp_limit[19]),
        .R(p_0_in));
  FDRE \comp_limit_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(comp_limit[1]),
        .R(p_0_in));
  FDRE \comp_limit_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(comp_limit[20]),
        .R(p_0_in));
  FDRE \comp_limit_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(comp_limit[21]),
        .R(p_0_in));
  FDRE \comp_limit_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(comp_limit[22]),
        .R(p_0_in));
  FDRE \comp_limit_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(comp_limit[23]),
        .R(p_0_in));
  FDRE \comp_limit_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(comp_limit[24]),
        .R(p_0_in));
  FDRE \comp_limit_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(comp_limit[25]),
        .R(p_0_in));
  FDRE \comp_limit_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(comp_limit[26]),
        .R(p_0_in));
  FDRE \comp_limit_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(comp_limit[27]),
        .R(p_0_in));
  FDRE \comp_limit_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(comp_limit[28]),
        .R(p_0_in));
  FDRE \comp_limit_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(comp_limit[29]),
        .R(p_0_in));
  FDRE \comp_limit_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(comp_limit[2]),
        .R(p_0_in));
  FDRE \comp_limit_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(comp_limit[30]),
        .R(p_0_in));
  FDRE \comp_limit_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(comp_limit[31]),
        .R(p_0_in));
  FDRE \comp_limit_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(comp_limit[3]),
        .R(p_0_in));
  FDRE \comp_limit_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(comp_limit[4]),
        .R(p_0_in));
  FDRE \comp_limit_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(comp_limit[5]),
        .R(p_0_in));
  FDRE \comp_limit_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(comp_limit[6]),
        .R(p_0_in));
  FDRE \comp_limit_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(comp_limit[7]),
        .R(p_0_in));
  FDRE \comp_limit_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(comp_limit[8]),
        .R(p_0_in));
  FDRE \comp_limit_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\comp_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(comp_limit[9]),
        .R(p_0_in));
  CARRY8 gtOp_carry
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({gtOp_carry_n_0,gtOp_carry_n_1,gtOp_carry_n_2,gtOp_carry_n_3,NLW_gtOp_carry_CO_UNCONNECTED[3],gtOp_carry_n_5,gtOp_carry_n_6,gtOp_carry_n_7}),
        .DI({gtOp_carry_i_1_n_0,gtOp_carry_i_2_n_0,gtOp_carry_i_3_n_0,gtOp_carry_i_4_n_0,gtOp_carry_i_5_n_0,gtOp_carry_i_6_n_0,gtOp_carry_i_7_n_0,gtOp_carry_i_8_n_0}),
        .O(NLW_gtOp_carry_O_UNCONNECTED[7:0]),
        .S({gtOp_carry_i_9_n_0,gtOp_carry_i_10_n_0,gtOp_carry_i_11_n_0,gtOp_carry_i_12_n_0,gtOp_carry_i_13_n_0,gtOp_carry_i_14_n_0,gtOp_carry_i_15_n_0,gtOp_carry_i_16_n_0}));
  CARRY8 gtOp_carry__0
       (.CI(gtOp_carry_n_0),
        .CI_TOP(1'b0),
        .CO({gtOp_carry__0_n_0,gtOp_carry__0_n_1,gtOp_carry__0_n_2,gtOp_carry__0_n_3,NLW_gtOp_carry__0_CO_UNCONNECTED[3],gtOp_carry__0_n_5,gtOp_carry__0_n_6,gtOp_carry__0_n_7}),
        .DI({gtOp_carry__0_i_1_n_0,gtOp_carry__0_i_2_n_0,gtOp_carry__0_i_3_n_0,gtOp_carry__0_i_4_n_0,gtOp_carry__0_i_5_n_0,gtOp_carry__0_i_6_n_0,gtOp_carry__0_i_7_n_0,gtOp_carry__0_i_8_n_0}),
        .O(NLW_gtOp_carry__0_O_UNCONNECTED[7:0]),
        .S({gtOp_carry__0_i_9_n_0,gtOp_carry__0_i_10_n_0,gtOp_carry__0_i_11_n_0,gtOp_carry__0_i_12_n_0,gtOp_carry__0_i_13_n_0,gtOp_carry__0_i_14_n_0,gtOp_carry__0_i_15_n_0,gtOp_carry__0_i_16_n_0}));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry__0_i_1
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [30]),
        .I1(comp_limit[30]),
        .I2(comp_limit[31]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [31]),
        .O(gtOp_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry__0_i_10
       (.I0(comp_limit[29]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [29]),
        .I2(comp_limit[28]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [28]),
        .O(gtOp_carry__0_i_10_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry__0_i_11
       (.I0(comp_limit[27]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [27]),
        .I2(comp_limit[26]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [26]),
        .O(gtOp_carry__0_i_11_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry__0_i_12
       (.I0(comp_limit[25]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [25]),
        .I2(comp_limit[24]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [24]),
        .O(gtOp_carry__0_i_12_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry__0_i_13
       (.I0(comp_limit[23]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [23]),
        .I2(comp_limit[22]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [22]),
        .O(gtOp_carry__0_i_13_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry__0_i_14
       (.I0(comp_limit[21]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [21]),
        .I2(comp_limit[20]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [20]),
        .O(gtOp_carry__0_i_14_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry__0_i_15
       (.I0(comp_limit[19]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [19]),
        .I2(comp_limit[18]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [18]),
        .O(gtOp_carry__0_i_15_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry__0_i_16
       (.I0(comp_limit[17]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [17]),
        .I2(comp_limit[16]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [16]),
        .O(gtOp_carry__0_i_16_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry__0_i_2
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [28]),
        .I1(comp_limit[28]),
        .I2(comp_limit[29]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [29]),
        .O(gtOp_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry__0_i_3
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [26]),
        .I1(comp_limit[26]),
        .I2(comp_limit[27]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [27]),
        .O(gtOp_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry__0_i_4
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [24]),
        .I1(comp_limit[24]),
        .I2(comp_limit[25]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [25]),
        .O(gtOp_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry__0_i_5
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [22]),
        .I1(comp_limit[22]),
        .I2(comp_limit[23]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [23]),
        .O(gtOp_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry__0_i_6
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [20]),
        .I1(comp_limit[20]),
        .I2(comp_limit[21]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [21]),
        .O(gtOp_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry__0_i_7
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [18]),
        .I1(comp_limit[18]),
        .I2(comp_limit[19]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [19]),
        .O(gtOp_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry__0_i_8
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [16]),
        .I1(comp_limit[16]),
        .I2(comp_limit[17]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [17]),
        .O(gtOp_carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry__0_i_9
       (.I0(comp_limit[31]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [31]),
        .I2(comp_limit[30]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [30]),
        .O(gtOp_carry__0_i_9_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry_i_1
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [14]),
        .I1(comp_limit[14]),
        .I2(comp_limit[15]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [15]),
        .O(gtOp_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry_i_10
       (.I0(comp_limit[13]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [13]),
        .I2(comp_limit[12]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [12]),
        .O(gtOp_carry_i_10_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry_i_11
       (.I0(comp_limit[11]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [11]),
        .I2(comp_limit[10]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [10]),
        .O(gtOp_carry_i_11_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry_i_12
       (.I0(comp_limit[9]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [9]),
        .I2(comp_limit[8]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [8]),
        .O(gtOp_carry_i_12_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry_i_13
       (.I0(comp_limit[7]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [7]),
        .I2(comp_limit[6]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [6]),
        .O(gtOp_carry_i_13_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry_i_14
       (.I0(comp_limit[5]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [5]),
        .I2(comp_limit[4]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [4]),
        .O(gtOp_carry_i_14_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry_i_15
       (.I0(comp_limit[3]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [3]),
        .I2(comp_limit[2]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [2]),
        .O(gtOp_carry_i_15_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry_i_16
       (.I0(comp_limit[1]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [1]),
        .I2(comp_limit[0]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [0]),
        .O(gtOp_carry_i_16_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry_i_2
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [12]),
        .I1(comp_limit[12]),
        .I2(comp_limit[13]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [13]),
        .O(gtOp_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry_i_3
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [10]),
        .I1(comp_limit[10]),
        .I2(comp_limit[11]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [11]),
        .O(gtOp_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry_i_4
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [8]),
        .I1(comp_limit[8]),
        .I2(comp_limit[9]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [9]),
        .O(gtOp_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry_i_5
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [6]),
        .I1(comp_limit[6]),
        .I2(comp_limit[7]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [7]),
        .O(gtOp_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry_i_6
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [4]),
        .I1(comp_limit[4]),
        .I2(comp_limit[5]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [5]),
        .O(gtOp_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry_i_7
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [2]),
        .I1(comp_limit[2]),
        .I2(comp_limit[3]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [3]),
        .O(gtOp_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    gtOp_carry_i_8
       (.I0(\ADDER_EN[1].ctr_reg[1]_0 [0]),
        .I1(comp_limit[0]),
        .I2(comp_limit[1]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [1]),
        .O(gtOp_carry_i_8_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    gtOp_carry_i_9
       (.I0(comp_limit[15]),
        .I1(\ADDER_EN[1].ctr_reg[1]_0 [15]),
        .I2(comp_limit[14]),
        .I3(\ADDER_EN[1].ctr_reg[1]_0 [14]),
        .O(gtOp_carry_i_9_n_0));
  CARRY8 \gtOp_inferred__0/i__carry 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\gtOp_inferred__0/i__carry_n_0 ,\gtOp_inferred__0/i__carry_n_1 ,\gtOp_inferred__0/i__carry_n_2 ,\gtOp_inferred__0/i__carry_n_3 ,\NLW_gtOp_inferred__0/i__carry_CO_UNCONNECTED [3],\gtOp_inferred__0/i__carry_n_5 ,\gtOp_inferred__0/i__carry_n_6 ,\gtOp_inferred__0/i__carry_n_7 }),
        .DI({i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0,i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0}),
        .O(\NLW_gtOp_inferred__0/i__carry_O_UNCONNECTED [7:0]),
        .S({i__carry_i_9_n_0,i__carry_i_10_n_0,i__carry_i_11_n_0,i__carry_i_12_n_0,i__carry_i_13_n_0,i__carry_i_14_n_0,i__carry_i_15_n_0,i__carry_i_16_n_0}));
  CARRY8 \gtOp_inferred__0/i__carry__0 
       (.CI(\gtOp_inferred__0/i__carry_n_0 ),
        .CI_TOP(1'b0),
        .CO({\gtOp_inferred__0/i__carry__0_n_0 ,\gtOp_inferred__0/i__carry__0_n_1 ,\gtOp_inferred__0/i__carry__0_n_2 ,\gtOp_inferred__0/i__carry__0_n_3 ,\NLW_gtOp_inferred__0/i__carry__0_CO_UNCONNECTED [3],\gtOp_inferred__0/i__carry__0_n_5 ,\gtOp_inferred__0/i__carry__0_n_6 ,\gtOp_inferred__0/i__carry__0_n_7 }),
        .DI({i__carry__0_i_1_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4_n_0,i__carry__0_i_5_n_0,i__carry__0_i_6_n_0,i__carry__0_i_7_n_0,i__carry__0_i_8_n_0}),
        .O(\NLW_gtOp_inferred__0/i__carry__0_O_UNCONNECTED [7:0]),
        .S({i__carry__0_i_9_n_0,i__carry__0_i_10_n_0,i__carry__0_i_11_n_0,i__carry__0_i_12_n_0,i__carry__0_i_13_n_0,i__carry__0_i_14_n_0,i__carry__0_i_15_n_0,i__carry__0_i_16_n_0}));
  CARRY8 \gtOp_inferred__1/i__carry 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\gtOp_inferred__1/i__carry_n_0 ,\gtOp_inferred__1/i__carry_n_1 ,\gtOp_inferred__1/i__carry_n_2 ,\gtOp_inferred__1/i__carry_n_3 ,\NLW_gtOp_inferred__1/i__carry_CO_UNCONNECTED [3],\gtOp_inferred__1/i__carry_n_5 ,\gtOp_inferred__1/i__carry_n_6 ,\gtOp_inferred__1/i__carry_n_7 }),
        .DI({i__carry_i_1__0_n_0,i__carry_i_2__0_n_0,i__carry_i_3__0_n_0,i__carry_i_4__0_n_0,i__carry_i_5__0_n_0,i__carry_i_6__0_n_0,i__carry_i_7__0_n_0,i__carry_i_8__0_n_0}),
        .O(\NLW_gtOp_inferred__1/i__carry_O_UNCONNECTED [7:0]),
        .S({i__carry_i_9__0_n_0,i__carry_i_10__0_n_0,i__carry_i_11__0_n_0,i__carry_i_12__0_n_0,i__carry_i_13__0_n_0,i__carry_i_14__0_n_0,i__carry_i_15__0_n_0,i__carry_i_16__0_n_0}));
  CARRY8 \gtOp_inferred__1/i__carry__0 
       (.CI(\gtOp_inferred__1/i__carry_n_0 ),
        .CI_TOP(1'b0),
        .CO({gtOp,\gtOp_inferred__1/i__carry__0_n_1 ,\gtOp_inferred__1/i__carry__0_n_2 ,\gtOp_inferred__1/i__carry__0_n_3 ,\NLW_gtOp_inferred__1/i__carry__0_CO_UNCONNECTED [3],\gtOp_inferred__1/i__carry__0_n_5 ,\gtOp_inferred__1/i__carry__0_n_6 ,\gtOp_inferred__1/i__carry__0_n_7 }),
        .DI({i__carry__0_i_1__0_n_0,i__carry__0_i_2__0_n_0,i__carry__0_i_3__0_n_0,i__carry__0_i_4__0_n_0,i__carry__0_i_5__0_n_0,i__carry__0_i_6__0_n_0,i__carry__0_i_7__0_n_0,i__carry__0_i_8__0_n_0}),
        .O(\NLW_gtOp_inferred__1/i__carry__0_O_UNCONNECTED [7:0]),
        .S({i__carry__0_i_9__0_n_0,i__carry__0_i_10__0_n_0,i__carry__0_i_11__0_n_0,i__carry__0_i_12__0_n_0,i__carry__0_i_13__0_n_0,i__carry__0_i_14__0_n_0,i__carry__0_i_15__0_n_0,i__carry__0_i_16__0_n_0}));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_1
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [30]),
        .I1(wb_limit[30]),
        .I2(wb_limit[31]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [31]),
        .O(i__carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_10
       (.I0(wb_limit[29]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [29]),
        .I2(wb_limit[28]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [28]),
        .O(i__carry__0_i_10_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_10__0
       (.I0(pref_limit[29]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [29]),
        .I2(pref_limit[28]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [28]),
        .O(i__carry__0_i_10__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_11
       (.I0(wb_limit[27]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [27]),
        .I2(wb_limit[26]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [26]),
        .O(i__carry__0_i_11_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_11__0
       (.I0(pref_limit[27]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [27]),
        .I2(pref_limit[26]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [26]),
        .O(i__carry__0_i_11__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_12
       (.I0(wb_limit[25]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [25]),
        .I2(wb_limit[24]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [24]),
        .O(i__carry__0_i_12_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_12__0
       (.I0(pref_limit[25]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [25]),
        .I2(pref_limit[24]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [24]),
        .O(i__carry__0_i_12__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_13
       (.I0(wb_limit[23]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [23]),
        .I2(wb_limit[22]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [22]),
        .O(i__carry__0_i_13_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_13__0
       (.I0(pref_limit[23]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [23]),
        .I2(pref_limit[22]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [22]),
        .O(i__carry__0_i_13__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_14
       (.I0(wb_limit[21]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [21]),
        .I2(wb_limit[20]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [20]),
        .O(i__carry__0_i_14_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_14__0
       (.I0(pref_limit[21]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [21]),
        .I2(pref_limit[20]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [20]),
        .O(i__carry__0_i_14__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_15
       (.I0(wb_limit[19]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [19]),
        .I2(wb_limit[18]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [18]),
        .O(i__carry__0_i_15_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_15__0
       (.I0(pref_limit[19]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [19]),
        .I2(pref_limit[18]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [18]),
        .O(i__carry__0_i_15__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_16
       (.I0(wb_limit[17]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [17]),
        .I2(wb_limit[16]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [16]),
        .O(i__carry__0_i_16_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_16__0
       (.I0(pref_limit[17]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [17]),
        .I2(pref_limit[16]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [16]),
        .O(i__carry__0_i_16__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_1__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [30]),
        .I1(pref_limit[30]),
        .I2(pref_limit[31]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [31]),
        .O(i__carry__0_i_1__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_2
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [28]),
        .I1(wb_limit[28]),
        .I2(wb_limit[29]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [29]),
        .O(i__carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_2__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [28]),
        .I1(pref_limit[28]),
        .I2(pref_limit[29]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [29]),
        .O(i__carry__0_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_3
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [26]),
        .I1(wb_limit[26]),
        .I2(wb_limit[27]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [27]),
        .O(i__carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_3__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [26]),
        .I1(pref_limit[26]),
        .I2(pref_limit[27]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [27]),
        .O(i__carry__0_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_4
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [24]),
        .I1(wb_limit[24]),
        .I2(wb_limit[25]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [25]),
        .O(i__carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_4__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [24]),
        .I1(pref_limit[24]),
        .I2(pref_limit[25]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [25]),
        .O(i__carry__0_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_5
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [22]),
        .I1(wb_limit[22]),
        .I2(wb_limit[23]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [23]),
        .O(i__carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_5__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [22]),
        .I1(pref_limit[22]),
        .I2(pref_limit[23]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [23]),
        .O(i__carry__0_i_5__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_6
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [20]),
        .I1(wb_limit[20]),
        .I2(wb_limit[21]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [21]),
        .O(i__carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_6__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [20]),
        .I1(pref_limit[20]),
        .I2(pref_limit[21]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [21]),
        .O(i__carry__0_i_6__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_7
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [18]),
        .I1(wb_limit[18]),
        .I2(wb_limit[19]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [19]),
        .O(i__carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_7__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [18]),
        .I1(pref_limit[18]),
        .I2(pref_limit[19]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [19]),
        .O(i__carry__0_i_7__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_8
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [16]),
        .I1(wb_limit[16]),
        .I2(wb_limit[17]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [17]),
        .O(i__carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_8__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [16]),
        .I1(pref_limit[16]),
        .I2(pref_limit[17]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [17]),
        .O(i__carry__0_i_8__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_9
       (.I0(wb_limit[31]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [31]),
        .I2(wb_limit[30]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [30]),
        .O(i__carry__0_i_9_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_9__0
       (.I0(pref_limit[31]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [31]),
        .I2(pref_limit[30]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [30]),
        .O(i__carry__0_i_9__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_1
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [14]),
        .I1(wb_limit[14]),
        .I2(wb_limit[15]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [15]),
        .O(i__carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_10
       (.I0(wb_limit[13]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [13]),
        .I2(wb_limit[12]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [12]),
        .O(i__carry_i_10_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_10__0
       (.I0(pref_limit[13]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [13]),
        .I2(pref_limit[12]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [12]),
        .O(i__carry_i_10__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_11
       (.I0(wb_limit[11]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [11]),
        .I2(wb_limit[10]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [10]),
        .O(i__carry_i_11_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_11__0
       (.I0(pref_limit[11]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [11]),
        .I2(pref_limit[10]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [10]),
        .O(i__carry_i_11__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_12
       (.I0(wb_limit[9]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [9]),
        .I2(wb_limit[8]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [8]),
        .O(i__carry_i_12_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_12__0
       (.I0(pref_limit[9]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [9]),
        .I2(pref_limit[8]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [8]),
        .O(i__carry_i_12__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_13
       (.I0(wb_limit[7]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [7]),
        .I2(wb_limit[6]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [6]),
        .O(i__carry_i_13_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_13__0
       (.I0(pref_limit[7]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [7]),
        .I2(pref_limit[6]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [6]),
        .O(i__carry_i_13__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_14
       (.I0(wb_limit[5]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [5]),
        .I2(wb_limit[4]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [4]),
        .O(i__carry_i_14_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_14__0
       (.I0(pref_limit[5]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [5]),
        .I2(pref_limit[4]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [4]),
        .O(i__carry_i_14__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_15
       (.I0(wb_limit[3]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [3]),
        .I2(wb_limit[2]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [2]),
        .O(i__carry_i_15_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_15__0
       (.I0(pref_limit[3]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [3]),
        .I2(pref_limit[2]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [2]),
        .O(i__carry_i_15__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_16
       (.I0(wb_limit[1]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [1]),
        .I2(wb_limit[0]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [0]),
        .O(i__carry_i_16_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_16__0
       (.I0(pref_limit[1]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [1]),
        .I2(pref_limit[0]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [0]),
        .O(i__carry_i_16__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_1__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [14]),
        .I1(pref_limit[14]),
        .I2(pref_limit[15]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [15]),
        .O(i__carry_i_1__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_2
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [12]),
        .I1(wb_limit[12]),
        .I2(wb_limit[13]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [13]),
        .O(i__carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_2__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [12]),
        .I1(pref_limit[12]),
        .I2(pref_limit[13]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [13]),
        .O(i__carry_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_3
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [10]),
        .I1(wb_limit[10]),
        .I2(wb_limit[11]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [11]),
        .O(i__carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_3__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [10]),
        .I1(pref_limit[10]),
        .I2(pref_limit[11]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [11]),
        .O(i__carry_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_4
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [8]),
        .I1(wb_limit[8]),
        .I2(wb_limit[9]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [9]),
        .O(i__carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_4__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [8]),
        .I1(pref_limit[8]),
        .I2(pref_limit[9]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [9]),
        .O(i__carry_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_5
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [6]),
        .I1(wb_limit[6]),
        .I2(wb_limit[7]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [7]),
        .O(i__carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_5__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [6]),
        .I1(pref_limit[6]),
        .I2(pref_limit[7]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [7]),
        .O(i__carry_i_5__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_6
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [4]),
        .I1(wb_limit[4]),
        .I2(wb_limit[5]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [5]),
        .O(i__carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_6__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [4]),
        .I1(pref_limit[4]),
        .I2(pref_limit[5]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [5]),
        .O(i__carry_i_6__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_7
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [2]),
        .I1(wb_limit[2]),
        .I2(wb_limit[3]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [3]),
        .O(i__carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_7__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [2]),
        .I1(pref_limit[2]),
        .I2(pref_limit[3]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [3]),
        .O(i__carry_i_7__0_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_8
       (.I0(\ADDER_EN[2].ctr_reg[2]_1 [0]),
        .I1(wb_limit[0]),
        .I2(wb_limit[1]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [1]),
        .O(i__carry_i_8_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_8__0
       (.I0(\ADDER_EN[0].ctr_reg[0]_2 [0]),
        .I1(pref_limit[0]),
        .I2(pref_limit[1]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [1]),
        .O(i__carry_i_8__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_9
       (.I0(wb_limit[15]),
        .I1(\ADDER_EN[2].ctr_reg[2]_1 [15]),
        .I2(wb_limit[14]),
        .I3(\ADDER_EN[2].ctr_reg[2]_1 [14]),
        .O(i__carry_i_9_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_9__0
       (.I0(pref_limit[15]),
        .I1(\ADDER_EN[0].ctr_reg[0]_2 [15]),
        .I2(pref_limit[14]),
        .I3(\ADDER_EN[0].ctr_reg[0]_2 [14]),
        .O(i__carry_i_9__0_n_0));
  LUT6 #(
    .INIT(64'hFEFEFEFEFEFEFE00)) 
    limit_irq_INST_0
       (.I0(gtOp),
        .I1(\gtOp_inferred__0/i__carry__0_n_0 ),
        .I2(gtOp_carry__0_n_0),
        .I3(limit_irq_INST_0_i_1_n_0),
        .I4(limit_irq_INST_0_i_2_n_0),
        .I5(limit_irq_INST_0_i_3_n_0),
        .O(limit_irq));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    limit_irq_INST_0_i_1
       (.I0(phase[2]),
        .I1(phase[3]),
        .I2(phase[0]),
        .I3(phase[1]),
        .I4(limit_irq_INST_0_i_4_n_0),
        .O(limit_irq_INST_0_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    limit_irq_INST_0_i_2
       (.I0(phase[8]),
        .I1(phase[9]),
        .I2(phase[10]),
        .I3(phase[11]),
        .I4(limit_irq_INST_0_i_5_n_0),
        .O(limit_irq_INST_0_i_2_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    limit_irq_INST_0_i_3
       (.I0(limit_irq_INST_0_i_6_n_0),
        .I1(limit_irq_INST_0_i_7_n_0),
        .I2(limit_irq_INST_0_i_8_n_0),
        .I3(limit_irq_INST_0_i_9_n_0),
        .O(limit_irq_INST_0_i_3_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    limit_irq_INST_0_i_4
       (.I0(phase[5]),
        .I1(phase[4]),
        .I2(phase[7]),
        .I3(phase[6]),
        .O(limit_irq_INST_0_i_4_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    limit_irq_INST_0_i_5
       (.I0(phase[13]),
        .I1(phase[12]),
        .I2(phase[15]),
        .I3(phase[14]),
        .O(limit_irq_INST_0_i_5_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    limit_irq_INST_0_i_6
       (.I0(phase[18]),
        .I1(phase[17]),
        .I2(phase[26]),
        .I3(phase[19]),
        .O(limit_irq_INST_0_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    limit_irq_INST_0_i_7
       (.I0(phase[22]),
        .I1(phase[21]),
        .I2(phase[16]),
        .I3(phase[23]),
        .O(limit_irq_INST_0_i_7_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    limit_irq_INST_0_i_8
       (.I0(phase[30]),
        .I1(phase[31]),
        .I2(phase[29]),
        .I3(phase[20]),
        .O(limit_irq_INST_0_i_8_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    limit_irq_INST_0_i_9
       (.I0(phase[24]),
        .I1(phase[27]),
        .I2(phase[28]),
        .I3(phase[25]),
        .O(limit_irq_INST_0_i_9_n_0));
  LUT5 #(
    .INIT(32'h00000020)) 
    \phase[15]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(s00_axi_wstrb[1]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[3]),
        .O(p_1_in[15]));
  LUT5 #(
    .INIT(32'h00000020)) 
    \phase[23]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(s00_axi_wstrb[2]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[3]),
        .O(p_1_in[23]));
  LUT5 #(
    .INIT(32'h00000020)) 
    \phase[31]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(s00_axi_wstrb[3]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[3]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \phase[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_WREADY),
        .I3(S_AXI_AWREADY),
        .O(slv_reg_wren));
  LUT5 #(
    .INIT(32'h00000020)) 
    \phase[7]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(s00_axi_wstrb[0]),
        .I3(axi_awaddr[2]),
        .I4(axi_awaddr[3]),
        .O(p_1_in[7]));
  FDRE \phase_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(phase[0]),
        .R(p_0_in));
  FDRE \phase_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(phase[10]),
        .R(p_0_in));
  FDRE \phase_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(phase[11]),
        .R(p_0_in));
  FDRE \phase_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(phase[12]),
        .R(p_0_in));
  FDRE \phase_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(phase[13]),
        .R(p_0_in));
  FDRE \phase_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(phase[14]),
        .R(p_0_in));
  FDRE \phase_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(phase[15]),
        .R(p_0_in));
  FDRE \phase_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(phase[16]),
        .R(p_0_in));
  FDRE \phase_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(phase[17]),
        .R(p_0_in));
  FDRE \phase_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(phase[18]),
        .R(p_0_in));
  FDRE \phase_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(phase[19]),
        .R(p_0_in));
  FDRE \phase_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(phase[1]),
        .R(p_0_in));
  FDRE \phase_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(phase[20]),
        .R(p_0_in));
  FDRE \phase_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(phase[21]),
        .R(p_0_in));
  FDRE \phase_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(phase[22]),
        .R(p_0_in));
  FDRE \phase_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(phase[23]),
        .R(p_0_in));
  FDRE \phase_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(phase[24]),
        .R(p_0_in));
  FDRE \phase_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(phase[25]),
        .R(p_0_in));
  FDRE \phase_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(phase[26]),
        .R(p_0_in));
  FDRE \phase_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(phase[27]),
        .R(p_0_in));
  FDRE \phase_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(phase[28]),
        .R(p_0_in));
  FDRE \phase_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(phase[29]),
        .R(p_0_in));
  FDRE \phase_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(phase[2]),
        .R(p_0_in));
  FDRE \phase_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(phase[30]),
        .R(p_0_in));
  FDRE \phase_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(phase[31]),
        .R(p_0_in));
  FDRE \phase_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(phase[3]),
        .R(p_0_in));
  FDRE \phase_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(phase[4]),
        .R(p_0_in));
  FDRE \phase_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(phase[5]),
        .R(p_0_in));
  FDRE \phase_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(phase[6]),
        .R(p_0_in));
  FDRE \phase_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(phase[7]),
        .R(p_0_in));
  FDRE \phase_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(phase[8]),
        .R(p_0_in));
  FDRE \phase_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(phase[9]),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'h00002000)) 
    \pref_limit[15]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[1]),
        .I4(axi_awaddr[3]),
        .O(\pref_limit[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00002000)) 
    \pref_limit[23]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[2]),
        .I4(axi_awaddr[3]),
        .O(\pref_limit[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00002000)) 
    \pref_limit[31]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[3]),
        .I4(axi_awaddr[3]),
        .O(\pref_limit[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00002000)) 
    \pref_limit[7]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[0]),
        .I4(axi_awaddr[3]),
        .O(\pref_limit[7]_i_1_n_0 ));
  FDRE \pref_limit_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(pref_limit[0]),
        .R(p_0_in));
  FDRE \pref_limit_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(pref_limit[10]),
        .R(p_0_in));
  FDRE \pref_limit_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(pref_limit[11]),
        .R(p_0_in));
  FDRE \pref_limit_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(pref_limit[12]),
        .R(p_0_in));
  FDRE \pref_limit_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(pref_limit[13]),
        .R(p_0_in));
  FDRE \pref_limit_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(pref_limit[14]),
        .R(p_0_in));
  FDRE \pref_limit_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(pref_limit[15]),
        .R(p_0_in));
  FDRE \pref_limit_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(pref_limit[16]),
        .R(p_0_in));
  FDRE \pref_limit_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(pref_limit[17]),
        .R(p_0_in));
  FDRE \pref_limit_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(pref_limit[18]),
        .R(p_0_in));
  FDRE \pref_limit_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(pref_limit[19]),
        .R(p_0_in));
  FDRE \pref_limit_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(pref_limit[1]),
        .R(p_0_in));
  FDRE \pref_limit_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(pref_limit[20]),
        .R(p_0_in));
  FDRE \pref_limit_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(pref_limit[21]),
        .R(p_0_in));
  FDRE \pref_limit_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(pref_limit[22]),
        .R(p_0_in));
  FDRE \pref_limit_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(pref_limit[23]),
        .R(p_0_in));
  FDRE \pref_limit_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(pref_limit[24]),
        .R(p_0_in));
  FDRE \pref_limit_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(pref_limit[25]),
        .R(p_0_in));
  FDRE \pref_limit_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(pref_limit[26]),
        .R(p_0_in));
  FDRE \pref_limit_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(pref_limit[27]),
        .R(p_0_in));
  FDRE \pref_limit_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(pref_limit[28]),
        .R(p_0_in));
  FDRE \pref_limit_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(pref_limit[29]),
        .R(p_0_in));
  FDRE \pref_limit_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(pref_limit[2]),
        .R(p_0_in));
  FDRE \pref_limit_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(pref_limit[30]),
        .R(p_0_in));
  FDRE \pref_limit_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(pref_limit[31]),
        .R(p_0_in));
  FDRE \pref_limit_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(pref_limit[3]),
        .R(p_0_in));
  FDRE \pref_limit_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(pref_limit[4]),
        .R(p_0_in));
  FDRE \pref_limit_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(pref_limit[5]),
        .R(p_0_in));
  FDRE \pref_limit_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(pref_limit[6]),
        .R(p_0_in));
  FDRE \pref_limit_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(pref_limit[7]),
        .R(p_0_in));
  FDRE \pref_limit_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(pref_limit[8]),
        .R(p_0_in));
  FDRE \pref_limit_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\pref_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(pref_limit[9]),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'h20000000)) 
    \wb_limit[15]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\wb_limit[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \wb_limit[23]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\wb_limit[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \wb_limit[31]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\wb_limit[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \wb_limit[7]_i_1 
       (.I0(slv_reg_wren),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\wb_limit[7]_i_1_n_0 ));
  FDRE \wb_limit_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(wb_limit[0]),
        .R(p_0_in));
  FDRE \wb_limit_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(wb_limit[10]),
        .R(p_0_in));
  FDRE \wb_limit_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(wb_limit[11]),
        .R(p_0_in));
  FDRE \wb_limit_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(wb_limit[12]),
        .R(p_0_in));
  FDRE \wb_limit_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(wb_limit[13]),
        .R(p_0_in));
  FDRE \wb_limit_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(wb_limit[14]),
        .R(p_0_in));
  FDRE \wb_limit_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(wb_limit[15]),
        .R(p_0_in));
  FDRE \wb_limit_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(wb_limit[16]),
        .R(p_0_in));
  FDRE \wb_limit_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(wb_limit[17]),
        .R(p_0_in));
  FDRE \wb_limit_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(wb_limit[18]),
        .R(p_0_in));
  FDRE \wb_limit_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(wb_limit[19]),
        .R(p_0_in));
  FDRE \wb_limit_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(wb_limit[1]),
        .R(p_0_in));
  FDRE \wb_limit_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(wb_limit[20]),
        .R(p_0_in));
  FDRE \wb_limit_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(wb_limit[21]),
        .R(p_0_in));
  FDRE \wb_limit_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(wb_limit[22]),
        .R(p_0_in));
  FDRE \wb_limit_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(wb_limit[23]),
        .R(p_0_in));
  FDRE \wb_limit_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(wb_limit[24]),
        .R(p_0_in));
  FDRE \wb_limit_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(wb_limit[25]),
        .R(p_0_in));
  FDRE \wb_limit_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(wb_limit[26]),
        .R(p_0_in));
  FDRE \wb_limit_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(wb_limit[27]),
        .R(p_0_in));
  FDRE \wb_limit_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(wb_limit[28]),
        .R(p_0_in));
  FDRE \wb_limit_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(wb_limit[29]),
        .R(p_0_in));
  FDRE \wb_limit_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(wb_limit[2]),
        .R(p_0_in));
  FDRE \wb_limit_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(wb_limit[30]),
        .R(p_0_in));
  FDRE \wb_limit_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(wb_limit[31]),
        .R(p_0_in));
  FDRE \wb_limit_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(wb_limit[3]),
        .R(p_0_in));
  FDRE \wb_limit_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(wb_limit[4]),
        .R(p_0_in));
  FDRE \wb_limit_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(wb_limit[5]),
        .R(p_0_in));
  FDRE \wb_limit_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(wb_limit[6]),
        .R(p_0_in));
  FDRE \wb_limit_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(wb_limit[7]),
        .R(p_0_in));
  FDRE \wb_limit_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(wb_limit[8]),
        .R(p_0_in));
  FDRE \wb_limit_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\wb_limit[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(wb_limit[9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h8000FFFF80000000)) 
    write_finish_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_WREADY),
        .I3(S_AXI_AWREADY),
        .I4(s00_axi_aresetn),
        .I5(write_finish),
        .O(write_finish_i_1_n_0));
  FDRE write_finish_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(write_finish_i_1_n_0),
        .Q(write_finish),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "ps_pl_top_configuration_prem_mem_counter_axi_0_0,prem_mem_counter_axi_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "prem_mem_counter_axi_v1_0,Vivado 2018.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (pmu_trigin,
    pmu_trigout,
    limit_irq,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  input pmu_trigin;
  output pmu_trigout;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 limit_irq INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME limit_irq, SENSITIVITY LEVEL_HIGH, PortWidth 1" *) output limit_irq;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 7, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 99990000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN ps_pl_top_configuration_zynq_ultra_ps_e_0_0_pl_clk0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [4:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [4:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 99990000, PHASE 0.000, CLK_DOMAIN ps_pl_top_configuration_zynq_ultra_ps_e_0_0_pl_clk0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire limit_irq;
  wire pmu_trigin;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign pmu_trigout = pmu_trigin;
  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_prem_mem_counter_axi_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .limit_irq(limit_irq),
        .pmu_trigin(pmu_trigin),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
