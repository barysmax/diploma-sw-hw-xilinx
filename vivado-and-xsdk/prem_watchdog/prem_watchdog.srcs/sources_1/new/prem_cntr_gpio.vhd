----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 05/13/2018 02:57:40 AM
-- Design Name:
-- Module Name: prem_cntr_gpio - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity prem_cntr_gpio is
    Port (
           pmu_trigin : in STD_LOGIC;

           clk : in STD_LOGIC;

           phase : in STD_LOGIC_VECTOR (1 downto 0);

           pref_limit : in STD_LOGIC_VECTOR (31 downto 0);
           pref_ctr : out STD_LOGIC_VECTOR (31 downto 0);

           comp_limit : in STD_LOGIC_VECTOR (31 downto 0);
           comp_ctr : out STD_LOGIC_VECTOR (31 downto 0);

           wb_limit : in STD_LOGIC_VECTOR (31 downto 0);
           wb_ctr : out STD_LOGIC_VECTOR (31 downto 0);

           limit_irq : out STD_LOGIC );

end prem_cntr_gpio;

architecture Behavioral of prem_cntr_gpio is

constant N_CNTRS: integer   := 3;

constant PREF_CNTR: integer := 0;
constant COMP_CNTR: integer := 1;
constant WB_CNTR: integer   := 2;

type counter is array (0 to N_CNTRS - 1) of std_logic_vector(31 downto 0);
signal ctr : counter := (others => (others => '0'));

signal ctr_rst : std_logic := '0';

--signal ctr_en : std_logic_vector(2 downto 0);
--------------------------------------------------


begin

    ctr_rst <= '1' when phase = "00" else '0';

    -- enable counter for one clock cycle on HIGH level of trigger;
    ADDER_EN: for ph in 0 to N_CNTRS -1 generate
        process(clk) is
        variable trigack_pending : std_logic := '0';
        begin
            if (ctr_rst = '1' ) then
                ctr(ph) <= (others => '0');
            else
                if rising_edge(clk) then
                    if (unsigned(phase) = (ph + 1)) and (pmu_trigin = '1') and (trigack_pending = '0') then
                        ctr(ph) <= ctr(ph) + 1;
                        trigack_pending := '1';
                    else 
                        trigack_pending := pmu_trigin;
                    end if;
                end if;
            end if;
        end process;
    end generate ADDER_EN;
    
    limit_irq <= '1' when (nor_reduce(phase) = '0') and
                    ( ctr(PREF_CNTR) > pref_limit or
                      ctr(COMP_CNTR) > comp_limit or
                      ctr(WB_CNTR)   > wb_limit) 
     else '0';
    
    pref_ctr <= ctr(PREF_CNTR);
    comp_ctr <= ctr(COMP_CNTR);
    wb_ctr   <= ctr(WB_CNTR);

end Behavioral;
