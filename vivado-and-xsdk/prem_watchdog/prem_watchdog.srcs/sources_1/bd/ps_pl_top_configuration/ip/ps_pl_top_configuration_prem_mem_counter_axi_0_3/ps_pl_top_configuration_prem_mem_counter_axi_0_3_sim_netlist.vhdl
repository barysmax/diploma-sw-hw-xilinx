-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
-- Date        : Sat May 19 17:53:51 2018
-- Host        : matejjoe running 64-bit Debian GNU/Linux 9.2 (stretch)
-- Command     : write_vhdl -force -mode funcsim -rename_top ps_pl_top_configuration_prem_mem_counter_axi_0_3 -prefix
--               ps_pl_top_configuration_prem_mem_counter_axi_0_3_
--               ps_pl_top_configuration_prem_mem_counter_axi_0_0_sim_netlist.vhdl
-- Design      : ps_pl_top_configuration_prem_mem_counter_axi_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xczu9eg-ffvb1156-2-i-es2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ps_pl_top_configuration_prem_mem_counter_axi_0_3_prem_mem_counter_axi_v1_0_S00_AXI is
  port (
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    limit_irq : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    pmu_trigin : in STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end ps_pl_top_configuration_prem_mem_counter_axi_0_3_prem_mem_counter_axi_v1_0_S00_AXI;

architecture STRUCTURE of ps_pl_top_configuration_prem_mem_counter_axi_0_3_prem_mem_counter_axi_v1_0_S00_AXI is
  signal \ADDER_EN[0].ctr[0][0]_i_3_n_0\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr[0][0]_i_4_n_0\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_0\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_1\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_10\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_11\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_12\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_13\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_14\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_15\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_2\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_3\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_5\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_6\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_7\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_8\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][0]_i_2_n_9\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_0\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_1\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_10\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_11\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_12\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_13\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_14\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_15\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_2\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_3\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_5\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_6\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_7\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_8\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][16]_i_1_n_9\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_1\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_10\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_11\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_12\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_13\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_14\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_15\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_2\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_3\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_5\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_6\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_7\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_8\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][24]_i_1_n_9\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_0\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_1\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_10\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_11\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_12\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_13\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_14\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_15\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_2\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_3\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_5\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_6\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_7\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_8\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0][8]_i_1_n_9\ : STD_LOGIC;
  signal \ADDER_EN[0].ctr_reg[0]_2\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \ADDER_EN[0].trigack_pending_i_1_n_0\ : STD_LOGIC;
  signal \ADDER_EN[0].trigack_pending_i_2_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr[1][0]_i_3_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr[1][0]_i_4_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr[1][0]_i_5_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr[1][0]_i_6_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_1\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_10\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_11\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_12\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_13\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_14\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_15\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_2\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_3\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_5\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_6\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_7\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_8\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][0]_i_2_n_9\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_1\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_10\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_11\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_12\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_13\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_14\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_15\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_2\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_3\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_5\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_6\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_7\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_8\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][16]_i_1_n_9\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_1\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_10\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_11\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_12\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_13\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_14\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_15\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_2\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_3\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_5\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_6\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_7\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_8\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][24]_i_1_n_9\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_1\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_10\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_11\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_12\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_13\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_14\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_15\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_2\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_3\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_5\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_6\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_7\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_8\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1][8]_i_1_n_9\ : STD_LOGIC;
  signal \ADDER_EN[1].ctr_reg[1]_0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \ADDER_EN[1].trigack_pending_i_1_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].trigack_pending_i_2_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].trigack_pending_i_3_n_0\ : STD_LOGIC;
  signal \ADDER_EN[1].trigack_pending_reg_n_0\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr[2][0]_i_3_n_0\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr[2][0]_i_4_n_0\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_0\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_1\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_10\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_11\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_12\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_13\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_14\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_15\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_2\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_3\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_5\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_6\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_7\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_8\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][0]_i_2_n_9\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_0\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_1\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_10\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_11\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_12\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_13\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_14\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_15\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_2\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_3\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_5\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_6\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_7\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_8\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][16]_i_1_n_9\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_1\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_10\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_11\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_12\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_13\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_14\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_15\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_2\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_3\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_5\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_6\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_7\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_8\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][24]_i_1_n_9\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_0\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_1\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_10\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_11\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_12\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_13\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_14\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_15\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_2\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_3\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_5\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_6\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_7\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_8\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2][8]_i_1_n_9\ : STD_LOGIC;
  signal \ADDER_EN[2].ctr_reg[2]_1\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \ADDER_EN[2].trigack_pending_i_1_n_0\ : STD_LOGIC;
  signal \ADDER_EN[2].trigack_pending_i_2_n_0\ : STD_LOGIC;
  signal \ADDER_EN[2].trigack_pending_reg_n_0\ : STD_LOGIC;
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 4 downto 2 );
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal comp_limit : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \comp_limit[15]_i_1_n_0\ : STD_LOGIC;
  signal \comp_limit[23]_i_1_n_0\ : STD_LOGIC;
  signal \comp_limit[31]_i_1_n_0\ : STD_LOGIC;
  signal \comp_limit[7]_i_1_n_0\ : STD_LOGIC;
  signal gtOp : STD_LOGIC;
  signal \gtOp_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_11_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_12_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_13_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_14_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_15_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_16_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_n_0\ : STD_LOGIC;
  signal \gtOp_carry__0_n_1\ : STD_LOGIC;
  signal \gtOp_carry__0_n_2\ : STD_LOGIC;
  signal \gtOp_carry__0_n_3\ : STD_LOGIC;
  signal \gtOp_carry__0_n_5\ : STD_LOGIC;
  signal \gtOp_carry__0_n_6\ : STD_LOGIC;
  signal \gtOp_carry__0_n_7\ : STD_LOGIC;
  signal gtOp_carry_i_10_n_0 : STD_LOGIC;
  signal gtOp_carry_i_11_n_0 : STD_LOGIC;
  signal gtOp_carry_i_12_n_0 : STD_LOGIC;
  signal gtOp_carry_i_13_n_0 : STD_LOGIC;
  signal gtOp_carry_i_14_n_0 : STD_LOGIC;
  signal gtOp_carry_i_15_n_0 : STD_LOGIC;
  signal gtOp_carry_i_16_n_0 : STD_LOGIC;
  signal gtOp_carry_i_1_n_0 : STD_LOGIC;
  signal gtOp_carry_i_2_n_0 : STD_LOGIC;
  signal gtOp_carry_i_3_n_0 : STD_LOGIC;
  signal gtOp_carry_i_4_n_0 : STD_LOGIC;
  signal gtOp_carry_i_5_n_0 : STD_LOGIC;
  signal gtOp_carry_i_6_n_0 : STD_LOGIC;
  signal gtOp_carry_i_7_n_0 : STD_LOGIC;
  signal gtOp_carry_i_8_n_0 : STD_LOGIC;
  signal gtOp_carry_i_9_n_0 : STD_LOGIC;
  signal gtOp_carry_n_0 : STD_LOGIC;
  signal gtOp_carry_n_1 : STD_LOGIC;
  signal gtOp_carry_n_2 : STD_LOGIC;
  signal gtOp_carry_n_3 : STD_LOGIC;
  signal gtOp_carry_n_5 : STD_LOGIC;
  signal gtOp_carry_n_6 : STD_LOGIC;
  signal gtOp_carry_n_7 : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry__0_n_0\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry__0_n_5\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry__0_n_6\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry__0_n_7\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry_n_5\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry_n_6\ : STD_LOGIC;
  signal \gtOp_inferred__0/i__carry_n_7\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry__0_n_1\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry__0_n_2\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry__0_n_3\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry__0_n_5\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry__0_n_6\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry__0_n_7\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry_n_0\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry_n_1\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry_n_2\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry_n_3\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry_n_5\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry_n_6\ : STD_LOGIC;
  signal \gtOp_inferred__1/i__carry_n_7\ : STD_LOGIC;
  signal \i__carry__0_i_10__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_10_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_11__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_11_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_12__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_12_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_13__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_13_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_14__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_14_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_15__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_15_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_16__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_16_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_6__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_7__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_8__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_9__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_9_n_0\ : STD_LOGIC;
  signal \i__carry_i_10__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_10_n_0\ : STD_LOGIC;
  signal \i__carry_i_11__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_11_n_0\ : STD_LOGIC;
  signal \i__carry_i_12__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_12_n_0\ : STD_LOGIC;
  signal \i__carry_i_13__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_13_n_0\ : STD_LOGIC;
  signal \i__carry_i_14__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_14_n_0\ : STD_LOGIC;
  signal \i__carry_i_15__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_15_n_0\ : STD_LOGIC;
  signal \i__carry_i_16__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_16_n_0\ : STD_LOGIC;
  signal \i__carry_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_2_n_0\ : STD_LOGIC;
  signal \i__carry_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_4_n_0\ : STD_LOGIC;
  signal \i__carry_i_5__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_6__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_6_n_0\ : STD_LOGIC;
  signal \i__carry_i_7__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_8__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_8_n_0\ : STD_LOGIC;
  signal \i__carry_i_9__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_9_n_0\ : STD_LOGIC;
  signal limit_irq_INST_0_i_1_n_0 : STD_LOGIC;
  signal limit_irq_INST_0_i_2_n_0 : STD_LOGIC;
  signal limit_irq_INST_0_i_3_n_0 : STD_LOGIC;
  signal limit_irq_INST_0_i_4_n_0 : STD_LOGIC;
  signal limit_irq_INST_0_i_5_n_0 : STD_LOGIC;
  signal limit_irq_INST_0_i_6_n_0 : STD_LOGIC;
  signal limit_irq_INST_0_i_7_n_0 : STD_LOGIC;
  signal limit_irq_INST_0_i_8_n_0 : STD_LOGIC;
  signal limit_irq_INST_0_i_9_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_10_out : STD_LOGIC;
  signal p_13_out : STD_LOGIC;
  signal p_16_out : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal phase : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal pref_limit : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \pref_limit[15]_i_1_n_0\ : STD_LOGIC;
  signal \pref_limit[23]_i_1_n_0\ : STD_LOGIC;
  signal \pref_limit[31]_i_1_n_0\ : STD_LOGIC;
  signal \pref_limit[7]_i_1_n_0\ : STD_LOGIC;
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal slv_reg_rden : STD_LOGIC;
  signal slv_reg_wren : STD_LOGIC;
  signal trigack_pending : STD_LOGIC;
  signal wb_limit : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \wb_limit[15]_i_1_n_0\ : STD_LOGIC;
  signal \wb_limit[23]_i_1_n_0\ : STD_LOGIC;
  signal \wb_limit[31]_i_1_n_0\ : STD_LOGIC;
  signal \wb_limit[7]_i_1_n_0\ : STD_LOGIC;
  signal write_finish : STD_LOGIC;
  signal write_finish_i_1_n_0 : STD_LOGIC;
  signal \NLW_ADDER_EN[0].ctr_reg[0][0]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ADDER_EN[0].ctr_reg[0][16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ADDER_EN[0].ctr_reg[0][24]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  signal \NLW_ADDER_EN[0].ctr_reg[0][8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ADDER_EN[1].ctr_reg[1][0]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ADDER_EN[1].ctr_reg[1][16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ADDER_EN[1].ctr_reg[1][24]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  signal \NLW_ADDER_EN[1].ctr_reg[1][8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ADDER_EN[2].ctr_reg[2][0]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ADDER_EN[2].ctr_reg[2][16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ADDER_EN[2].ctr_reg[2][24]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  signal \NLW_ADDER_EN[2].ctr_reg[2][8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_gtOp_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_gtOp_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_gtOp_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_gtOp_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_gtOp_inferred__0/i__carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_gtOp_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_gtOp_inferred__0/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_gtOp_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_gtOp_inferred__1/i__carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_gtOp_inferred__1/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_gtOp_inferred__1/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_gtOp_inferred__1/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ADDER_EN[1].trigack_pending_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of limit_irq_INST_0_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \phase[31]_i_2\ : label is "soft_lutpair1";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
\ADDER_EN[0].ctr[0][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => limit_irq_INST_0_i_2_n_0,
      I1 => \ADDER_EN[0].ctr[0][0]_i_3_n_0\,
      I2 => limit_irq_INST_0_i_4_n_0,
      I3 => limit_irq_INST_0_i_3_n_0,
      O => p_16_out
    );
\ADDER_EN[0].ctr[0][0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => phase(0),
      I1 => phase(1),
      I2 => pmu_trigin,
      I3 => trigack_pending,
      I4 => phase(3),
      I5 => phase(2),
      O => \ADDER_EN[0].ctr[0][0]_i_3_n_0\
    );
\ADDER_EN[0].ctr[0][0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(0),
      O => \ADDER_EN[0].ctr[0][0]_i_4_n_0\
    );
\ADDER_EN[0].ctr_reg[0][0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_15\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(0)
    );
\ADDER_EN[0].ctr_reg[0][0]_i_2\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_0\,
      CO(6) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_1\,
      CO(5) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_2\,
      CO(4) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_3\,
      CO(3) => \NLW_ADDER_EN[0].ctr_reg[0][0]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_5\,
      CO(1) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_6\,
      CO(0) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_7\,
      DI(7 downto 0) => B"00000001",
      O(7) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_8\,
      O(6) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_9\,
      O(5) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_10\,
      O(4) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_11\,
      O(3) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_12\,
      O(2) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_13\,
      O(1) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_14\,
      O(0) => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_15\,
      S(7 downto 1) => \ADDER_EN[0].ctr_reg[0]_2\(7 downto 1),
      S(0) => \ADDER_EN[0].ctr[0][0]_i_4_n_0\
    );
\ADDER_EN[0].ctr_reg[0][10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_13\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(10)
    );
\ADDER_EN[0].ctr_reg[0][11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_12\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(11)
    );
\ADDER_EN[0].ctr_reg[0][12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_11\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(12)
    );
\ADDER_EN[0].ctr_reg[0][13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_10\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(13)
    );
\ADDER_EN[0].ctr_reg[0][14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_9\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(14)
    );
\ADDER_EN[0].ctr_reg[0][15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_8\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(15)
    );
\ADDER_EN[0].ctr_reg[0][16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_15\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(16)
    );
\ADDER_EN[0].ctr_reg[0][16]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_0\,
      CO(6) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_1\,
      CO(5) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_2\,
      CO(4) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_3\,
      CO(3) => \NLW_ADDER_EN[0].ctr_reg[0][16]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_5\,
      CO(1) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_6\,
      CO(0) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_8\,
      O(6) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_9\,
      O(5) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_10\,
      O(4) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_11\,
      O(3) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_12\,
      O(2) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_13\,
      O(1) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_14\,
      O(0) => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_15\,
      S(7 downto 0) => \ADDER_EN[0].ctr_reg[0]_2\(23 downto 16)
    );
\ADDER_EN[0].ctr_reg[0][17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_14\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(17)
    );
\ADDER_EN[0].ctr_reg[0][18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_13\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(18)
    );
\ADDER_EN[0].ctr_reg[0][19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_12\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(19)
    );
\ADDER_EN[0].ctr_reg[0][1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_14\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(1)
    );
\ADDER_EN[0].ctr_reg[0][20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_11\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(20)
    );
\ADDER_EN[0].ctr_reg[0][21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_10\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(21)
    );
\ADDER_EN[0].ctr_reg[0][22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_9\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(22)
    );
\ADDER_EN[0].ctr_reg[0][23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_8\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(23)
    );
\ADDER_EN[0].ctr_reg[0][24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_15\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(24)
    );
\ADDER_EN[0].ctr_reg[0][24]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ADDER_EN[0].ctr_reg[0][16]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \NLW_ADDER_EN[0].ctr_reg[0][24]_i_1_CO_UNCONNECTED\(7),
      CO(6) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_1\,
      CO(5) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_2\,
      CO(4) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_3\,
      CO(3) => \NLW_ADDER_EN[0].ctr_reg[0][24]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_5\,
      CO(1) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_6\,
      CO(0) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_8\,
      O(6) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_9\,
      O(5) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_10\,
      O(4) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_11\,
      O(3) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_12\,
      O(2) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_13\,
      O(1) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_14\,
      O(0) => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_15\,
      S(7 downto 0) => \ADDER_EN[0].ctr_reg[0]_2\(31 downto 24)
    );
\ADDER_EN[0].ctr_reg[0][25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_14\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(25)
    );
\ADDER_EN[0].ctr_reg[0][26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_13\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(26)
    );
\ADDER_EN[0].ctr_reg[0][27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_12\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(27)
    );
\ADDER_EN[0].ctr_reg[0][28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_11\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(28)
    );
\ADDER_EN[0].ctr_reg[0][29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_10\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(29)
    );
\ADDER_EN[0].ctr_reg[0][2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_13\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(2)
    );
\ADDER_EN[0].ctr_reg[0][30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_9\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(30)
    );
\ADDER_EN[0].ctr_reg[0][31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][24]_i_1_n_8\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(31)
    );
\ADDER_EN[0].ctr_reg[0][3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_12\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(3)
    );
\ADDER_EN[0].ctr_reg[0][4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_11\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(4)
    );
\ADDER_EN[0].ctr_reg[0][5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_10\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(5)
    );
\ADDER_EN[0].ctr_reg[0][6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_9\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(6)
    );
\ADDER_EN[0].ctr_reg[0][7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_8\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(7)
    );
\ADDER_EN[0].ctr_reg[0][8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_15\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(8)
    );
\ADDER_EN[0].ctr_reg[0][8]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ADDER_EN[0].ctr_reg[0][0]_i_2_n_0\,
      CI_TOP => '0',
      CO(7) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_0\,
      CO(6) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_1\,
      CO(5) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_2\,
      CO(4) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_3\,
      CO(3) => \NLW_ADDER_EN[0].ctr_reg[0][8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_5\,
      CO(1) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_6\,
      CO(0) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_8\,
      O(6) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_9\,
      O(5) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_10\,
      O(4) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_11\,
      O(3) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_12\,
      O(2) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_13\,
      O(1) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_14\,
      O(0) => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_15\,
      S(7 downto 0) => \ADDER_EN[0].ctr_reg[0]_2\(15 downto 8)
    );
\ADDER_EN[0].ctr_reg[0][9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_16_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[0].ctr_reg[0][8]_i_1_n_14\,
      Q => \ADDER_EN[0].ctr_reg[0]_2\(9)
    );
\ADDER_EN[0].trigack_pending_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCFCECCCCC0CE"
    )
        port map (
      I0 => \ADDER_EN[0].trigack_pending_i_2_n_0\,
      I1 => pmu_trigin,
      I2 => limit_irq_INST_0_i_3_n_0,
      I3 => \ADDER_EN[1].trigack_pending_i_3_n_0\,
      I4 => limit_irq_INST_0_i_2_n_0,
      I5 => trigack_pending,
      O => \ADDER_EN[0].trigack_pending_i_1_n_0\
    );
\ADDER_EN[0].trigack_pending_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000100000"
    )
        port map (
      I0 => \ADDER_EN[1].ctr[1][0]_i_6_n_0\,
      I1 => trigack_pending,
      I2 => pmu_trigin,
      I3 => phase(1),
      I4 => phase(0),
      I5 => limit_irq_INST_0_i_4_n_0,
      O => \ADDER_EN[0].trigack_pending_i_2_n_0\
    );
\ADDER_EN[0].trigack_pending_reg\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ADDER_EN[0].trigack_pending_i_1_n_0\,
      Q => trigack_pending,
      R => '0'
    );
\ADDER_EN[1].ctr[1][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => limit_irq_INST_0_i_2_n_0,
      I1 => \ADDER_EN[1].ctr[1][0]_i_4_n_0\,
      I2 => limit_irq_INST_0_i_4_n_0,
      I3 => limit_irq_INST_0_i_3_n_0,
      O => p_13_out
    );
\ADDER_EN[1].ctr[1][0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => limit_irq_INST_0_i_2_n_0,
      I1 => phase(1),
      I2 => phase(0),
      I3 => \ADDER_EN[1].ctr[1][0]_i_6_n_0\,
      I4 => limit_irq_INST_0_i_4_n_0,
      I5 => limit_irq_INST_0_i_3_n_0,
      O => \ADDER_EN[1].ctr[1][0]_i_3_n_0\
    );
\ADDER_EN[1].ctr[1][0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => phase(1),
      I1 => phase(0),
      I2 => pmu_trigin,
      I3 => \ADDER_EN[1].trigack_pending_reg_n_0\,
      I4 => phase(3),
      I5 => phase(2),
      O => \ADDER_EN[1].ctr[1][0]_i_4_n_0\
    );
\ADDER_EN[1].ctr[1][0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(0),
      O => \ADDER_EN[1].ctr[1][0]_i_5_n_0\
    );
\ADDER_EN[1].ctr[1][0]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => phase(2),
      I1 => phase(3),
      O => \ADDER_EN[1].ctr[1][0]_i_6_n_0\
    );
\ADDER_EN[1].ctr_reg[1][0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_15\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(0)
    );
\ADDER_EN[1].ctr_reg[1][0]_i_2\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_0\,
      CO(6) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_1\,
      CO(5) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_2\,
      CO(4) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_3\,
      CO(3) => \NLW_ADDER_EN[1].ctr_reg[1][0]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_5\,
      CO(1) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_6\,
      CO(0) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_7\,
      DI(7 downto 0) => B"00000001",
      O(7) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_8\,
      O(6) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_9\,
      O(5) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_10\,
      O(4) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_11\,
      O(3) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_12\,
      O(2) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_13\,
      O(1) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_14\,
      O(0) => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_15\,
      S(7 downto 1) => \ADDER_EN[1].ctr_reg[1]_0\(7 downto 1),
      S(0) => \ADDER_EN[1].ctr[1][0]_i_5_n_0\
    );
\ADDER_EN[1].ctr_reg[1][10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_13\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(10)
    );
\ADDER_EN[1].ctr_reg[1][11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_12\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(11)
    );
\ADDER_EN[1].ctr_reg[1][12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_11\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(12)
    );
\ADDER_EN[1].ctr_reg[1][13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_10\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(13)
    );
\ADDER_EN[1].ctr_reg[1][14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_9\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(14)
    );
\ADDER_EN[1].ctr_reg[1][15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_8\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(15)
    );
\ADDER_EN[1].ctr_reg[1][16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_15\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(16)
    );
\ADDER_EN[1].ctr_reg[1][16]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_0\,
      CO(6) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_1\,
      CO(5) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_2\,
      CO(4) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_3\,
      CO(3) => \NLW_ADDER_EN[1].ctr_reg[1][16]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_5\,
      CO(1) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_6\,
      CO(0) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_8\,
      O(6) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_9\,
      O(5) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_10\,
      O(4) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_11\,
      O(3) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_12\,
      O(2) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_13\,
      O(1) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_14\,
      O(0) => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_15\,
      S(7 downto 0) => \ADDER_EN[1].ctr_reg[1]_0\(23 downto 16)
    );
\ADDER_EN[1].ctr_reg[1][17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_14\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(17)
    );
\ADDER_EN[1].ctr_reg[1][18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_13\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(18)
    );
\ADDER_EN[1].ctr_reg[1][19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_12\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(19)
    );
\ADDER_EN[1].ctr_reg[1][1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_14\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(1)
    );
\ADDER_EN[1].ctr_reg[1][20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_11\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(20)
    );
\ADDER_EN[1].ctr_reg[1][21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_10\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(21)
    );
\ADDER_EN[1].ctr_reg[1][22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_9\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(22)
    );
\ADDER_EN[1].ctr_reg[1][23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_8\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(23)
    );
\ADDER_EN[1].ctr_reg[1][24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_15\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(24)
    );
\ADDER_EN[1].ctr_reg[1][24]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ADDER_EN[1].ctr_reg[1][16]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \NLW_ADDER_EN[1].ctr_reg[1][24]_i_1_CO_UNCONNECTED\(7),
      CO(6) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_1\,
      CO(5) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_2\,
      CO(4) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_3\,
      CO(3) => \NLW_ADDER_EN[1].ctr_reg[1][24]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_5\,
      CO(1) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_6\,
      CO(0) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_8\,
      O(6) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_9\,
      O(5) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_10\,
      O(4) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_11\,
      O(3) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_12\,
      O(2) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_13\,
      O(1) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_14\,
      O(0) => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_15\,
      S(7 downto 0) => \ADDER_EN[1].ctr_reg[1]_0\(31 downto 24)
    );
\ADDER_EN[1].ctr_reg[1][25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_14\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(25)
    );
\ADDER_EN[1].ctr_reg[1][26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_13\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(26)
    );
\ADDER_EN[1].ctr_reg[1][27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_12\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(27)
    );
\ADDER_EN[1].ctr_reg[1][28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_11\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(28)
    );
\ADDER_EN[1].ctr_reg[1][29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_10\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(29)
    );
\ADDER_EN[1].ctr_reg[1][2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_13\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(2)
    );
\ADDER_EN[1].ctr_reg[1][30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_9\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(30)
    );
\ADDER_EN[1].ctr_reg[1][31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][24]_i_1_n_8\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(31)
    );
\ADDER_EN[1].ctr_reg[1][3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_12\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(3)
    );
\ADDER_EN[1].ctr_reg[1][4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_11\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(4)
    );
\ADDER_EN[1].ctr_reg[1][5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_10\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(5)
    );
\ADDER_EN[1].ctr_reg[1][6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_9\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(6)
    );
\ADDER_EN[1].ctr_reg[1][7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_8\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(7)
    );
\ADDER_EN[1].ctr_reg[1][8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_15\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(8)
    );
\ADDER_EN[1].ctr_reg[1][8]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ADDER_EN[1].ctr_reg[1][0]_i_2_n_0\,
      CI_TOP => '0',
      CO(7) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_0\,
      CO(6) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_1\,
      CO(5) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_2\,
      CO(4) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_3\,
      CO(3) => \NLW_ADDER_EN[1].ctr_reg[1][8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_5\,
      CO(1) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_6\,
      CO(0) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_8\,
      O(6) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_9\,
      O(5) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_10\,
      O(4) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_11\,
      O(3) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_12\,
      O(2) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_13\,
      O(1) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_14\,
      O(0) => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_15\,
      S(7 downto 0) => \ADDER_EN[1].ctr_reg[1]_0\(15 downto 8)
    );
\ADDER_EN[1].ctr_reg[1][9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_13_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[1].ctr_reg[1][8]_i_1_n_14\,
      Q => \ADDER_EN[1].ctr_reg[1]_0\(9)
    );
\ADDER_EN[1].trigack_pending_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCFCECCCCC0CE"
    )
        port map (
      I0 => \ADDER_EN[1].trigack_pending_i_2_n_0\,
      I1 => pmu_trigin,
      I2 => limit_irq_INST_0_i_3_n_0,
      I3 => \ADDER_EN[1].trigack_pending_i_3_n_0\,
      I4 => limit_irq_INST_0_i_2_n_0,
      I5 => \ADDER_EN[1].trigack_pending_reg_n_0\,
      O => \ADDER_EN[1].trigack_pending_i_1_n_0\
    );
\ADDER_EN[1].trigack_pending_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000100000"
    )
        port map (
      I0 => \ADDER_EN[1].ctr[1][0]_i_6_n_0\,
      I1 => \ADDER_EN[1].trigack_pending_reg_n_0\,
      I2 => pmu_trigin,
      I3 => phase(0),
      I4 => phase(1),
      I5 => limit_irq_INST_0_i_4_n_0,
      O => \ADDER_EN[1].trigack_pending_i_2_n_0\
    );
\ADDER_EN[1].trigack_pending_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => phase(1),
      I1 => phase(0),
      I2 => phase(3),
      I3 => phase(2),
      I4 => limit_irq_INST_0_i_4_n_0,
      O => \ADDER_EN[1].trigack_pending_i_3_n_0\
    );
\ADDER_EN[1].trigack_pending_reg\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ADDER_EN[1].trigack_pending_i_1_n_0\,
      Q => \ADDER_EN[1].trigack_pending_reg_n_0\,
      R => '0'
    );
\ADDER_EN[2].ctr[2][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => limit_irq_INST_0_i_2_n_0,
      I1 => \ADDER_EN[2].ctr[2][0]_i_3_n_0\,
      I2 => limit_irq_INST_0_i_4_n_0,
      I3 => limit_irq_INST_0_i_3_n_0,
      O => p_10_out
    );
\ADDER_EN[2].ctr[2][0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000080"
    )
        port map (
      I0 => phase(0),
      I1 => phase(1),
      I2 => pmu_trigin,
      I3 => \ADDER_EN[2].trigack_pending_reg_n_0\,
      I4 => phase(3),
      I5 => phase(2),
      O => \ADDER_EN[2].ctr[2][0]_i_3_n_0\
    );
\ADDER_EN[2].ctr[2][0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(0),
      O => \ADDER_EN[2].ctr[2][0]_i_4_n_0\
    );
\ADDER_EN[2].ctr_reg[2][0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_15\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(0)
    );
\ADDER_EN[2].ctr_reg[2][0]_i_2\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_0\,
      CO(6) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_1\,
      CO(5) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_2\,
      CO(4) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_3\,
      CO(3) => \NLW_ADDER_EN[2].ctr_reg[2][0]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_5\,
      CO(1) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_6\,
      CO(0) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_7\,
      DI(7 downto 0) => B"00000001",
      O(7) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_8\,
      O(6) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_9\,
      O(5) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_10\,
      O(4) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_11\,
      O(3) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_12\,
      O(2) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_13\,
      O(1) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_14\,
      O(0) => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_15\,
      S(7 downto 1) => \ADDER_EN[2].ctr_reg[2]_1\(7 downto 1),
      S(0) => \ADDER_EN[2].ctr[2][0]_i_4_n_0\
    );
\ADDER_EN[2].ctr_reg[2][10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_13\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(10)
    );
\ADDER_EN[2].ctr_reg[2][11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_12\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(11)
    );
\ADDER_EN[2].ctr_reg[2][12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_11\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(12)
    );
\ADDER_EN[2].ctr_reg[2][13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_10\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(13)
    );
\ADDER_EN[2].ctr_reg[2][14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_9\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(14)
    );
\ADDER_EN[2].ctr_reg[2][15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_8\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(15)
    );
\ADDER_EN[2].ctr_reg[2][16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_15\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(16)
    );
\ADDER_EN[2].ctr_reg[2][16]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_0\,
      CO(6) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_1\,
      CO(5) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_2\,
      CO(4) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_3\,
      CO(3) => \NLW_ADDER_EN[2].ctr_reg[2][16]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_5\,
      CO(1) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_6\,
      CO(0) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_8\,
      O(6) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_9\,
      O(5) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_10\,
      O(4) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_11\,
      O(3) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_12\,
      O(2) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_13\,
      O(1) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_14\,
      O(0) => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_15\,
      S(7 downto 0) => \ADDER_EN[2].ctr_reg[2]_1\(23 downto 16)
    );
\ADDER_EN[2].ctr_reg[2][17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_14\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(17)
    );
\ADDER_EN[2].ctr_reg[2][18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_13\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(18)
    );
\ADDER_EN[2].ctr_reg[2][19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_12\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(19)
    );
\ADDER_EN[2].ctr_reg[2][1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_14\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(1)
    );
\ADDER_EN[2].ctr_reg[2][20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_11\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(20)
    );
\ADDER_EN[2].ctr_reg[2][21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_10\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(21)
    );
\ADDER_EN[2].ctr_reg[2][22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_9\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(22)
    );
\ADDER_EN[2].ctr_reg[2][23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_8\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(23)
    );
\ADDER_EN[2].ctr_reg[2][24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_15\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(24)
    );
\ADDER_EN[2].ctr_reg[2][24]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ADDER_EN[2].ctr_reg[2][16]_i_1_n_0\,
      CI_TOP => '0',
      CO(7) => \NLW_ADDER_EN[2].ctr_reg[2][24]_i_1_CO_UNCONNECTED\(7),
      CO(6) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_1\,
      CO(5) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_2\,
      CO(4) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_3\,
      CO(3) => \NLW_ADDER_EN[2].ctr_reg[2][24]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_5\,
      CO(1) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_6\,
      CO(0) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_8\,
      O(6) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_9\,
      O(5) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_10\,
      O(4) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_11\,
      O(3) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_12\,
      O(2) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_13\,
      O(1) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_14\,
      O(0) => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_15\,
      S(7 downto 0) => \ADDER_EN[2].ctr_reg[2]_1\(31 downto 24)
    );
\ADDER_EN[2].ctr_reg[2][25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_14\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(25)
    );
\ADDER_EN[2].ctr_reg[2][26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_13\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(26)
    );
\ADDER_EN[2].ctr_reg[2][27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_12\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(27)
    );
\ADDER_EN[2].ctr_reg[2][28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_11\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(28)
    );
\ADDER_EN[2].ctr_reg[2][29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_10\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(29)
    );
\ADDER_EN[2].ctr_reg[2][2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_13\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(2)
    );
\ADDER_EN[2].ctr_reg[2][30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_9\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(30)
    );
\ADDER_EN[2].ctr_reg[2][31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][24]_i_1_n_8\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(31)
    );
\ADDER_EN[2].ctr_reg[2][3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_12\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(3)
    );
\ADDER_EN[2].ctr_reg[2][4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_11\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(4)
    );
\ADDER_EN[2].ctr_reg[2][5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_10\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(5)
    );
\ADDER_EN[2].ctr_reg[2][6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_9\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(6)
    );
\ADDER_EN[2].ctr_reg[2][7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_8\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(7)
    );
\ADDER_EN[2].ctr_reg[2][8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_15\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(8)
    );
\ADDER_EN[2].ctr_reg[2][8]_i_1\: unisim.vcomponents.CARRY8
     port map (
      CI => \ADDER_EN[2].ctr_reg[2][0]_i_2_n_0\,
      CI_TOP => '0',
      CO(7) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_0\,
      CO(6) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_1\,
      CO(5) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_2\,
      CO(4) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_3\,
      CO(3) => \NLW_ADDER_EN[2].ctr_reg[2][8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_5\,
      CO(1) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_6\,
      CO(0) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_8\,
      O(6) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_9\,
      O(5) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_10\,
      O(4) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_11\,
      O(3) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_12\,
      O(2) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_13\,
      O(1) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_14\,
      O(0) => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_15\,
      S(7 downto 0) => \ADDER_EN[2].ctr_reg[2]_1\(15 downto 8)
    );
\ADDER_EN[2].ctr_reg[2][9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => p_10_out,
      CLR => \ADDER_EN[1].ctr[1][0]_i_3_n_0\,
      D => \ADDER_EN[2].ctr_reg[2][8]_i_1_n_14\,
      Q => \ADDER_EN[2].ctr_reg[2]_1\(9)
    );
\ADDER_EN[2].trigack_pending_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCFCECCCCC0CE"
    )
        port map (
      I0 => \ADDER_EN[2].trigack_pending_i_2_n_0\,
      I1 => pmu_trigin,
      I2 => limit_irq_INST_0_i_3_n_0,
      I3 => \ADDER_EN[1].trigack_pending_i_3_n_0\,
      I4 => limit_irq_INST_0_i_2_n_0,
      I5 => \ADDER_EN[2].trigack_pending_reg_n_0\,
      O => \ADDER_EN[2].trigack_pending_i_1_n_0\
    );
\ADDER_EN[2].trigack_pending_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => \ADDER_EN[1].ctr[1][0]_i_6_n_0\,
      I1 => \ADDER_EN[2].trigack_pending_reg_n_0\,
      I2 => pmu_trigin,
      I3 => phase(1),
      I4 => phase(0),
      I5 => limit_irq_INST_0_i_4_n_0,
      O => \ADDER_EN[2].trigack_pending_i_2_n_0\
    );
\ADDER_EN[2].trigack_pending_reg\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \ADDER_EN[2].trigack_pending_i_1_n_0\,
      Q => \ADDER_EN[2].trigack_pending_reg_n_0\,
      R => '0'
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(0),
      Q => sel0(0),
      S => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(1),
      Q => sel0(1),
      S => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(2),
      Q => sel0(2),
      S => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => p_0_in
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(0),
      Q => axi_awaddr(2),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(1),
      Q => axi_awaddr(3),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(2),
      Q => axi_awaddr(4),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => p_0_in
    );
axi_bvalid_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => write_finish,
      I1 => s00_axi_bready,
      I2 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => p_0_in
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(0),
      I1 => wb_limit(0),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(0),
      I5 => comp_limit(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(0),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(0),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(0),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(10),
      I1 => wb_limit(10),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(10),
      I5 => comp_limit(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(10),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(10),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(10),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(11),
      I1 => wb_limit(11),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(11),
      I5 => comp_limit(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(11),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(11),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(11),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(12),
      I1 => wb_limit(12),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(12),
      I5 => comp_limit(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(12),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(12),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(12),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(13),
      I1 => wb_limit(13),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(13),
      I5 => comp_limit(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(13),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(13),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(13),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(14),
      I1 => wb_limit(14),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(14),
      I5 => comp_limit(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(14),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(14),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(14),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(15),
      I1 => wb_limit(15),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(15),
      I5 => comp_limit(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(15),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(15),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(15),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(16),
      I1 => wb_limit(16),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(16),
      I5 => comp_limit(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(16),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(16),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(16),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(17),
      I1 => wb_limit(17),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(17),
      I5 => comp_limit(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(17),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(17),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(17),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(18),
      I1 => wb_limit(18),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(18),
      I5 => comp_limit(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(18),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(18),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(18),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(19),
      I1 => wb_limit(19),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(19),
      I5 => comp_limit(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(19),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(19),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(19),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(1),
      I1 => wb_limit(1),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(1),
      I5 => comp_limit(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(1),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(1),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(1),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(20),
      I1 => wb_limit(20),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(20),
      I5 => comp_limit(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(20),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(20),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(20),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(21),
      I1 => wb_limit(21),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(21),
      I5 => comp_limit(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(21),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(21),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(21),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(22),
      I1 => wb_limit(22),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(22),
      I5 => comp_limit(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(22),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(22),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(22),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(23),
      I1 => wb_limit(23),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(23),
      I5 => comp_limit(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(23),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(23),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(23),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(24),
      I1 => wb_limit(24),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(24),
      I5 => comp_limit(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(24),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(24),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(24),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(25),
      I1 => wb_limit(25),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(25),
      I5 => comp_limit(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(25),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(25),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(25),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(26),
      I1 => wb_limit(26),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(26),
      I5 => comp_limit(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(26),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(26),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(26),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(27),
      I1 => wb_limit(27),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(27),
      I5 => comp_limit(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(27),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(27),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(27),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(28),
      I1 => wb_limit(28),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(28),
      I5 => comp_limit(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(28),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(28),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(28),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(29),
      I1 => wb_limit(29),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(29),
      I5 => comp_limit(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(29),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(29),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(29),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(2),
      I1 => wb_limit(2),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(2),
      I5 => comp_limit(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(2),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(2),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(2),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(30),
      I1 => wb_limit(30),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(30),
      I5 => comp_limit(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(30),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(30),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(30),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(31),
      I1 => wb_limit(31),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(31),
      I5 => comp_limit(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(31),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(31),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(31),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[31]_i_4_n_0\
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(3),
      I1 => wb_limit(3),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(3),
      I5 => comp_limit(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(3),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(3),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(3),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(4),
      I1 => wb_limit(4),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(4),
      I5 => comp_limit(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(4),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(4),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(4),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(5),
      I1 => wb_limit(5),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(5),
      I5 => comp_limit(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(5),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(5),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(5),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(6),
      I1 => wb_limit(6),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(6),
      I5 => comp_limit(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(6),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(6),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(6),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(7),
      I1 => wb_limit(7),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(7),
      I5 => comp_limit(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(7),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(7),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(7),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(8),
      I1 => wb_limit(8),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(8),
      I5 => comp_limit(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(8),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(8),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(8),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => pref_limit(9),
      I1 => wb_limit(9),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => phase(9),
      I5 => comp_limit(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCF0AA"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(9),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(9),
      I2 => \ADDER_EN[1].ctr_reg[1]_0\(9),
      I3 => sel0(0),
      I4 => sel0(1),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[0]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_2_n_0\,
      I1 => \axi_rdata[0]_i_3_n_0\,
      O => reg_data_out(0),
      S => sel0(2)
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[10]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_2_n_0\,
      I1 => \axi_rdata[10]_i_3_n_0\,
      O => reg_data_out(10),
      S => sel0(2)
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[11]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_2_n_0\,
      I1 => \axi_rdata[11]_i_3_n_0\,
      O => reg_data_out(11),
      S => sel0(2)
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[12]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_2_n_0\,
      I1 => \axi_rdata[12]_i_3_n_0\,
      O => reg_data_out(12),
      S => sel0(2)
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[13]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_2_n_0\,
      I1 => \axi_rdata[13]_i_3_n_0\,
      O => reg_data_out(13),
      S => sel0(2)
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[14]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_2_n_0\,
      I1 => \axi_rdata[14]_i_3_n_0\,
      O => reg_data_out(14),
      S => sel0(2)
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[15]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata[15]_i_3_n_0\,
      O => reg_data_out(15),
      S => sel0(2)
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[16]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_2_n_0\,
      I1 => \axi_rdata[16]_i_3_n_0\,
      O => reg_data_out(16),
      S => sel0(2)
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[17]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_2_n_0\,
      I1 => \axi_rdata[17]_i_3_n_0\,
      O => reg_data_out(17),
      S => sel0(2)
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[18]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_2_n_0\,
      I1 => \axi_rdata[18]_i_3_n_0\,
      O => reg_data_out(18),
      S => sel0(2)
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[19]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_2_n_0\,
      I1 => \axi_rdata[19]_i_3_n_0\,
      O => reg_data_out(19),
      S => sel0(2)
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[1]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_2_n_0\,
      I1 => \axi_rdata[1]_i_3_n_0\,
      O => reg_data_out(1),
      S => sel0(2)
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[20]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_2_n_0\,
      I1 => \axi_rdata[20]_i_3_n_0\,
      O => reg_data_out(20),
      S => sel0(2)
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[21]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_2_n_0\,
      I1 => \axi_rdata[21]_i_3_n_0\,
      O => reg_data_out(21),
      S => sel0(2)
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[22]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_2_n_0\,
      I1 => \axi_rdata[22]_i_3_n_0\,
      O => reg_data_out(22),
      S => sel0(2)
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[23]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_2_n_0\,
      I1 => \axi_rdata[23]_i_3_n_0\,
      O => reg_data_out(23),
      S => sel0(2)
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[24]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => \axi_rdata[24]_i_3_n_0\,
      O => reg_data_out(24),
      S => sel0(2)
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[25]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_2_n_0\,
      I1 => \axi_rdata[25]_i_3_n_0\,
      O => reg_data_out(25),
      S => sel0(2)
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[26]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_2_n_0\,
      I1 => \axi_rdata[26]_i_3_n_0\,
      O => reg_data_out(26),
      S => sel0(2)
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[27]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_2_n_0\,
      I1 => \axi_rdata[27]_i_3_n_0\,
      O => reg_data_out(27),
      S => sel0(2)
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[28]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_2_n_0\,
      I1 => \axi_rdata[28]_i_3_n_0\,
      O => reg_data_out(28),
      S => sel0(2)
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[29]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_2_n_0\,
      I1 => \axi_rdata[29]_i_3_n_0\,
      O => reg_data_out(29),
      S => sel0(2)
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[2]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_2_n_0\,
      I1 => \axi_rdata[2]_i_3_n_0\,
      O => reg_data_out(2),
      S => sel0(2)
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[30]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_2_n_0\,
      I1 => \axi_rdata[30]_i_3_n_0\,
      O => reg_data_out(30),
      S => sel0(2)
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[31]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_3_n_0\,
      I1 => \axi_rdata[31]_i_4_n_0\,
      O => reg_data_out(31),
      S => sel0(2)
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[3]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_2_n_0\,
      I1 => \axi_rdata[3]_i_3_n_0\,
      O => reg_data_out(3),
      S => sel0(2)
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[4]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => \axi_rdata[4]_i_3_n_0\,
      O => reg_data_out(4),
      S => sel0(2)
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[5]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_2_n_0\,
      I1 => \axi_rdata[5]_i_3_n_0\,
      O => reg_data_out(5),
      S => sel0(2)
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[6]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_2_n_0\,
      I1 => \axi_rdata[6]_i_3_n_0\,
      O => reg_data_out(6),
      S => sel0(2)
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[7]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_2_n_0\,
      I1 => \axi_rdata[7]_i_3_n_0\,
      O => reg_data_out(7),
      S => sel0(2)
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[8]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_2_n_0\,
      I1 => \axi_rdata[8]_i_3_n_0\,
      O => reg_data_out(8),
      S => sel0(2)
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => p_0_in
    );
\axi_rdata_reg[9]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_2_n_0\,
      I1 => \axi_rdata[9]_i_3_n_0\,
      O => reg_data_out(9),
      S => sel0(2)
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => p_0_in
    );
\comp_limit[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(1),
      I4 => axi_awaddr(3),
      O => \comp_limit[15]_i_1_n_0\
    );
\comp_limit[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(2),
      I4 => axi_awaddr(3),
      O => \comp_limit[23]_i_1_n_0\
    );
\comp_limit[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(3),
      I4 => axi_awaddr(3),
      O => \comp_limit[31]_i_1_n_0\
    );
\comp_limit[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(0),
      I4 => axi_awaddr(3),
      O => \comp_limit[7]_i_1_n_0\
    );
\comp_limit_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => comp_limit(0),
      R => p_0_in
    );
\comp_limit_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => comp_limit(10),
      R => p_0_in
    );
\comp_limit_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => comp_limit(11),
      R => p_0_in
    );
\comp_limit_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => comp_limit(12),
      R => p_0_in
    );
\comp_limit_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => comp_limit(13),
      R => p_0_in
    );
\comp_limit_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => comp_limit(14),
      R => p_0_in
    );
\comp_limit_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => comp_limit(15),
      R => p_0_in
    );
\comp_limit_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => comp_limit(16),
      R => p_0_in
    );
\comp_limit_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => comp_limit(17),
      R => p_0_in
    );
\comp_limit_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => comp_limit(18),
      R => p_0_in
    );
\comp_limit_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => comp_limit(19),
      R => p_0_in
    );
\comp_limit_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => comp_limit(1),
      R => p_0_in
    );
\comp_limit_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => comp_limit(20),
      R => p_0_in
    );
\comp_limit_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => comp_limit(21),
      R => p_0_in
    );
\comp_limit_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => comp_limit(22),
      R => p_0_in
    );
\comp_limit_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => comp_limit(23),
      R => p_0_in
    );
\comp_limit_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => comp_limit(24),
      R => p_0_in
    );
\comp_limit_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => comp_limit(25),
      R => p_0_in
    );
\comp_limit_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => comp_limit(26),
      R => p_0_in
    );
\comp_limit_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => comp_limit(27),
      R => p_0_in
    );
\comp_limit_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => comp_limit(28),
      R => p_0_in
    );
\comp_limit_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => comp_limit(29),
      R => p_0_in
    );
\comp_limit_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => comp_limit(2),
      R => p_0_in
    );
\comp_limit_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => comp_limit(30),
      R => p_0_in
    );
\comp_limit_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => comp_limit(31),
      R => p_0_in
    );
\comp_limit_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => comp_limit(3),
      R => p_0_in
    );
\comp_limit_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => comp_limit(4),
      R => p_0_in
    );
\comp_limit_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => comp_limit(5),
      R => p_0_in
    );
\comp_limit_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => comp_limit(6),
      R => p_0_in
    );
\comp_limit_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => comp_limit(7),
      R => p_0_in
    );
\comp_limit_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => comp_limit(8),
      R => p_0_in
    );
\comp_limit_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \comp_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => comp_limit(9),
      R => p_0_in
    );
gtOp_carry: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => gtOp_carry_n_0,
      CO(6) => gtOp_carry_n_1,
      CO(5) => gtOp_carry_n_2,
      CO(4) => gtOp_carry_n_3,
      CO(3) => NLW_gtOp_carry_CO_UNCONNECTED(3),
      CO(2) => gtOp_carry_n_5,
      CO(1) => gtOp_carry_n_6,
      CO(0) => gtOp_carry_n_7,
      DI(7) => gtOp_carry_i_1_n_0,
      DI(6) => gtOp_carry_i_2_n_0,
      DI(5) => gtOp_carry_i_3_n_0,
      DI(4) => gtOp_carry_i_4_n_0,
      DI(3) => gtOp_carry_i_5_n_0,
      DI(2) => gtOp_carry_i_6_n_0,
      DI(1) => gtOp_carry_i_7_n_0,
      DI(0) => gtOp_carry_i_8_n_0,
      O(7 downto 0) => NLW_gtOp_carry_O_UNCONNECTED(7 downto 0),
      S(7) => gtOp_carry_i_9_n_0,
      S(6) => gtOp_carry_i_10_n_0,
      S(5) => gtOp_carry_i_11_n_0,
      S(4) => gtOp_carry_i_12_n_0,
      S(3) => gtOp_carry_i_13_n_0,
      S(2) => gtOp_carry_i_14_n_0,
      S(1) => gtOp_carry_i_15_n_0,
      S(0) => gtOp_carry_i_16_n_0
    );
\gtOp_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => gtOp_carry_n_0,
      CI_TOP => '0',
      CO(7) => \gtOp_carry__0_n_0\,
      CO(6) => \gtOp_carry__0_n_1\,
      CO(5) => \gtOp_carry__0_n_2\,
      CO(4) => \gtOp_carry__0_n_3\,
      CO(3) => \NLW_gtOp_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \gtOp_carry__0_n_5\,
      CO(1) => \gtOp_carry__0_n_6\,
      CO(0) => \gtOp_carry__0_n_7\,
      DI(7) => \gtOp_carry__0_i_1_n_0\,
      DI(6) => \gtOp_carry__0_i_2_n_0\,
      DI(5) => \gtOp_carry__0_i_3_n_0\,
      DI(4) => \gtOp_carry__0_i_4_n_0\,
      DI(3) => \gtOp_carry__0_i_5_n_0\,
      DI(2) => \gtOp_carry__0_i_6_n_0\,
      DI(1) => \gtOp_carry__0_i_7_n_0\,
      DI(0) => \gtOp_carry__0_i_8_n_0\,
      O(7 downto 0) => \NLW_gtOp_carry__0_O_UNCONNECTED\(7 downto 0),
      S(7) => \gtOp_carry__0_i_9_n_0\,
      S(6) => \gtOp_carry__0_i_10_n_0\,
      S(5) => \gtOp_carry__0_i_11_n_0\,
      S(4) => \gtOp_carry__0_i_12_n_0\,
      S(3) => \gtOp_carry__0_i_13_n_0\,
      S(2) => \gtOp_carry__0_i_14_n_0\,
      S(1) => \gtOp_carry__0_i_15_n_0\,
      S(0) => \gtOp_carry__0_i_16_n_0\
    );
\gtOp_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(30),
      I1 => comp_limit(30),
      I2 => comp_limit(31),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(31),
      O => \gtOp_carry__0_i_1_n_0\
    );
\gtOp_carry__0_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(29),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(29),
      I2 => comp_limit(28),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(28),
      O => \gtOp_carry__0_i_10_n_0\
    );
\gtOp_carry__0_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(27),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(27),
      I2 => comp_limit(26),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(26),
      O => \gtOp_carry__0_i_11_n_0\
    );
\gtOp_carry__0_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(25),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(25),
      I2 => comp_limit(24),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(24),
      O => \gtOp_carry__0_i_12_n_0\
    );
\gtOp_carry__0_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(23),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(23),
      I2 => comp_limit(22),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(22),
      O => \gtOp_carry__0_i_13_n_0\
    );
\gtOp_carry__0_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(21),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(21),
      I2 => comp_limit(20),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(20),
      O => \gtOp_carry__0_i_14_n_0\
    );
\gtOp_carry__0_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(19),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(19),
      I2 => comp_limit(18),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(18),
      O => \gtOp_carry__0_i_15_n_0\
    );
\gtOp_carry__0_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(17),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(17),
      I2 => comp_limit(16),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(16),
      O => \gtOp_carry__0_i_16_n_0\
    );
\gtOp_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(28),
      I1 => comp_limit(28),
      I2 => comp_limit(29),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(29),
      O => \gtOp_carry__0_i_2_n_0\
    );
\gtOp_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(26),
      I1 => comp_limit(26),
      I2 => comp_limit(27),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(27),
      O => \gtOp_carry__0_i_3_n_0\
    );
\gtOp_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(24),
      I1 => comp_limit(24),
      I2 => comp_limit(25),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(25),
      O => \gtOp_carry__0_i_4_n_0\
    );
\gtOp_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(22),
      I1 => comp_limit(22),
      I2 => comp_limit(23),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(23),
      O => \gtOp_carry__0_i_5_n_0\
    );
\gtOp_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(20),
      I1 => comp_limit(20),
      I2 => comp_limit(21),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(21),
      O => \gtOp_carry__0_i_6_n_0\
    );
\gtOp_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(18),
      I1 => comp_limit(18),
      I2 => comp_limit(19),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(19),
      O => \gtOp_carry__0_i_7_n_0\
    );
\gtOp_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(16),
      I1 => comp_limit(16),
      I2 => comp_limit(17),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(17),
      O => \gtOp_carry__0_i_8_n_0\
    );
\gtOp_carry__0_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(31),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(31),
      I2 => comp_limit(30),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(30),
      O => \gtOp_carry__0_i_9_n_0\
    );
gtOp_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(14),
      I1 => comp_limit(14),
      I2 => comp_limit(15),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(15),
      O => gtOp_carry_i_1_n_0
    );
gtOp_carry_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(13),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(13),
      I2 => comp_limit(12),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(12),
      O => gtOp_carry_i_10_n_0
    );
gtOp_carry_i_11: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(11),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(11),
      I2 => comp_limit(10),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(10),
      O => gtOp_carry_i_11_n_0
    );
gtOp_carry_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(9),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(9),
      I2 => comp_limit(8),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(8),
      O => gtOp_carry_i_12_n_0
    );
gtOp_carry_i_13: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(7),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(7),
      I2 => comp_limit(6),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(6),
      O => gtOp_carry_i_13_n_0
    );
gtOp_carry_i_14: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(5),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(5),
      I2 => comp_limit(4),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(4),
      O => gtOp_carry_i_14_n_0
    );
gtOp_carry_i_15: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(3),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(3),
      I2 => comp_limit(2),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(2),
      O => gtOp_carry_i_15_n_0
    );
gtOp_carry_i_16: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(1),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(1),
      I2 => comp_limit(0),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(0),
      O => gtOp_carry_i_16_n_0
    );
gtOp_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(12),
      I1 => comp_limit(12),
      I2 => comp_limit(13),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(13),
      O => gtOp_carry_i_2_n_0
    );
gtOp_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(10),
      I1 => comp_limit(10),
      I2 => comp_limit(11),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(11),
      O => gtOp_carry_i_3_n_0
    );
gtOp_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(8),
      I1 => comp_limit(8),
      I2 => comp_limit(9),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(9),
      O => gtOp_carry_i_4_n_0
    );
gtOp_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(6),
      I1 => comp_limit(6),
      I2 => comp_limit(7),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(7),
      O => gtOp_carry_i_5_n_0
    );
gtOp_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(4),
      I1 => comp_limit(4),
      I2 => comp_limit(5),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(5),
      O => gtOp_carry_i_6_n_0
    );
gtOp_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(2),
      I1 => comp_limit(2),
      I2 => comp_limit(3),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(3),
      O => gtOp_carry_i_7_n_0
    );
gtOp_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[1].ctr_reg[1]_0\(0),
      I1 => comp_limit(0),
      I2 => comp_limit(1),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(1),
      O => gtOp_carry_i_8_n_0
    );
gtOp_carry_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => comp_limit(15),
      I1 => \ADDER_EN[1].ctr_reg[1]_0\(15),
      I2 => comp_limit(14),
      I3 => \ADDER_EN[1].ctr_reg[1]_0\(14),
      O => gtOp_carry_i_9_n_0
    );
\gtOp_inferred__0/i__carry\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \gtOp_inferred__0/i__carry_n_0\,
      CO(6) => \gtOp_inferred__0/i__carry_n_1\,
      CO(5) => \gtOp_inferred__0/i__carry_n_2\,
      CO(4) => \gtOp_inferred__0/i__carry_n_3\,
      CO(3) => \NLW_gtOp_inferred__0/i__carry_CO_UNCONNECTED\(3),
      CO(2) => \gtOp_inferred__0/i__carry_n_5\,
      CO(1) => \gtOp_inferred__0/i__carry_n_6\,
      CO(0) => \gtOp_inferred__0/i__carry_n_7\,
      DI(7) => \i__carry_i_1_n_0\,
      DI(6) => \i__carry_i_2_n_0\,
      DI(5) => \i__carry_i_3_n_0\,
      DI(4) => \i__carry_i_4_n_0\,
      DI(3) => \i__carry_i_5_n_0\,
      DI(2) => \i__carry_i_6_n_0\,
      DI(1) => \i__carry_i_7_n_0\,
      DI(0) => \i__carry_i_8_n_0\,
      O(7 downto 0) => \NLW_gtOp_inferred__0/i__carry_O_UNCONNECTED\(7 downto 0),
      S(7) => \i__carry_i_9_n_0\,
      S(6) => \i__carry_i_10_n_0\,
      S(5) => \i__carry_i_11_n_0\,
      S(4) => \i__carry_i_12_n_0\,
      S(3) => \i__carry_i_13_n_0\,
      S(2) => \i__carry_i_14_n_0\,
      S(1) => \i__carry_i_15_n_0\,
      S(0) => \i__carry_i_16_n_0\
    );
\gtOp_inferred__0/i__carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => \gtOp_inferred__0/i__carry_n_0\,
      CI_TOP => '0',
      CO(7) => \gtOp_inferred__0/i__carry__0_n_0\,
      CO(6) => \gtOp_inferred__0/i__carry__0_n_1\,
      CO(5) => \gtOp_inferred__0/i__carry__0_n_2\,
      CO(4) => \gtOp_inferred__0/i__carry__0_n_3\,
      CO(3) => \NLW_gtOp_inferred__0/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => \gtOp_inferred__0/i__carry__0_n_5\,
      CO(1) => \gtOp_inferred__0/i__carry__0_n_6\,
      CO(0) => \gtOp_inferred__0/i__carry__0_n_7\,
      DI(7) => \i__carry__0_i_1_n_0\,
      DI(6) => \i__carry__0_i_2_n_0\,
      DI(5) => \i__carry__0_i_3_n_0\,
      DI(4) => \i__carry__0_i_4_n_0\,
      DI(3) => \i__carry__0_i_5_n_0\,
      DI(2) => \i__carry__0_i_6_n_0\,
      DI(1) => \i__carry__0_i_7_n_0\,
      DI(0) => \i__carry__0_i_8_n_0\,
      O(7 downto 0) => \NLW_gtOp_inferred__0/i__carry__0_O_UNCONNECTED\(7 downto 0),
      S(7) => \i__carry__0_i_9_n_0\,
      S(6) => \i__carry__0_i_10_n_0\,
      S(5) => \i__carry__0_i_11_n_0\,
      S(4) => \i__carry__0_i_12_n_0\,
      S(3) => \i__carry__0_i_13_n_0\,
      S(2) => \i__carry__0_i_14_n_0\,
      S(1) => \i__carry__0_i_15_n_0\,
      S(0) => \i__carry__0_i_16_n_0\
    );
\gtOp_inferred__1/i__carry\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \gtOp_inferred__1/i__carry_n_0\,
      CO(6) => \gtOp_inferred__1/i__carry_n_1\,
      CO(5) => \gtOp_inferred__1/i__carry_n_2\,
      CO(4) => \gtOp_inferred__1/i__carry_n_3\,
      CO(3) => \NLW_gtOp_inferred__1/i__carry_CO_UNCONNECTED\(3),
      CO(2) => \gtOp_inferred__1/i__carry_n_5\,
      CO(1) => \gtOp_inferred__1/i__carry_n_6\,
      CO(0) => \gtOp_inferred__1/i__carry_n_7\,
      DI(7) => \i__carry_i_1__0_n_0\,
      DI(6) => \i__carry_i_2__0_n_0\,
      DI(5) => \i__carry_i_3__0_n_0\,
      DI(4) => \i__carry_i_4__0_n_0\,
      DI(3) => \i__carry_i_5__0_n_0\,
      DI(2) => \i__carry_i_6__0_n_0\,
      DI(1) => \i__carry_i_7__0_n_0\,
      DI(0) => \i__carry_i_8__0_n_0\,
      O(7 downto 0) => \NLW_gtOp_inferred__1/i__carry_O_UNCONNECTED\(7 downto 0),
      S(7) => \i__carry_i_9__0_n_0\,
      S(6) => \i__carry_i_10__0_n_0\,
      S(5) => \i__carry_i_11__0_n_0\,
      S(4) => \i__carry_i_12__0_n_0\,
      S(3) => \i__carry_i_13__0_n_0\,
      S(2) => \i__carry_i_14__0_n_0\,
      S(1) => \i__carry_i_15__0_n_0\,
      S(0) => \i__carry_i_16__0_n_0\
    );
\gtOp_inferred__1/i__carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => \gtOp_inferred__1/i__carry_n_0\,
      CI_TOP => '0',
      CO(7) => gtOp,
      CO(6) => \gtOp_inferred__1/i__carry__0_n_1\,
      CO(5) => \gtOp_inferred__1/i__carry__0_n_2\,
      CO(4) => \gtOp_inferred__1/i__carry__0_n_3\,
      CO(3) => \NLW_gtOp_inferred__1/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => \gtOp_inferred__1/i__carry__0_n_5\,
      CO(1) => \gtOp_inferred__1/i__carry__0_n_6\,
      CO(0) => \gtOp_inferred__1/i__carry__0_n_7\,
      DI(7) => \i__carry__0_i_1__0_n_0\,
      DI(6) => \i__carry__0_i_2__0_n_0\,
      DI(5) => \i__carry__0_i_3__0_n_0\,
      DI(4) => \i__carry__0_i_4__0_n_0\,
      DI(3) => \i__carry__0_i_5__0_n_0\,
      DI(2) => \i__carry__0_i_6__0_n_0\,
      DI(1) => \i__carry__0_i_7__0_n_0\,
      DI(0) => \i__carry__0_i_8__0_n_0\,
      O(7 downto 0) => \NLW_gtOp_inferred__1/i__carry__0_O_UNCONNECTED\(7 downto 0),
      S(7) => \i__carry__0_i_9__0_n_0\,
      S(6) => \i__carry__0_i_10__0_n_0\,
      S(5) => \i__carry__0_i_11__0_n_0\,
      S(4) => \i__carry__0_i_12__0_n_0\,
      S(3) => \i__carry__0_i_13__0_n_0\,
      S(2) => \i__carry__0_i_14__0_n_0\,
      S(1) => \i__carry__0_i_15__0_n_0\,
      S(0) => \i__carry__0_i_16__0_n_0\
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(30),
      I1 => wb_limit(30),
      I2 => wb_limit(31),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(31),
      O => \i__carry__0_i_1_n_0\
    );
\i__carry__0_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(29),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(29),
      I2 => wb_limit(28),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(28),
      O => \i__carry__0_i_10_n_0\
    );
\i__carry__0_i_10__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(29),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(29),
      I2 => pref_limit(28),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(28),
      O => \i__carry__0_i_10__0_n_0\
    );
\i__carry__0_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(27),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(27),
      I2 => wb_limit(26),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(26),
      O => \i__carry__0_i_11_n_0\
    );
\i__carry__0_i_11__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(27),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(27),
      I2 => pref_limit(26),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(26),
      O => \i__carry__0_i_11__0_n_0\
    );
\i__carry__0_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(25),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(25),
      I2 => wb_limit(24),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(24),
      O => \i__carry__0_i_12_n_0\
    );
\i__carry__0_i_12__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(25),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(25),
      I2 => pref_limit(24),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(24),
      O => \i__carry__0_i_12__0_n_0\
    );
\i__carry__0_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(23),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(23),
      I2 => wb_limit(22),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(22),
      O => \i__carry__0_i_13_n_0\
    );
\i__carry__0_i_13__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(23),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(23),
      I2 => pref_limit(22),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(22),
      O => \i__carry__0_i_13__0_n_0\
    );
\i__carry__0_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(21),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(21),
      I2 => wb_limit(20),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(20),
      O => \i__carry__0_i_14_n_0\
    );
\i__carry__0_i_14__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(21),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(21),
      I2 => pref_limit(20),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(20),
      O => \i__carry__0_i_14__0_n_0\
    );
\i__carry__0_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(19),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(19),
      I2 => wb_limit(18),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(18),
      O => \i__carry__0_i_15_n_0\
    );
\i__carry__0_i_15__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(19),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(19),
      I2 => pref_limit(18),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(18),
      O => \i__carry__0_i_15__0_n_0\
    );
\i__carry__0_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(17),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(17),
      I2 => wb_limit(16),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(16),
      O => \i__carry__0_i_16_n_0\
    );
\i__carry__0_i_16__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(17),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(17),
      I2 => pref_limit(16),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(16),
      O => \i__carry__0_i_16__0_n_0\
    );
\i__carry__0_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(30),
      I1 => pref_limit(30),
      I2 => pref_limit(31),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(31),
      O => \i__carry__0_i_1__0_n_0\
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(28),
      I1 => wb_limit(28),
      I2 => wb_limit(29),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(29),
      O => \i__carry__0_i_2_n_0\
    );
\i__carry__0_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(28),
      I1 => pref_limit(28),
      I2 => pref_limit(29),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(29),
      O => \i__carry__0_i_2__0_n_0\
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(26),
      I1 => wb_limit(26),
      I2 => wb_limit(27),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(27),
      O => \i__carry__0_i_3_n_0\
    );
\i__carry__0_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(26),
      I1 => pref_limit(26),
      I2 => pref_limit(27),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(27),
      O => \i__carry__0_i_3__0_n_0\
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(24),
      I1 => wb_limit(24),
      I2 => wb_limit(25),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(25),
      O => \i__carry__0_i_4_n_0\
    );
\i__carry__0_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(24),
      I1 => pref_limit(24),
      I2 => pref_limit(25),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(25),
      O => \i__carry__0_i_4__0_n_0\
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(22),
      I1 => wb_limit(22),
      I2 => wb_limit(23),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(23),
      O => \i__carry__0_i_5_n_0\
    );
\i__carry__0_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(22),
      I1 => pref_limit(22),
      I2 => pref_limit(23),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(23),
      O => \i__carry__0_i_5__0_n_0\
    );
\i__carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(20),
      I1 => wb_limit(20),
      I2 => wb_limit(21),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(21),
      O => \i__carry__0_i_6_n_0\
    );
\i__carry__0_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(20),
      I1 => pref_limit(20),
      I2 => pref_limit(21),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(21),
      O => \i__carry__0_i_6__0_n_0\
    );
\i__carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(18),
      I1 => wb_limit(18),
      I2 => wb_limit(19),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(19),
      O => \i__carry__0_i_7_n_0\
    );
\i__carry__0_i_7__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(18),
      I1 => pref_limit(18),
      I2 => pref_limit(19),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(19),
      O => \i__carry__0_i_7__0_n_0\
    );
\i__carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(16),
      I1 => wb_limit(16),
      I2 => wb_limit(17),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(17),
      O => \i__carry__0_i_8_n_0\
    );
\i__carry__0_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(16),
      I1 => pref_limit(16),
      I2 => pref_limit(17),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(17),
      O => \i__carry__0_i_8__0_n_0\
    );
\i__carry__0_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(31),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(31),
      I2 => wb_limit(30),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(30),
      O => \i__carry__0_i_9_n_0\
    );
\i__carry__0_i_9__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(31),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(31),
      I2 => pref_limit(30),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(30),
      O => \i__carry__0_i_9__0_n_0\
    );
\i__carry_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(14),
      I1 => wb_limit(14),
      I2 => wb_limit(15),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(15),
      O => \i__carry_i_1_n_0\
    );
\i__carry_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(13),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(13),
      I2 => wb_limit(12),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(12),
      O => \i__carry_i_10_n_0\
    );
\i__carry_i_10__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(13),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(13),
      I2 => pref_limit(12),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(12),
      O => \i__carry_i_10__0_n_0\
    );
\i__carry_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(11),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(11),
      I2 => wb_limit(10),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(10),
      O => \i__carry_i_11_n_0\
    );
\i__carry_i_11__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(11),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(11),
      I2 => pref_limit(10),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(10),
      O => \i__carry_i_11__0_n_0\
    );
\i__carry_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(9),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(9),
      I2 => wb_limit(8),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(8),
      O => \i__carry_i_12_n_0\
    );
\i__carry_i_12__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(9),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(9),
      I2 => pref_limit(8),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(8),
      O => \i__carry_i_12__0_n_0\
    );
\i__carry_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(7),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(7),
      I2 => wb_limit(6),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(6),
      O => \i__carry_i_13_n_0\
    );
\i__carry_i_13__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(7),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(7),
      I2 => pref_limit(6),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(6),
      O => \i__carry_i_13__0_n_0\
    );
\i__carry_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(5),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(5),
      I2 => wb_limit(4),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(4),
      O => \i__carry_i_14_n_0\
    );
\i__carry_i_14__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(5),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(5),
      I2 => pref_limit(4),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(4),
      O => \i__carry_i_14__0_n_0\
    );
\i__carry_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(3),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(3),
      I2 => wb_limit(2),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(2),
      O => \i__carry_i_15_n_0\
    );
\i__carry_i_15__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(3),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(3),
      I2 => pref_limit(2),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(2),
      O => \i__carry_i_15__0_n_0\
    );
\i__carry_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(1),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(1),
      I2 => wb_limit(0),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(0),
      O => \i__carry_i_16_n_0\
    );
\i__carry_i_16__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(1),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(1),
      I2 => pref_limit(0),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(0),
      O => \i__carry_i_16__0_n_0\
    );
\i__carry_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(14),
      I1 => pref_limit(14),
      I2 => pref_limit(15),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(15),
      O => \i__carry_i_1__0_n_0\
    );
\i__carry_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(12),
      I1 => wb_limit(12),
      I2 => wb_limit(13),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(13),
      O => \i__carry_i_2_n_0\
    );
\i__carry_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(12),
      I1 => pref_limit(12),
      I2 => pref_limit(13),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(13),
      O => \i__carry_i_2__0_n_0\
    );
\i__carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(10),
      I1 => wb_limit(10),
      I2 => wb_limit(11),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(11),
      O => \i__carry_i_3_n_0\
    );
\i__carry_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(10),
      I1 => pref_limit(10),
      I2 => pref_limit(11),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(11),
      O => \i__carry_i_3__0_n_0\
    );
\i__carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(8),
      I1 => wb_limit(8),
      I2 => wb_limit(9),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(9),
      O => \i__carry_i_4_n_0\
    );
\i__carry_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(8),
      I1 => pref_limit(8),
      I2 => pref_limit(9),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(9),
      O => \i__carry_i_4__0_n_0\
    );
\i__carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(6),
      I1 => wb_limit(6),
      I2 => wb_limit(7),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(7),
      O => \i__carry_i_5_n_0\
    );
\i__carry_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(6),
      I1 => pref_limit(6),
      I2 => pref_limit(7),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(7),
      O => \i__carry_i_5__0_n_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(4),
      I1 => wb_limit(4),
      I2 => wb_limit(5),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(5),
      O => \i__carry_i_6_n_0\
    );
\i__carry_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(4),
      I1 => pref_limit(4),
      I2 => pref_limit(5),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(5),
      O => \i__carry_i_6__0_n_0\
    );
\i__carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(2),
      I1 => wb_limit(2),
      I2 => wb_limit(3),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(3),
      O => \i__carry_i_7_n_0\
    );
\i__carry_i_7__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(2),
      I1 => pref_limit(2),
      I2 => pref_limit(3),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(3),
      O => \i__carry_i_7__0_n_0\
    );
\i__carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[2].ctr_reg[2]_1\(0),
      I1 => wb_limit(0),
      I2 => wb_limit(1),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(1),
      O => \i__carry_i_8_n_0\
    );
\i__carry_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \ADDER_EN[0].ctr_reg[0]_2\(0),
      I1 => pref_limit(0),
      I2 => pref_limit(1),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(1),
      O => \i__carry_i_8__0_n_0\
    );
\i__carry_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => wb_limit(15),
      I1 => \ADDER_EN[2].ctr_reg[2]_1\(15),
      I2 => wb_limit(14),
      I3 => \ADDER_EN[2].ctr_reg[2]_1\(14),
      O => \i__carry_i_9_n_0\
    );
\i__carry_i_9__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => pref_limit(15),
      I1 => \ADDER_EN[0].ctr_reg[0]_2\(15),
      I2 => pref_limit(14),
      I3 => \ADDER_EN[0].ctr_reg[0]_2\(14),
      O => \i__carry_i_9__0_n_0\
    );
limit_irq_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFEFEFEFEFEFE00"
    )
        port map (
      I0 => gtOp,
      I1 => \gtOp_inferred__0/i__carry__0_n_0\,
      I2 => \gtOp_carry__0_n_0\,
      I3 => limit_irq_INST_0_i_1_n_0,
      I4 => limit_irq_INST_0_i_2_n_0,
      I5 => limit_irq_INST_0_i_3_n_0,
      O => limit_irq
    );
limit_irq_INST_0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => phase(2),
      I1 => phase(3),
      I2 => phase(0),
      I3 => phase(1),
      I4 => limit_irq_INST_0_i_4_n_0,
      O => limit_irq_INST_0_i_1_n_0
    );
limit_irq_INST_0_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => phase(8),
      I1 => phase(9),
      I2 => phase(10),
      I3 => phase(11),
      I4 => limit_irq_INST_0_i_5_n_0,
      O => limit_irq_INST_0_i_2_n_0
    );
limit_irq_INST_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => limit_irq_INST_0_i_6_n_0,
      I1 => limit_irq_INST_0_i_7_n_0,
      I2 => limit_irq_INST_0_i_8_n_0,
      I3 => limit_irq_INST_0_i_9_n_0,
      O => limit_irq_INST_0_i_3_n_0
    );
limit_irq_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => phase(5),
      I1 => phase(4),
      I2 => phase(7),
      I3 => phase(6),
      O => limit_irq_INST_0_i_4_n_0
    );
limit_irq_INST_0_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => phase(13),
      I1 => phase(12),
      I2 => phase(15),
      I3 => phase(14),
      O => limit_irq_INST_0_i_5_n_0
    );
limit_irq_INST_0_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => phase(18),
      I1 => phase(17),
      I2 => phase(26),
      I3 => phase(19),
      O => limit_irq_INST_0_i_6_n_0
    );
limit_irq_INST_0_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => phase(22),
      I1 => phase(21),
      I2 => phase(16),
      I3 => phase(23),
      O => limit_irq_INST_0_i_7_n_0
    );
limit_irq_INST_0_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => phase(30),
      I1 => phase(31),
      I2 => phase(29),
      I3 => phase(20),
      O => limit_irq_INST_0_i_8_n_0
    );
limit_irq_INST_0_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => phase(24),
      I1 => phase(27),
      I2 => phase(28),
      I3 => phase(25),
      O => limit_irq_INST_0_i_9_n_0
    );
\phase[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000020"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => s00_axi_wstrb(1),
      I3 => axi_awaddr(2),
      I4 => axi_awaddr(3),
      O => p_1_in(15)
    );
\phase[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000020"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => s00_axi_wstrb(2),
      I3 => axi_awaddr(2),
      I4 => axi_awaddr(3),
      O => p_1_in(23)
    );
\phase[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000020"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => s00_axi_wstrb(3),
      I3 => axi_awaddr(2),
      I4 => axi_awaddr(3),
      O => p_1_in(31)
    );
\phase[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s_axi_wready\,
      I3 => \^s_axi_awready\,
      O => slv_reg_wren
    );
\phase[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000020"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => s00_axi_wstrb(0),
      I3 => axi_awaddr(2),
      I4 => axi_awaddr(3),
      O => p_1_in(7)
    );
\phase_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => phase(0),
      R => p_0_in
    );
\phase_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => phase(10),
      R => p_0_in
    );
\phase_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => phase(11),
      R => p_0_in
    );
\phase_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => phase(12),
      R => p_0_in
    );
\phase_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => phase(13),
      R => p_0_in
    );
\phase_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => phase(14),
      R => p_0_in
    );
\phase_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => phase(15),
      R => p_0_in
    );
\phase_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => phase(16),
      R => p_0_in
    );
\phase_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => phase(17),
      R => p_0_in
    );
\phase_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => phase(18),
      R => p_0_in
    );
\phase_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => phase(19),
      R => p_0_in
    );
\phase_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => phase(1),
      R => p_0_in
    );
\phase_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => phase(20),
      R => p_0_in
    );
\phase_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => phase(21),
      R => p_0_in
    );
\phase_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => phase(22),
      R => p_0_in
    );
\phase_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => phase(23),
      R => p_0_in
    );
\phase_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => phase(24),
      R => p_0_in
    );
\phase_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => phase(25),
      R => p_0_in
    );
\phase_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => phase(26),
      R => p_0_in
    );
\phase_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => phase(27),
      R => p_0_in
    );
\phase_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => phase(28),
      R => p_0_in
    );
\phase_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => phase(29),
      R => p_0_in
    );
\phase_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => phase(2),
      R => p_0_in
    );
\phase_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => phase(30),
      R => p_0_in
    );
\phase_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => phase(31),
      R => p_0_in
    );
\phase_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => phase(3),
      R => p_0_in
    );
\phase_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => phase(4),
      R => p_0_in
    );
\phase_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => phase(5),
      R => p_0_in
    );
\phase_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => phase(6),
      R => p_0_in
    );
\phase_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => phase(7),
      R => p_0_in
    );
\phase_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => phase(8),
      R => p_0_in
    );
\phase_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => phase(9),
      R => p_0_in
    );
\pref_limit[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(1),
      I4 => axi_awaddr(3),
      O => \pref_limit[15]_i_1_n_0\
    );
\pref_limit[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(2),
      I4 => axi_awaddr(3),
      O => \pref_limit[23]_i_1_n_0\
    );
\pref_limit[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(3),
      I4 => axi_awaddr(3),
      O => \pref_limit[31]_i_1_n_0\
    );
\pref_limit[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(0),
      I4 => axi_awaddr(3),
      O => \pref_limit[7]_i_1_n_0\
    );
\pref_limit_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => pref_limit(0),
      R => p_0_in
    );
\pref_limit_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => pref_limit(10),
      R => p_0_in
    );
\pref_limit_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => pref_limit(11),
      R => p_0_in
    );
\pref_limit_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => pref_limit(12),
      R => p_0_in
    );
\pref_limit_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => pref_limit(13),
      R => p_0_in
    );
\pref_limit_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => pref_limit(14),
      R => p_0_in
    );
\pref_limit_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => pref_limit(15),
      R => p_0_in
    );
\pref_limit_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => pref_limit(16),
      R => p_0_in
    );
\pref_limit_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => pref_limit(17),
      R => p_0_in
    );
\pref_limit_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => pref_limit(18),
      R => p_0_in
    );
\pref_limit_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => pref_limit(19),
      R => p_0_in
    );
\pref_limit_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => pref_limit(1),
      R => p_0_in
    );
\pref_limit_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => pref_limit(20),
      R => p_0_in
    );
\pref_limit_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => pref_limit(21),
      R => p_0_in
    );
\pref_limit_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => pref_limit(22),
      R => p_0_in
    );
\pref_limit_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => pref_limit(23),
      R => p_0_in
    );
\pref_limit_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => pref_limit(24),
      R => p_0_in
    );
\pref_limit_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => pref_limit(25),
      R => p_0_in
    );
\pref_limit_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => pref_limit(26),
      R => p_0_in
    );
\pref_limit_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => pref_limit(27),
      R => p_0_in
    );
\pref_limit_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => pref_limit(28),
      R => p_0_in
    );
\pref_limit_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => pref_limit(29),
      R => p_0_in
    );
\pref_limit_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => pref_limit(2),
      R => p_0_in
    );
\pref_limit_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => pref_limit(30),
      R => p_0_in
    );
\pref_limit_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => pref_limit(31),
      R => p_0_in
    );
\pref_limit_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => pref_limit(3),
      R => p_0_in
    );
\pref_limit_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => pref_limit(4),
      R => p_0_in
    );
\pref_limit_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => pref_limit(5),
      R => p_0_in
    );
\pref_limit_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => pref_limit(6),
      R => p_0_in
    );
\pref_limit_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => pref_limit(7),
      R => p_0_in
    );
\pref_limit_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => pref_limit(8),
      R => p_0_in
    );
\pref_limit_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \pref_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => pref_limit(9),
      R => p_0_in
    );
\wb_limit[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => axi_awaddr(2),
      I4 => s00_axi_wstrb(1),
      O => \wb_limit[15]_i_1_n_0\
    );
\wb_limit[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => axi_awaddr(2),
      I4 => s00_axi_wstrb(2),
      O => \wb_limit[23]_i_1_n_0\
    );
\wb_limit[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => axi_awaddr(2),
      I4 => s00_axi_wstrb(3),
      O => \wb_limit[31]_i_1_n_0\
    );
\wb_limit[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => slv_reg_wren,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => axi_awaddr(2),
      I4 => s00_axi_wstrb(0),
      O => \wb_limit[7]_i_1_n_0\
    );
\wb_limit_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => wb_limit(0),
      R => p_0_in
    );
\wb_limit_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => wb_limit(10),
      R => p_0_in
    );
\wb_limit_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => wb_limit(11),
      R => p_0_in
    );
\wb_limit_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => wb_limit(12),
      R => p_0_in
    );
\wb_limit_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => wb_limit(13),
      R => p_0_in
    );
\wb_limit_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => wb_limit(14),
      R => p_0_in
    );
\wb_limit_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => wb_limit(15),
      R => p_0_in
    );
\wb_limit_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => wb_limit(16),
      R => p_0_in
    );
\wb_limit_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => wb_limit(17),
      R => p_0_in
    );
\wb_limit_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => wb_limit(18),
      R => p_0_in
    );
\wb_limit_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => wb_limit(19),
      R => p_0_in
    );
\wb_limit_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => wb_limit(1),
      R => p_0_in
    );
\wb_limit_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => wb_limit(20),
      R => p_0_in
    );
\wb_limit_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => wb_limit(21),
      R => p_0_in
    );
\wb_limit_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => wb_limit(22),
      R => p_0_in
    );
\wb_limit_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => wb_limit(23),
      R => p_0_in
    );
\wb_limit_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => wb_limit(24),
      R => p_0_in
    );
\wb_limit_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => wb_limit(25),
      R => p_0_in
    );
\wb_limit_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => wb_limit(26),
      R => p_0_in
    );
\wb_limit_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => wb_limit(27),
      R => p_0_in
    );
\wb_limit_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => wb_limit(28),
      R => p_0_in
    );
\wb_limit_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => wb_limit(29),
      R => p_0_in
    );
\wb_limit_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => wb_limit(2),
      R => p_0_in
    );
\wb_limit_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => wb_limit(30),
      R => p_0_in
    );
\wb_limit_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => wb_limit(31),
      R => p_0_in
    );
\wb_limit_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => wb_limit(3),
      R => p_0_in
    );
\wb_limit_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => wb_limit(4),
      R => p_0_in
    );
\wb_limit_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => wb_limit(5),
      R => p_0_in
    );
\wb_limit_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => wb_limit(6),
      R => p_0_in
    );
\wb_limit_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => wb_limit(7),
      R => p_0_in
    );
\wb_limit_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => wb_limit(8),
      R => p_0_in
    );
\wb_limit_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \wb_limit[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => wb_limit(9),
      R => p_0_in
    );
write_finish_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000FFFF80000000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s_axi_wready\,
      I3 => \^s_axi_awready\,
      I4 => s00_axi_aresetn,
      I5 => write_finish,
      O => write_finish_i_1_n_0
    );
write_finish_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => write_finish_i_1_n_0,
      Q => write_finish,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ps_pl_top_configuration_prem_mem_counter_axi_0_3_prem_mem_counter_axi_v1_0 is
  port (
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    limit_irq : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    pmu_trigin : in STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end ps_pl_top_configuration_prem_mem_counter_axi_0_3_prem_mem_counter_axi_v1_0;

architecture STRUCTURE of ps_pl_top_configuration_prem_mem_counter_axi_0_3_prem_mem_counter_axi_v1_0 is
begin
prem_mem_counter_axi_v1_0_S00_AXI_inst: entity work.ps_pl_top_configuration_prem_mem_counter_axi_0_3_prem_mem_counter_axi_v1_0_S00_AXI
     port map (
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      limit_irq => limit_irq,
      pmu_trigin => pmu_trigin,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(2 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(2 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ps_pl_top_configuration_prem_mem_counter_axi_0_3 is
  port (
    pmu_trigin : in STD_LOGIC;
    pmu_trigout : out STD_LOGIC;
    limit_irq : out STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of ps_pl_top_configuration_prem_mem_counter_axi_0_3 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of ps_pl_top_configuration_prem_mem_counter_axi_0_3 : entity is "ps_pl_top_configuration_prem_mem_counter_axi_0_0,prem_mem_counter_axi_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of ps_pl_top_configuration_prem_mem_counter_axi_0_3 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of ps_pl_top_configuration_prem_mem_counter_axi_0_3 : entity is "prem_mem_counter_axi_v1_0,Vivado 2018.1";
end ps_pl_top_configuration_prem_mem_counter_axi_0_3;

architecture STRUCTURE of ps_pl_top_configuration_prem_mem_counter_axi_0_3 is
  signal \<const0>\ : STD_LOGIC;
  signal \^pmu_trigin\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of limit_irq : signal is "xilinx.com:signal:interrupt:1.0 limit_irq INTERRUPT";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of limit_irq : signal is "XIL_INTERFACENAME limit_irq, SENSITIVITY LEVEL_HIGH, PortWidth 1";
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 99990000, PHASE 0.000, CLK_DOMAIN ps_pl_top_configuration_zynq_ultra_ps_e_0_0_pl_clk0";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 7, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 99990000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN ps_pl_top_configuration_zynq_ultra_ps_e_0_0_pl_clk0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  \^pmu_trigin\ <= pmu_trigin;
  pmu_trigout <= \^pmu_trigin\;
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.ps_pl_top_configuration_prem_mem_counter_axi_0_3_prem_mem_counter_axi_v1_0
     port map (
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      limit_irq => limit_irq,
      pmu_trigin => \^pmu_trigin\,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(4 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(4 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
