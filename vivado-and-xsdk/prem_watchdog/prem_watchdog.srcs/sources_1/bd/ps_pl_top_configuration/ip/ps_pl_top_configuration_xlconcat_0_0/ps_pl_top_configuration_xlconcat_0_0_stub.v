// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
// Date        : Sat May 19 16:27:23 2018
// Host        : matejjoe running 64-bit unknown
// Command     : write_verilog -force -mode synth_stub
//               /home/barysmax/src/xilinx/vivado-and-xsdk/prem_watchdog/prem_watchdog.srcs/sources_1/bd/ps_pl_top_configuration/ip/ps_pl_top_configuration_xlconcat_0_0/ps_pl_top_configuration_xlconcat_0_0_stub.v
// Design      : ps_pl_top_configuration_xlconcat_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xczu9eg-ffvb1156-2-i-es2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "xlconcat_v2_1_1_xlconcat,Vivado 2018.1" *)
module ps_pl_top_configuration_xlconcat_0_0(In0, In1, In2, In3, dout)
/* synthesis syn_black_box black_box_pad_pin="In0[0:0],In1[0:0],In2[0:0],In3[0:0],dout[3:0]" */;
  input [0:0]In0;
  input [0:0]In1;
  input [0:0]In2;
  input [0:0]In3;
  output [3:0]dout;
endmodule
