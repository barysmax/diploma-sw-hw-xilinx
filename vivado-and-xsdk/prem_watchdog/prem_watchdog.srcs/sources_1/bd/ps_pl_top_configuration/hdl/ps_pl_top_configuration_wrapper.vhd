--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
--Date        : Sun May 20 02:02:55 2018
--Host        : matejjoe running 64-bit Debian GNU/Linux 9.2 (stretch)
--Command     : generate_target ps_pl_top_configuration_wrapper.bd
--Design      : ps_pl_top_configuration_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ps_pl_top_configuration_wrapper is
  port (
    GPIO_LED1 : out STD_LOGIC
  );
end ps_pl_top_configuration_wrapper;

architecture STRUCTURE of ps_pl_top_configuration_wrapper is
  component ps_pl_top_configuration is
  port (
    GPIO_LED1 : out STD_LOGIC
  );
  end component ps_pl_top_configuration;
begin
ps_pl_top_configuration_i: component ps_pl_top_configuration
     port map (
      GPIO_LED1 => GPIO_LED1
    );
end STRUCTURE;
