vlib work
vlib riviera

vlib riviera/xilinx_vip
vlib riviera/xil_defaultlib
vlib riviera/xpm
vlib riviera/axi_infrastructure_v1_1_0
vlib riviera/smartconnect_v1_0
vlib riviera/axi_protocol_checker_v2_0_2
vlib riviera/axi_vip_v1_1_2
vlib riviera/zynq_ultra_ps_e_vip_v1_0_2
vlib riviera/lib_cdc_v1_0_2
vlib riviera/proc_sys_reset_v5_0_12
vlib riviera/generic_baseblocks_v2_1_0
vlib riviera/fifo_generator_v13_2_2
vlib riviera/axi_data_fifo_v2_1_15
vlib riviera/axi_register_slice_v2_1_16
vlib riviera/axi_protocol_converter_v2_1_16
vlib riviera/axi_clock_converter_v2_1_15
vlib riviera/blk_mem_gen_v8_4_1
vlib riviera/axi_dwidth_converter_v2_1_16

vmap xilinx_vip riviera/xilinx_vip
vmap xil_defaultlib riviera/xil_defaultlib
vmap xpm riviera/xpm
vmap axi_infrastructure_v1_1_0 riviera/axi_infrastructure_v1_1_0
vmap smartconnect_v1_0 riviera/smartconnect_v1_0
vmap axi_protocol_checker_v2_0_2 riviera/axi_protocol_checker_v2_0_2
vmap axi_vip_v1_1_2 riviera/axi_vip_v1_1_2
vmap zynq_ultra_ps_e_vip_v1_0_2 riviera/zynq_ultra_ps_e_vip_v1_0_2
vmap lib_cdc_v1_0_2 riviera/lib_cdc_v1_0_2
vmap proc_sys_reset_v5_0_12 riviera/proc_sys_reset_v5_0_12
vmap generic_baseblocks_v2_1_0 riviera/generic_baseblocks_v2_1_0
vmap fifo_generator_v13_2_2 riviera/fifo_generator_v13_2_2
vmap axi_data_fifo_v2_1_15 riviera/axi_data_fifo_v2_1_15
vmap axi_register_slice_v2_1_16 riviera/axi_register_slice_v2_1_16
vmap axi_protocol_converter_v2_1_16 riviera/axi_protocol_converter_v2_1_16
vmap axi_clock_converter_v2_1_15 riviera/axi_clock_converter_v2_1_15
vmap blk_mem_gen_v8_4_1 riviera/blk_mem_gen_v8_4_1
vmap axi_dwidth_converter_v2_1_16 riviera/axi_dwidth_converter_v2_1_16

vlog -work xilinx_vip  -sv2k12 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/home/barysmax/Xilinx/Vivado/2018.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/home/barysmax/Xilinx/Vivado/2018.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work axi_infrastructure_v1_1_0  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work smartconnect_v1_0  -sv2k12 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/sc_util_v1_0_vl_rfs.sv" \

vlog -work axi_protocol_checker_v2_0_2  -sv2k12 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/3755/hdl/axi_protocol_checker_v2_0_vl_rfs.sv" \

vlog -work axi_vip_v1_1_2  -sv2k12 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/725c/hdl/axi_vip_v1_1_vl_rfs.sv" \

vlog -work zynq_ultra_ps_e_vip_v1_0_2  -sv2k12 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl/zynq_ultra_ps_e_vip_v1_0_vl_rfs.sv" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_zynq_ultra_ps_e_0_0/sim/design_1_zynq_ultra_ps_e_0_0_vip_wrapper.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/sim/design_1.vhd" \

vcom -work lib_cdc_v1_0_2 -93 \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \

vcom -work proc_sys_reset_v5_0_12 -93 \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/f86a/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_rst_ps8_0_99M_0/sim/design_1_rst_ps8_0_99M_0.vhd" \
"../../../bd/design_1/ipshared/305b/hdl/pmuevents_counter_wreg_v1_0_S00_AXI.vhd" \
"../../../bd/design_1/ipshared/305b/hdl/pmuevents_counter_wreg_v1_0.vhd" \
"../../../bd/design_1/ip/design_1_pmuevents_counter_wr_0_1/sim/design_1_pmuevents_counter_wr_0_1.vhd" \

vlog -work generic_baseblocks_v2_1_0  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work fifo_generator_v13_2_2  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/7aff/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_2_2 -93 \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/7aff/hdl/fifo_generator_v13_2_rfs.vhd" \

vlog -work fifo_generator_v13_2_2  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/7aff/hdl/fifo_generator_v13_2_rfs.v" \

vlog -work axi_data_fifo_v2_1_15  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/d114/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_16  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/0cde/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work axi_protocol_converter_v2_1_16  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/1229/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work axi_clock_converter_v2_1_15  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/d371/hdl/axi_clock_converter_v2_1_vl_rfs.v" \

vlog -work blk_mem_gen_v8_4_1  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/67d8/simulation/blk_mem_gen_v8_4.v" \

vlog -work axi_dwidth_converter_v2_1_16  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/2c2b/hdl/axi_dwidth_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/02c8/hdl/verilog" "+incdir+../../../../waypoints_detection.srcs/sources_1/bd/design_1/ipshared/ad7b/hdl" "+incdir+/home/barysmax/Xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_auto_ds_0/sim/design_1_auto_ds_0.v" \
"../../../bd/design_1/ip/design_1_auto_pc_0/sim/design_1_auto_pc_0.v" \

vlog -work xil_defaultlib \
"glbl.v"

