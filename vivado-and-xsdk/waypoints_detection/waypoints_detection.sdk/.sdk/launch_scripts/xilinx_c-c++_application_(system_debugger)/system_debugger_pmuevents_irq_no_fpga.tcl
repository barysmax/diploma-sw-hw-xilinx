connect -url tcp:127.0.0.1:3121
source /home/barysmax/Xilinx/SDK/2018.1/scripts/sdk/util/zynqmp_utils.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent JTAG-SMT2NC 210308A1CE1F"} -index 1
reset_apu
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent JTAG-SMT2NC 210308A1CE1F"} -index 1
loadhw -hw /home/barysmax/src/xilinx/other/waypoints_detection/waypoints_detection.sdk/design_1_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x80000000 0xbfffffff} {0x400000000 0x5ffffffff} {0x1000000000 0x7fffffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent JTAG-SMT2NC 210308A1CE1F"} -index 1
source /home/barysmax/src/xilinx/other/waypoints_detection/waypoints_detection.sdk/design_1_wrapper_hw_platform_0/psu_init.tcl
psu_init
catch {psu_protection}
targets -set -nocase -filter {name =~"*A53*0" && jtag_cable_name =~ "Digilent JTAG-SMT2NC 210308A1CE1F"} -index 1
rst -processor
dow /home/barysmax/src/xilinx/other/waypoints_detection/waypoints_detection.sdk/waypoints_tries/Debug/pmuevents_as_irq_try.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~"*A53*0" && jtag_cable_name =~ "Digilent JTAG-SMT2NC 210308A1CE1F"} -index 1
con
