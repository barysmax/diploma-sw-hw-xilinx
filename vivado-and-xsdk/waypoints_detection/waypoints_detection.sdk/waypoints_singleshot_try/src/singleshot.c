/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "platform.h"
#include "xil_printf.h"

// Coresight libs
#include "csaccess.h"

static void prefetch()
{
	printf("%s\n\r", __func__);
	sleep(5);
}

static void compute()
{
	printf("%s\n\r", __func__);
	sleep(5);
}

static void writeback()
{
	printf("%s\n\r", __func__);
	sleep(5);
}

void etm_diag(cs_device_t etm)
{
	cs_etmv4_config_t conf;
	cs_etm_config_init_ex(etm, &conf);
	conf.flags = CS_ETMC_ALL;
	cs_etm_config_get_ex(etm, &conf);
	cs_etm_config_print_ex(etm, &conf);
}

int do_init_etm(cs_device_t dev)
{
    int rc;
    int etm_version = cs_etm_get_version(dev);

    printf("CSDEMO: Initialising ETM/PTM\n %d", etm_version);

    /* ASSERT that this is an etm etc */
    assert(cs_device_has_class(dev, CS_DEVCLASS_SOURCE));

    /* set to a 'clean' state - clears events & values, retains ctrl and ID, ensure programmable */
    if ((rc = cs_etm_clean(dev)) != 0) {
        printf("CSDEMO: Failed to set ETM/PTM into clean state\n");
        return rc;
    }


    /* ETMv4 initialisation */
    cs_etmv4_config_t etm_cf;

    cs_etm_config_init_ex(dev, &etm_cf);

    etm_cf.flags |= CS_ETMC_ADDR_COMP;
    /* configure address comparator (ac) 0 to check for addr &prefetch */
    etm_cf.addr_comps[0].acvr_l = (size_t) prefetch & 0x0FFFFFFFF;
    etm_cf.addr_comps[0].acvr_h = ((size_t) prefetch >> 32) & 0x0FFFFFFFF;
    etm_cf.addr_comps[0].acatr_l = 0;
    /* configure ac 1 to check for addr &compute */
    etm_cf.addr_comps[1].acvr_l = (size_t) compute & 0x0FFFFFFFF;
    etm_cf.addr_comps[1].acvr_h = ((size_t) compute >> 32) & 0x0FFFFFFFF;
    etm_cf.addr_comps[1].acatr_l = 0;
    /* configure ac 2 to check for addr &writeback */
    etm_cf.addr_comps[2].acvr_l = (size_t) writeback & 0x0FFFFFFFF;
    etm_cf.addr_comps[2].acvr_h = ((size_t) writeback >> 32) & 0x0FFFFFFFF;
    etm_cf.addr_comps[2].acatr_l = 0;
    /* allow ac 0-2 */
    etm_cf.addr_comps_acc_mask = 0x7;

    /* configure single-shot comparator (ssc) 0 to fire when non-speculative match on ac0 occurs*/
    etm_cf.flags |= CS_ETMC_SSHOT_CTRL;
    etm_cf.ss_comps[0].ssccr |= (1 << 24); /* reset after every fire */
    etm_cf.ss_comps[0].ssccr |= (1 << 0); /* select acc0 */
    etm_cf.ss_comps_acc_mask = 1;

    etm_cf.flags |= CS_ETMC_RES_SEL;
    /* configure resource selector (rs) 2 for selecting ssc0 */
    /*21:20 = PAIRINV:INV = b0, GROUP = 0b0011, SELECT = b1*/
    etm_cf.rsctlr[2] |= (0b000 << 20) | (0b0011  << 16)  | (1 << 0);
    /* configure rs 4 for selecting ac 1 */
    /*21:20 = PAIRINV:INV = b0, GROUP = b0100, SELECT = b10*/
    etm_cf.rsctlr[4] = 0x00040002;
    /* configure rs 6 for selecting ac 2 */
    /*21:20 = PAIRINV:INV = b0, GROUP = b0100, SELECT = b100*/
    etm_cf.rsctlr[6] = 0x00040004;
    /* allow rs 2,4,6*/
    etm_cf.rsctlr_acc_mask = 0x00000054;

    etm_cf.flags |= CS_ETMC_EVENTSELECT;
    /* configure external output (eo) 0,1,2 to fire when rs2, rs4, rs6 signals */
    etm_cf.eventctlr0r = 0x00060402;
    /* ATB trigger enable */
    etm_cf.eventctlr1r |= 0x007;

    cs_etm_config_put_ex(dev, &etm_cf);

    cs_etm_disable_programming(dev); /* This brings an ETM to the action! */
    return 0;
}

void do_init_cti_a53(cs_device_t dev)
{

	cs_cti_reset(dev);
	cs_cti_set_trigin_channels(dev, 4, 1 << 0);
	cs_cti_set_trigin_channels(dev, 5, 1 << 1);
	// cs_cti_set_trigout_channels(dev, 0, 1 << 1); /*halt on compute*/
	cs_cti_set_trigin_channels(dev, 6, 1 << 2);
	cs_cti_set_global_channels(dev, 0b0111);
	//cs_cti_set_trigout_channels(dev, 2, 1 << 0);
	cs_cti_enable(dev);
}

void do_init_cti_soc(cs_device_t dev)
{

	cs_cti_reset(dev);
	cs_cti_set_global_channels(dev, 0b0111);
	cs_cti_set_trigout_channels(dev, 0, 1 << 0);
	cs_cti_set_trigout_channels(dev, 1, 1 << 1);
	cs_cti_set_trigout_channels(dev, 2, 1 << 2);

	// cs_cti_set_active_channel(dev, 1);
	// cs_cti_set_active_channel(dev, 2);
	cs_cti_enable(dev);
}

int configure_coresight(cs_device_t etm0, cs_device_t cti_a53_0, cs_device_t cti_soc1)
{

    int err = do_init_etm(etm0);
     if (err) {
     	printf("etm configuration failed %d\n\r", err);
    }
    do_init_cti_a53(cti_a53_0);
    do_init_cti_soc(cti_soc1);

    return 0;
}

int main()
{
    init_platform();
    cs_init();

	printf("=================Configure===================\n\r\n\r");

    // ftm outs is at cti_soc1
    // https://www.xilinx.com/html_docs/xilinx2017_4/SDK_Doc/SDK_references/reference_cross-trigerring-zynqmp.html

    cs_device_t etm0 = cs_device_register(0xFEC40000);
    cs_device_t cti_soc1 = cs_device_register(0xFE9A0000);
    cs_device_t cti_a53_0 = cs_device_register(0xFEC20000);

    configure_coresight(etm0, cti_a53_0, cti_soc1);

    cs_cti_diag();
    //sleep(5);
    printf("=================Start!=======================\n\r\n\r");

    prefetch();
    cs_cti_diag();

    compute();
    cs_cti_diag();

    writeback();
	cs_cti_diag();

    prefetch();
    compute();
    writeback();

	etm_diag(etm0);

	cs_shutdown();
	cleanup_platform();
    return 0;
}
