/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "csaccess.h"

#define CTIINTACK 0x00000010
#define CHANNEL 0

int main()
{
    init_platform();
    cs_init();

    print("\n\rLet the triggers be with you.\n\r");
    cs_device_t cti_a53_0 = cs_device_register(0xFEC20000);
    cs_device_t cti_soc1 = cs_device_register(0xFE9A0000);

    printf("Clear all active channels\n\r");
    cs_cti_clear_all_active_channels(cti_a53_0);
    cs_cti_clear_all_active_channels(cti_soc1);
    sleep(2);

    printf("Reset CTIs\n\r");
    cs_cti_reset(cti_a53_0);
    cs_cti_reset(cti_soc1);
    sleep(2);

    printf("Set up ch 0 -> trigout 0,  trigin 0 -> ch 3 at a53. Enable CTI.\n\r");
    cs_cti_set_global_channels(cti_a53_0, 0b1000);
    cs_cti_set_trigout_channels(cti_a53_0, 0, 1 << CHANNEL);
    cs_cti_set_trigin_channels(cti_a53_0, 0, 1 << 3);

    cs_cti_set_global_channels(cti_soc1, 0b1000);
    cs_cti_set_trigout_channels(cti_soc1, 3, 1U << 3);

    cs_cti_enable(cti_a53_0);
    cs_cti_enable(cti_soc1);
    sleep(2);

    printf("Set channel active! Must start debug state..");
    cs_cti_pulse_channel(cti_a53_0, CHANNEL);
    sleep(5);

    printf("...Clear channel\n\r");
    cs_cti_clear_active_channel(cti_a53_0, 0);

    printf("ACK the IRQ now.\n\r");
    cs_device_write_only(cti_a53_0, CTIINTACK, 1 << CHANNEL);
    cs_cti_diag();

    cleanup_platform();
    return 0;
}
