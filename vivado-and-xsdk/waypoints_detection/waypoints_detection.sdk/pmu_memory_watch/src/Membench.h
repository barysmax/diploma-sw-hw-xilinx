/*
 * Membench.h
 *
 *  Created on: Apr 25, 2018
 *      Author: barysmax
 */

#ifndef SRC_MEMBENCH_H_
#define SRC_MEMBENCH_H_

#include <stddef.h>
#include <stdbool.h>

typedef struct
{
	size_t count;
	size_t bits;
}cpu_set_t;

struct cfg {
	bool sequential;
	unsigned size;
	unsigned num_threads;
	unsigned read_count;
	cpu_set_t cpu_set;
	bool write;
	unsigned ofs;
	bool use_cycles; /* instead of ns */
	unsigned repeats;
};

void run_benchmark(struct cfg *cfg);

#endif /* SRC_MEMBENCH_H_ */
