/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "platform.h"
#include "xil_printf.h"
#include "xscugic.h"

// Coresight libs
#include "csaccess.h"
#include "csregisters.h"

//#include "Membench.h"

// PMU defines
#define PMU0_ADDR 0xFEC30000
#define PMU_L2D_CACHE_EVENT 0x16
#define PMU_L2D_CACHE_REFILL_EVENT 0x17
#define PMU_COUNTER 0
#define PMU_IRQ_GIC 121

static cs_device_t pmu0;

#define ARR_SIZE 0x100000
static size_t arr[ARR_SIZE];

XScuGic Intc; 	     /* Instance of the Interrupt Controller */
static XScuGic_Config *GicConfig;    /* The configuration parameters of the
                                       controller */
volatile int INT_WAIT = 1;

void pmuirq_ack_handler()
{
	cs_pmu_clear_overflow(pmu0, 1U << PMU_COUNTER);
	INT_WAIT = 0;
}


int pmuirq_ack_configure()
{

	/*
	 * Initialize the interrupt controller driver so that it is ready to
	 * use.
	 */
	GicConfig = XScuGic_LookupConfig(XPAR_SCUGIC_0_DEVICE_ID);
	assert(!XScuGic_CfgInitialize(&Intc, GicConfig,
					GicConfig->CpuBaseAddress));
	/*
	 * Setup the Interrupt System
	 * Connect the interrupt controller interrupt handler to the hardware
	 * interrupt handling logic in the ARM processor.
	 */
	// set edge triggered
	XScuGic_SetPriorityTriggerType(&Intc, PMU_IRQ_GIC, 0xA0, 0x3);
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
			&Intc);
	/*
	 * Enable interrupts in the ARM
	 */
	Xil_ExceptionEnable();

	/*
	 * Connect a device driver handler that will be called when an
	 * interrupt for the device occurs, the device driver handler performs
	 * the specific interrupt processing for the device
	 */
	XScuGic_Connect(&Intc, PMU_IRQ_GIC,
			   (Xil_ExceptionHandler) pmuirq_ack_handler,
			   (void *)&Intc);


	/*
	 * Enable the interrupt for the device and then cause (simulate) an
	 * interrupt so the handlers will be called
	 */
	XScuGic_Enable(&Intc, PMU_IRQ_GIC);
}

void do_mem_accesses()
{
	//struct cfg bench = {
	//		.sequential = false,
	//		.num_threads = 1,
	//		.size = wss,
	//		.read_count = acc_num,
	//		.use_cycles = false,
	//		.repeats = 1
	//};
	//run_benchmark(&bench);

	int i = ARR_SIZE - 1;
	int volatile x;
	while (i--){
		x = arr[i];
	}
}

void print_pmu_stat(cs_device_t pmu)
{
	cs_pmu_t pmu_s;
	pmu_s.mask = 0xffffffff;
	cs_pmu_read_status(pmu, 0xFF ^ CS_PMU_ENABLE ^ CS_PMU_DISABLE, &pmu_s);
	cs_pmu_mask_t mask = pmu_s.mask;
	printf("Event mask: 0x%0x\n\r", mask);
	for (int i = 0; i < 31 && mask != 0; ++i) {
		if (mask & 1) {
			printf("Event %d: 0x%x, Counts = %x\n\r", i, pmu_s.eventtypes[i], pmu_s.counts[i]);
		}
		mask >>= 1;
	}
	printf("div64: %u\n\r", pmu_s.div64);
	printf("Cycles: 0x%x\n\r", pmu_s.cycles);
	printf("Set for overflow: 0x%x\n\r", pmu_s.overflow);
	printf("PMU enabled: %d\n\r", cs_pmu_is_enabled(pmu));
}

int main()
{
    init_platform();
    cs_init();
    pmuirq_ack_configure();

	printf("\n\r=================Configure===================\n\r");
	pmu0 = cs_device_register(PMU0_ADDR);
	cs_pmu_t pmu_s;
	cs_pmu_read_status(pmu0, 0xFF ^ CS_PMU_ENABLE ^ CS_PMU_DISABLE, &pmu_s);

	printf("Reset the pmu0...\n\r");
	cs_pmu_reset(pmu0, CS_PMU_CYCLES | CS_PMU_OVERFLOW | CS_PMU_COUNTS| CS_PMU_DISABLE);
	print_pmu_stat(pmu0);

	/* enable the counter (c) 0*/
	pmu_s.mask = (1U << PMU_COUNTER );
	/* limit the c0 at 10000 events*/
	pmu_s.counts[PMU_COUNTER] = 0xFFFFFFFF - 10000;
	/* choose event type for c0*/
	pmu_s.eventtypes[PMU_COUNTER] = PMU_L2D_CACHE_REFILL_EVENT;

	cs_device_write(pmu0, CS_PMINTENSET,  1U << PMU_COUNTER);
	cs_pmu_write_status(pmu0,
			CS_PMU_CYCLES | CS_PMU_COUNTS | CS_PMU_EVENTTYPES | CS_PMU_ENABLE, &pmu_s);

    printf("\n\r=================Start!=======================\n\r");

	do_mem_accesses();

	print_pmu_stat(pmu0);
	printf("Done exactly %d read accesses.\n\r", ARR_SIZE);


	cs_shutdown();
	while(INT_WAIT) {sleep(1);};
	printf("Interrupt ack received.\n\r");
	print_pmu_stat(pmu0);
	fflush(0);
	cleanup_platform();
    return 0;
}
