/*
 * pl_counter.c
 *
 *  Created on: May 7, 2018
 *      Author: barysmax
 */

#include <assert.h>

#include "pl_counter.h"
#include "csaccess.h"
#include "coresight-addrs.h"
#include "xil_io.h"

#define CTI_A53_TRIGIN_EXT0 4

#define ETM_EXTIN0 0
#define ETM_COUNTER0 0
#define ETM_RESSEL_FOR_EXTIN 2
#define ETM_RESSEL_EXTIN_GROUP 0b0000
#define ETM_RESSEL_FOR_COUNTER 4
#define ETM_RESSEL_COUNTER_GROUP 0b0010

void pl_count_init()
{
    cs_device_t cti_soc1 = cs_device_register(cti_soc1_addr);
	// Set up CTI that triggers PL (through FTM)
	cs_cti_reset(cti_soc1);
	cs_cti_set_global_channels(cti_soc1, 0);
}

void pl_count_percpu_init(uint32_t cpu_id)
{
    cs_device_t cti_soc1 = cs_device_register(cti_soc1_addr);
	cs_device_t cti_cpu = cs_device_register(cti_a53_addr[cpu_id]);
    cs_device_t etm = cs_device_register(etm_addr[cpu_id]);
    cs_device_t pmu = cs_device_register(pmu_addr[cpu_id]);

    // Set up cpu's CTI
	cs_cti_reset(cti_cpu);
	cs_cti_set_trigin_channels(cti_cpu, CTI_A53_TRIGIN_EXT0, 1 << cpu_id);
	cs_cti_set_global_channels(cti_cpu, 1 << cpu_id);

	// Set up that cpu's ETM
	assert(cs_etm_clean(etm) != 0);
	// Enable event exporting at that cpu's PMU
	cs_etmv4_config_t etm_cf;
    cs_etm_config_init_ex(etm, &etm_cf);

    /* Select external event input 0 to detect PMUBUS_EVENT */;
    etm_cf.flags |= CS_ETMC_RES_SEL;
    etm_cf.extinselr = 0x00000000 | PMUBUS_EVENT;

    /* configure resource selector (rs) 2 for selecting extinput 0 */
    etm_cf.rsctlr[2] =  (ETM_RESSEL_EXTIN_GROUP << 16) | 1U << ETM_EXTIN0;
    /* configure resource selector (rs) 4 for selecting counter 0 */
    /*21:20 = PAIRINV:INV = b0, GROUP = 0b0010, SELECT = b1*/
    etm_cf.rsctlr[4] = (ETM_RESSEL_COUNTER_GROUP << 16) | 1U << ETM_COUNTER0;
    etm_cf.rsctlr_acc_mask = (1 << ETM_RESSEL_FOR_EXTIN) | (1 << ETM_RESSEL_FOR_COUNTER);


    etm_cf.flags |= CS_ETMC_COUNTER;
    etm_cf.counter[0].cntctlr = 1U << 16U /*self reload mode*/ | ETM_RESSEL_EXTIN_GROUP;
    etm_cf.counter[0].cntrldvr = 3U; /* 4 events*/
    etm_cf.counter[0].cntvr = 3U; /* 4 events*/
    etm_cf.counter_acc_mask = 1;


    etm_cf.flags |= CS_ETMC_EVENTSELECT;
    /* configure external output (eo) 0 to fire when rs4 signals */
    etm_cf.eventctlr0r = 0x00000004;
    etm_cf.eventctlr1r |= 0x001;

    cs_etm_config_put_ex(dev, &etm_cf);

	// Open a channel for that CPU in SoC's CTI

}

void pl_count_reset(uint32_t cpu)
{
    Xil_Out32(pl_count_addr[cpu], 0);
}

uint32_t pl_count_read(uint32_t cpu)
{
    return Xil_In32(pl_count_addr[cpu]);
}


