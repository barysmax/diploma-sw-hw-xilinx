/*
 * pl_counter.c
 *
 *  Created on: May 7, 2018
 *      Author: barysmax
 */

#include <assert.h>

#include "xil_io.h"
#include "csaccess.h"
#include "csregisters.h"

#include "pl_counter.h"
#include "coresight-addrs.h"


#define CTI_A53_TRIGIN_EXT0 4U

#define ETM_EXTIN 0U

#define ETM_COUNTER 0U
#define ETM_COUNTER_SELF_RELOAD_MASK (1U << 16U)
#define ETM_EVENTS_BUFF_NUM 4U

#define ETM_RESSEL_EXTIN 2U
#define ETM_RESSEL_EXTIN_GROUP_MASK (0b0000 << 16U)

#define ETM_RESSEL_COUNTER 4U
#define ETM_RESSEL_COUNTER_GROUP_MASK (0b0010 << 16U)

void pl_count_init()
{
    cs_device_t cti_soc1 = cs_device_register(cti_soc1_addr);
	// Set up CTI that triggers PL (through FTM)
	cs_cti_reset(cti_soc1);
	cs_cti_set_global_channels(cti_soc1, 0);
	cs_cti_enable(cti_soc1);
}

static void setup_cti_a53_percpu(uint32_t cpu_id)
{
	cs_device_t cti_cpu = cs_device_register(cti_a53_addr[cpu_id]);
	cs_cti_reset(cti_cpu);
	cs_cti_set_trigin_channels(cti_cpu, CTI_A53_TRIGIN_EXT0, 1 << cpu_id);
	cs_cti_set_global_channels(cti_cpu, 1 << cpu_id);
	cs_cti_enable(cti_a53);
}

static void setup_etm_percpu(uint32_t cpu_id)
{
	assert(cs_etm_clean(etm) != 0);
	// Enable event exporting at that cpu's PMU
	cs_etmv4_config_t etm_cf;
    cs_etm_config_init_ex(etm, &etm_cf);

    /* Select external event input 0 to detect PMUBUS_EVENT */;
    etm_cf.flags |= CS_ETMC_RES_SEL;
    etm_cf.extinselr = 0x00000000 | PMUBUS_EVENT;

    /* configure resource selector (rs) 2 for selecting extinput 0 */
    etm_cf.rsctlr[2] =  ETM_RESSEL_EXTIN_GROUP_MASK | 1U << ETM_EXTIN;
    /* configure resource selector (rs) 4 for selecting counter 0 */
    /*21:20 = PAIRINV:INV = b0, GROUP = 0b0010, SELECT = b1*/
    etm_cf.rsctlr[4] = ETM_RESSEL_COUNTER_GROUP_MASK| 1U << ETM_COUNTER;
    etm_cf.rsctlr_acc_mask = (1 << ETM_RESSEL_EXTIN) | (1 << ETM_RESSEL_COUNTER);


    etm_cf.flags |= CS_ETMC_COUNTER;
    etm_cf.counter[0].cntctlr = ETM_COUNTER_SELF_RELOAD_MASK | ETM_RESSEL_EXTIN;
    etm_cf.counter[0].cntrldvr = ETM_EVENTS_BUFF_NUM - 1;
    etm_cf.counter[0].cntvr = ETM_EVENTS_BUFF_NUM - 1;
    etm_cf.counter_acc_mask = 1 << ETM_COUNTER;


    etm_cf.flags |= CS_ETMC_EVENTSELECT;
    /* configure external output (eo) 0 to fire when rs4 signals */
    etm_cf.eventctlr0r = ETM_RESSEL_COUNTER;
    etm_cf.eventctlr1r |= 1U << cpu_id;

    cs_etm_config_put_ex(etm, &etm_cf);

	// Open a channel for that CPU in SoC's CTI
    uint32_t glob_chan_mask = cs_device_read(cti_soc1, CS_CTIGATE);
    cs_cti_set_global_channels(cti_soc1, glob_chan_mask | (1U << cpu_id));

    cs_etm_disable_programming(etm); /* This brings an ETM to the action! */


}

void pl_count_percpu_init(uint32_t cpu_id)
{
    cs_device_t cti_soc1 = cs_device_register(cti_soc1_addr);
    cs_device_t etm = cs_device_register(etm_addr[cpu_id]);
    cs_device_t pmu = cs_device_register(pmu_addr[cpu_id]);

    setup_cti_a53_percpu(cpu_id);

}

void pl_count_reset(uint32_t cpu)
{
    Xil_Out32(pl_count_addr[cpu], 0);
}

uint32_t pl_count_read(uint32_t cpu)
{
    return Xil_In32(pl_count_addr[cpu]);
}


