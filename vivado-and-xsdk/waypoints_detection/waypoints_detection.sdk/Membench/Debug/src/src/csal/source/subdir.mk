################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/src/csal/source/cs_access_cmnfns.c \
../src/src/csal/source/cs_cti_ect.c \
../src/src/csal/source/cs_debug_halt.c \
../src/src/csal/source/cs_debug_sample.c \
../src/src/csal/source/cs_demo_known_boards.c \
../src/src/csal/source/cs_etm.c \
../src/src/csal/source/cs_etm_v4.c \
../src/src/csal/source/cs_init_manage.c \
../src/src/csal/source/cs_pmu.c \
../src/src/csal/source/cs_reg_access.c \
../src/src/csal/source/cs_sw_stim.c \
../src/src/csal/source/cs_topology.c \
../src/src/csal/source/cs_trace_metadata.c \
../src/src/csal/source/cs_trace_sink.c \
../src/src/csal/source/cs_trace_source.c \
../src/src/csal/source/cs_ts_gen.c \
../src/src/csal/source/cs_util_create_snapshot.c \
../src/src/csal/source/csregistration.c 

OBJS += \
./src/src/csal/source/cs_access_cmnfns.o \
./src/src/csal/source/cs_cti_ect.o \
./src/src/csal/source/cs_debug_halt.o \
./src/src/csal/source/cs_debug_sample.o \
./src/src/csal/source/cs_demo_known_boards.o \
./src/src/csal/source/cs_etm.o \
./src/src/csal/source/cs_etm_v4.o \
./src/src/csal/source/cs_init_manage.o \
./src/src/csal/source/cs_pmu.o \
./src/src/csal/source/cs_reg_access.o \
./src/src/csal/source/cs_sw_stim.o \
./src/src/csal/source/cs_topology.o \
./src/src/csal/source/cs_trace_metadata.o \
./src/src/csal/source/cs_trace_sink.o \
./src/src/csal/source/cs_trace_source.o \
./src/src/csal/source/cs_ts_gen.o \
./src/src/csal/source/cs_util_create_snapshot.o \
./src/src/csal/source/csregistration.o 

C_DEPS += \
./src/src/csal/source/cs_access_cmnfns.d \
./src/src/csal/source/cs_cti_ect.d \
./src/src/csal/source/cs_debug_halt.d \
./src/src/csal/source/cs_debug_sample.d \
./src/src/csal/source/cs_demo_known_boards.d \
./src/src/csal/source/cs_etm.d \
./src/src/csal/source/cs_etm_v4.d \
./src/src/csal/source/cs_init_manage.d \
./src/src/csal/source/cs_pmu.d \
./src/src/csal/source/cs_reg_access.d \
./src/src/csal/source/cs_sw_stim.d \
./src/src/csal/source/cs_topology.d \
./src/src/csal/source/cs_trace_metadata.d \
./src/src/csal/source/cs_trace_sink.d \
./src/src/csal/source/cs_trace_source.d \
./src/src/csal/source/cs_ts_gen.d \
./src/src/csal/source/cs_util_create_snapshot.d \
./src/src/csal/source/csregistration.d 


# Each subdirectory must supply rules for building sources it contributes
src/src/csal/source/%.o: ../src/src/csal/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v8 gcc compiler'
	aarch64-none-elf-gcc -DBAREMETAL=1 -DMEMBENCH_LIBUSE -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -I../../standalone_bsp_0/psu_cortexa53_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


