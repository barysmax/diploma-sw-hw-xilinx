################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/csal/source/cs_access_cmnfns.c \
../src/csal/source/cs_cti_ect.c \
../src/csal/source/cs_debug_halt.c \
../src/csal/source/cs_debug_sample.c \
../src/csal/source/cs_demo_known_boards.c \
../src/csal/source/cs_etm.c \
../src/csal/source/cs_etm_v4.c \
../src/csal/source/cs_init_manage.c \
../src/csal/source/cs_pmu.c \
../src/csal/source/cs_reg_access.c \
../src/csal/source/cs_sw_stim.c \
../src/csal/source/cs_topology.c \
../src/csal/source/cs_trace_metadata.c \
../src/csal/source/cs_trace_sink.c \
../src/csal/source/cs_trace_source.c \
../src/csal/source/cs_ts_gen.c \
../src/csal/source/cs_util_create_snapshot.c \
../src/csal/source/csregistration.c 

OBJS += \
./src/csal/source/cs_access_cmnfns.o \
./src/csal/source/cs_cti_ect.o \
./src/csal/source/cs_debug_halt.o \
./src/csal/source/cs_debug_sample.o \
./src/csal/source/cs_demo_known_boards.o \
./src/csal/source/cs_etm.o \
./src/csal/source/cs_etm_v4.o \
./src/csal/source/cs_init_manage.o \
./src/csal/source/cs_pmu.o \
./src/csal/source/cs_reg_access.o \
./src/csal/source/cs_sw_stim.o \
./src/csal/source/cs_topology.o \
./src/csal/source/cs_trace_metadata.o \
./src/csal/source/cs_trace_sink.o \
./src/csal/source/cs_trace_source.o \
./src/csal/source/cs_ts_gen.o \
./src/csal/source/cs_util_create_snapshot.o \
./src/csal/source/csregistration.o 

C_DEPS += \
./src/csal/source/cs_access_cmnfns.d \
./src/csal/source/cs_cti_ect.d \
./src/csal/source/cs_debug_halt.d \
./src/csal/source/cs_debug_sample.d \
./src/csal/source/cs_demo_known_boards.d \
./src/csal/source/cs_etm.d \
./src/csal/source/cs_etm_v4.d \
./src/csal/source/cs_init_manage.d \
./src/csal/source/cs_pmu.d \
./src/csal/source/cs_reg_access.d \
./src/csal/source/cs_sw_stim.d \
./src/csal/source/cs_topology.d \
./src/csal/source/cs_trace_metadata.d \
./src/csal/source/cs_trace_sink.d \
./src/csal/source/cs_trace_source.d \
./src/csal/source/cs_ts_gen.d \
./src/csal/source/cs_util_create_snapshot.d \
./src/csal/source/csregistration.d 


# Each subdirectory must supply rules for building sources it contributes
src/csal/source/%.o: ../src/csal/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v8 gcc compiler'
	aarch64-none-elf-gcc -DBAREMETAL=1 -DMEMBENCH_LIBUSE -DNO_DIAG=1 -DNO_CHECK=1 -Wall -O0 -g3 -I../../standalone_bsp_0/psu_cortexa53_0/include -I"/home/barysmax/src/xilinx/other/waypoints_detection/waypoints_detection.sdk/Membench/src/csal/include" -I"/home/barysmax/src/xilinx/other/waypoints_detection/waypoints_detection.sdk/Membench/src/csal/source" -c -fmessage-length=0 -MT"$@" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


