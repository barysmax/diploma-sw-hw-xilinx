################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/Membench.c \
../src/main.c \
../src/pl_counter.c \
../src/pmu-cache.c 

OBJS += \
./src/Membench.o \
./src/main.o \
./src/pl_counter.o \
./src/pmu-cache.o 

C_DEPS += \
./src/Membench.d \
./src/main.d \
./src/pl_counter.d \
./src/pmu-cache.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v8 gcc compiler'
	aarch64-none-elf-gcc -DMEMBENCH_LIBUSE -DBAREMETAL=1 -Wall -O0 -g3 -I../../standalone_bsp_0/psu_cortexa53_0/include -I"/home/barysmax/src/xilinx/other/CSAL/include" -c -fmessage-length=0 -MT"$@" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


