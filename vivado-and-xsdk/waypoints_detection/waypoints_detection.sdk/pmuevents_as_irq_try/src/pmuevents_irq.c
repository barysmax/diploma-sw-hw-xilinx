/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "xil_printf.h"
#include "xil_io.h"

// Coresight libs
#include "csaccess.h"
#include "csregisters.h"

// FTM defines
#define FTM_GPI 0x10

// PMU
#define PMUBUS_EVENT (21U + 4U)

#define ARR_SIZE 8*0x100000
static size_t arr[ARR_SIZE];

static void do_mem_accesses()
{
	int i = ARR_SIZE - 1;
	int volatile x;
	while (i--){
		x = arr[i];
		usleep(1);
	}
}

void print_pmu_stat(cs_device_t pmu)
{
	cs_pmu_t pmu_s;
	pmu_s.mask = 0xffffffff;
	cs_pmu_read_status(pmu, 0xFF ^ CS_PMU_ENABLE ^ CS_PMU_DISABLE, &pmu_s);
	cs_pmu_mask_t mask = pmu_s.mask;
	printf("Event mask: 0x%0x\n\r", mask);
	for (int i = 0; i < 31 && mask != 0; ++i) {
		if (mask & 1) {
			printf("Event %d: 0x%x, Counts = %u\n\r", i, pmu_s.eventtypes[i], pmu_s.counts[i]);
		}
		mask >>= 1;
	}
	printf("div64: %u\n\r", pmu_s.div64);
	printf("Cycles: 0x%x\n\r", pmu_s.cycles);
	printf("Set for overflow: 0x%x\n\r", pmu_s.overflow);
	printf("PMU enabled: %d\n\r", cs_pmu_is_enabled(pmu));
}

static void prefetch()
{
	printf("%s\n\r", __func__);
	sleep(5);
}

static void compute()
{
	printf("%s\n\r", __func__);
	sleep(5);
}

static void writeback()
{
	printf("%s\n\r", __func__);
	sleep(5);
}

void etm_diag(cs_device_t etm)
{
	cs_etmv4_config_t conf;
	cs_etm_config_init_ex(etm, &conf);
	conf.flags = CS_ETMC_ALL;
	cs_etm_config_get_ex(etm, &conf);
	cs_etm_config_print_ex(etm, &conf);
}

int do_init_etm(cs_device_t dev)
{
    int rc;
    int etm_version = cs_etm_get_version(dev);

    printf("CSDEMO: Initialising ETM/PTM\n %d", etm_version);

    /* ASSERT that this is an etm etc */
    assert(cs_device_has_class(dev, CS_DEVCLASS_SOURCE));

    /* set to a 'clean' state - clears events & values, retains ctrl and ID, ensure programmable */
    if ((rc = cs_etm_clean(dev)) != 0) {
        printf("CSDEMO: Failed to set ETM/PTM into clean state\n");
        return rc;
    }


    /* ETMv4 initialisation */
    cs_etmv4_config_t etm_cf;

    cs_etm_config_init_ex(dev, &etm_cf);

    /* Select external event input 0 to detect PMUBUS_EVENT */;

    etm_cf.flags |= CS_ETMC_RES_SEL;
    etm_cf.extinselr = 0x00000000 | PMUBUS_EVENT;

    /* configure resource selector (rs) 2 for selecting extinput 0 */
    /*21:20 = PAIRINV:INV = b0, GROUP = b0000, SELECT = b1*/
    etm_cf.rsctlr[2] = 1U;
    /* configure resource selector (rs) 4 for selecting counter 0 */
    /*21:20 = PAIRINV:INV = b0, GROUP = 0b0010, SELECT = b1*/
    etm_cf.rsctlr[4] = (0b0010 << 16) | 1U;
    // etm_cf.rsctlr_acc_mask = (1 << 2) | (1 << 4);


    etm_cf.flags |= CS_ETMC_COUNTER;
    etm_cf.counter[0].cntctlr = 1U << 16U /*self reload mode*/ | 2U /*selects resource selector 2*/;
    etm_cf.counter[0].cntrldvr = 3U; /* 4 events*/
    etm_cf.counter[0].cntvr = 3U; /* 4 events*/
    etm_cf.counter_acc_mask = 1;


    etm_cf.flags |= CS_ETMC_EVENTSELECT;
    /* configure external output (eo) 0 to fire when rs2 signals */
    etm_cf.eventctlr0r = 0x00000004;
    /* ATB trigger enable */
    etm_cf.eventctlr1r |= 0x001;

    cs_etm_config_put_ex(dev, &etm_cf);

    cs_etm_disable_programming(dev); /* This brings an ETM to the action! */
    return 0;
}

void do_init_cti_a53(cs_device_t dev)
{

	cs_cti_reset(dev);
	cs_cti_set_trigin_channels(dev, 4, 1 << 0);
	cs_cti_set_trigin_channels(dev, 5, 1 << 1);
	// cs_cti_set_trigout_channels(dev, 0, 1 << 1); /*halt on compute*/
	cs_cti_set_trigin_channels(dev, 6, 1 << 2);
	cs_cti_set_global_channels(dev, 0b0111);
	//cs_cti_set_trigout_channels(dev, 2, 1 << 0);
	cs_cti_enable(dev);
}

void do_init_cti_soc(cs_device_t dev)
{

	cs_cti_reset(dev);
	cs_cti_set_global_channels(dev, 0b0111);
	cs_cti_set_trigout_channels(dev, 0, 1 << 0);
	cs_cti_set_trigout_channels(dev, 1, 1 << 1);
	cs_cti_set_trigout_channels(dev, 2, 1 << 2);

	// cs_cti_set_active_channel(dev, 1);
	// cs_cti_set_active_channel(dev, 2);
	cs_cti_enable(dev);
}

int configure_coresight(cs_device_t etm0, cs_device_t cti_a53_0, cs_device_t cti_soc1)
{
    do_init_cti_a53(cti_a53_0);
    do_init_cti_soc(cti_soc1);
	int err = do_init_etm(etm0);
     if (err) {
     	printf("etm configuration failed %d\n\r", err);
    }
    return 0;
}

int main()
{
    init_platform();
    cs_init();

	printf("=================Configure===================\n\r\n\r");

    // ftm outs is at cti_soc1
    // https://www.xilinx.com/html_docs/xilinx2017_4/SDK_Doc/SDK_references/reference_cross-trigerring-zynqmp.html

    cs_device_t etm0 = cs_device_register(0xFEC40000);
    cs_device_t cti_soc1 = cs_device_register(0xFE9A0000);
    cs_device_t cti_a53_0 = cs_device_register(0xFEC20000);
	cs_device_t pmu0 = cs_device_register(0xFEC30000);
	//cs_device_t ftm = cs_device_register(0xFE9D0000);


    configure_coresight(etm0, cti_a53_0, cti_soc1);

    Xil_Out32(0xA0000000, 0);
    unsigned int pl_counter = Xil_In32(0xA0000000);
    printf("PL: %u\n\r", pl_counter << 2);


	cs_pmu_t pmu_s;
	cs_pmu_read_status(pmu0, 0xFF ^ CS_PMU_DISABLE, &pmu_s);
	pmu_s.eventtypes[0] = 0x17;
	pmu_s.mask = 1;
	cs_pmu_bus_export(pmu0, 1);
	cs_pmu_write_status(pmu0, CS_PMU_ENABLE | CS_PMU_EVENTTYPES, &pmu_s);
    //sleep(5);
    printf("=================Start!=======================\n\r\n\r");

    do_mem_accesses();

	cs_pmu_bus_export(pmu0, 0);
    cs_device_write_only(pmu0, CS_PMCNTENCLR, 1);

    pl_counter = Xil_In32(0xA0000000);
  	uint32_t pmu_val = cs_device_read(pmu0, CS_PMEVCNTR32(0));
	printf("PL: %u\tPMU:%u\n\r", pl_counter << 2, pmu_val);

	cs_shutdown();
	cleanup_platform();
    return 0;
}
