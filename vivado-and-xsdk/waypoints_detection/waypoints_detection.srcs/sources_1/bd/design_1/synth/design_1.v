//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
//Date        : Mon Apr 23 16:54:10 2018
//Host        : matejjoe running 64-bit Debian GNU/Linux 9.2 (stretch)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=5,numReposBlks=5,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=2,numPkgbdBlks=0,bdsource=USER,da_zynq_ultra_ps_e_cnt=2,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (GPIO_DIP_SW0,
    GPIO_DIP_SW1,
    GPIO_DIP_SW7,
    GPIO_LED0,
    GPIO_LED1,
    GPIO_LED2,
    GPIO_LED3,
    GPIO_LED4,
    GPIO_LED5,
    GPIO_LED7);
  input GPIO_DIP_SW0;
  input GPIO_DIP_SW1;
  input GPIO_DIP_SW7;
  output GPIO_LED0;
  output GPIO_LED1;
  output GPIO_LED2;
  output GPIO_LED3;
  output GPIO_LED4;
  output GPIO_LED5;
  output GPIO_LED7;

  wire GPIO_DIP_SW0_1;
  wire GPIO_DIP_SW1_1;
  wire GPIO_DIP_SW7_1;
  wire count_interrupts_0_bit0;
  wire count_interrupts_0_bit1;
  wire count_interrupts_0_bit2;
  wire count_interrupts_1_bit0;
  wire count_interrupts_1_bit1;
  wire count_interrupts_1_bit2;
  wire [0:0]util_vector_logic_0_Res;
  wire [0:0]util_vector_logic_1_Res;
  wire zynq_ultra_ps_e_0_pl_clk0;
  wire zynq_ultra_ps_e_0_pl_resetn0;
  wire zynq_ultra_ps_e_0_ps_pl_trigger_1;
  wire zynq_ultra_ps_e_0_ps_pl_trigger_2;

  assign GPIO_DIP_SW0_1 = GPIO_DIP_SW0;
  assign GPIO_DIP_SW1_1 = GPIO_DIP_SW1;
  assign GPIO_DIP_SW7_1 = GPIO_DIP_SW7;
  assign GPIO_LED0 = count_interrupts_0_bit0;
  assign GPIO_LED1 = count_interrupts_0_bit1;
  assign GPIO_LED2 = count_interrupts_0_bit2;
  assign GPIO_LED3 = count_interrupts_1_bit0;
  assign GPIO_LED4 = count_interrupts_1_bit1;
  assign GPIO_LED5 = count_interrupts_1_bit2;
  assign GPIO_LED7 = GPIO_DIP_SW7_1;
  design_1_count_interrupts_0_1 count_interrupts_0
       (.bit0(count_interrupts_0_bit0),
        .bit1(count_interrupts_0_bit1),
        .bit2(count_interrupts_0_bit2),
        .irq(zynq_ultra_ps_e_0_ps_pl_trigger_1),
        .rst(util_vector_logic_1_Res));
  design_1_count_interrupts_1_1 count_interrupts_1
       (.bit0(count_interrupts_1_bit0),
        .bit1(count_interrupts_1_bit1),
        .bit2(count_interrupts_1_bit2),
        .irq(zynq_ultra_ps_e_0_ps_pl_trigger_2),
        .rst(util_vector_logic_0_Res));
  design_1_util_vector_logic_0_0 util_vector_logic_0
       (.Op1(zynq_ultra_ps_e_0_pl_resetn0),
        .Op2(GPIO_DIP_SW1_1),
        .Res(util_vector_logic_0_Res));
  design_1_util_vector_logic_0_1 util_vector_logic_1
       (.Op1(GPIO_DIP_SW0_1),
        .Op2(zynq_ultra_ps_e_0_pl_resetn0),
        .Res(util_vector_logic_1_Res));
  design_1_zynq_ultra_ps_e_0_0 zynq_ultra_ps_e_0
       (.maxigp0_arready(1'b0),
        .maxigp0_awready(1'b0),
        .maxigp0_bid({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .maxigp0_bresp({1'b0,1'b0}),
        .maxigp0_bvalid(1'b0),
        .maxigp0_rdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .maxigp0_rid({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .maxigp0_rlast(1'b0),
        .maxigp0_rresp({1'b0,1'b0}),
        .maxigp0_rvalid(1'b0),
        .maxigp0_wready(1'b0),
        .maxigp1_arready(1'b0),
        .maxigp1_awready(1'b0),
        .maxigp1_bid({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .maxigp1_bresp({1'b0,1'b0}),
        .maxigp1_bvalid(1'b0),
        .maxigp1_rdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .maxigp1_rid({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .maxigp1_rlast(1'b0),
        .maxigp1_rresp({1'b0,1'b0}),
        .maxigp1_rvalid(1'b0),
        .maxigp1_wready(1'b0),
        .maxihpm0_fpd_aclk(zynq_ultra_ps_e_0_pl_clk0),
        .maxihpm1_fpd_aclk(zynq_ultra_ps_e_0_pl_clk0),
        .pl_clk0(zynq_ultra_ps_e_0_pl_clk0),
        .pl_ps_irq0(1'b0),
        .pl_ps_trigack_0(1'b0),
        .pl_ps_trigack_1(util_vector_logic_1_Res),
        .pl_ps_trigack_2(util_vector_logic_0_Res),
        .pl_ps_trigack_3(1'b0),
        .pl_resetn0(zynq_ultra_ps_e_0_pl_resetn0),
        .ps_pl_trigger_1(zynq_ultra_ps_e_0_ps_pl_trigger_1),
        .ps_pl_trigger_2(zynq_ultra_ps_e_0_ps_pl_trigger_2));
endmodule
