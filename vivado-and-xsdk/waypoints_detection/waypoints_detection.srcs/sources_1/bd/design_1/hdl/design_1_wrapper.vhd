--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
--Date        : Thu May 10 19:58:40 2018
--Host        : matejjoe running 64-bit Debian GNU/Linux 9.2 (stretch)
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    GPIO_DIP_SW0 : in STD_LOGIC;
    GPIO_DIP_SW1 : in STD_LOGIC;
    GPIO_DIP_SW2 : in STD_LOGIC;
    GPIO_DIP_SW3 : in STD_LOGIC;
    GPIO_DIP_SW4 : in STD_LOGIC;
    GPIO_LED0 : out STD_LOGIC;
    GPIO_LED1 : out STD_LOGIC;
    GPIO_LED2 : out STD_LOGIC;
    GPIO_LED3 : out STD_LOGIC;
    GPIO_LED7 : out STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    GPIO_LED0 : out STD_LOGIC;
    GPIO_LED1 : out STD_LOGIC;
    GPIO_LED2 : out STD_LOGIC;
    GPIO_DIP_SW1 : in STD_LOGIC;
    GPIO_DIP_SW2 : in STD_LOGIC;
    GPIO_LED3 : out STD_LOGIC;
    GPIO_DIP_SW3 : in STD_LOGIC;
    GPIO_DIP_SW0 : in STD_LOGIC;
    GPIO_LED7 : out STD_LOGIC;
    GPIO_DIP_SW4 : in STD_LOGIC
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      GPIO_DIP_SW0 => GPIO_DIP_SW0,
      GPIO_DIP_SW1 => GPIO_DIP_SW1,
      GPIO_DIP_SW2 => GPIO_DIP_SW2,
      GPIO_DIP_SW3 => GPIO_DIP_SW3,
      GPIO_DIP_SW4 => GPIO_DIP_SW4,
      GPIO_LED0 => GPIO_LED0,
      GPIO_LED1 => GPIO_LED1,
      GPIO_LED2 => GPIO_LED2,
      GPIO_LED3 => GPIO_LED3,
      GPIO_LED7 => GPIO_LED7
    );
end STRUCTURE;
