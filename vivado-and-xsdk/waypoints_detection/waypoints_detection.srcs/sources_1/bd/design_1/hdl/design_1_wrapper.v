//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
//Date        : Mon Apr 23 16:54:11 2018
//Host        : matejjoe running 64-bit Debian GNU/Linux 9.2 (stretch)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (GPIO_DIP_SW0,
    GPIO_DIP_SW1,
    GPIO_DIP_SW7,
    GPIO_LED0,
    GPIO_LED1,
    GPIO_LED2,
    GPIO_LED3,
    GPIO_LED4,
    GPIO_LED5,
    GPIO_LED7);
  input GPIO_DIP_SW0;
  input GPIO_DIP_SW1;
  input GPIO_DIP_SW7;
  output GPIO_LED0;
  output GPIO_LED1;
  output GPIO_LED2;
  output GPIO_LED3;
  output GPIO_LED4;
  output GPIO_LED5;
  output GPIO_LED7;

  wire GPIO_DIP_SW0;
  wire GPIO_DIP_SW1;
  wire GPIO_DIP_SW7;
  wire GPIO_LED0;
  wire GPIO_LED1;
  wire GPIO_LED2;
  wire GPIO_LED3;
  wire GPIO_LED4;
  wire GPIO_LED5;
  wire GPIO_LED7;

  design_1 design_1_i
       (.GPIO_DIP_SW0(GPIO_DIP_SW0),
        .GPIO_DIP_SW1(GPIO_DIP_SW1),
        .GPIO_DIP_SW7(GPIO_DIP_SW7),
        .GPIO_LED0(GPIO_LED0),
        .GPIO_LED1(GPIO_LED1),
        .GPIO_LED2(GPIO_LED2),
        .GPIO_LED3(GPIO_LED3),
        .GPIO_LED4(GPIO_LED4),
        .GPIO_LED5(GPIO_LED5),
        .GPIO_LED7(GPIO_LED7));
endmodule
