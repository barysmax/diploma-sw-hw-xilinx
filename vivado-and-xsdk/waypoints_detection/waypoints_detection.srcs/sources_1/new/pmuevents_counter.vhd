----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/02/2018 09:25:24 PM
-- Design Name: 
-- Module Name: pmuevents_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pmuevents_counter is
    Port ( pmu_trigin : in STD_LOGIC;
           pmu_trigout : out STD_LOGIC;
           counter_gpo : out STD_LOGIC_VECTOR (31 downto 0));
end pmuevents_counter;

architecture Behavioral of pmuevents_counter is
signal count : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
begin
    process (pmu_trigin) begin
        if rising_edge(pmu_trigin) then
            count <= count + 1;
        end if;
    end process;
    pmu_trigout <= pmu_trigin; 
    counter_gpo <= count;
end Behavioral;
