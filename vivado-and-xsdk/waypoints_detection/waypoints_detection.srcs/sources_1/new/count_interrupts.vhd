----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/23/2018 03:11:44 PM
-- Design Name: 
-- Module Name: count_interrupts - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity count_interrupts is
    Port ( irq : in STD_LOGIC;
           rst : in STD_LOGIC;
           bit0 : out STD_LOGIC;
           bit1 : out STD_LOGIC;
           bit2 : out STD_LOGIC );
end count_interrupts;

architecture Behavioral of count_interrupts is
signal count : STD_LOGIC_VECTOR(2 downto 0) := "000";
begin
    process(irq, rst) begin
        if (rst = '1') then
            count <= "000";
        elsif (rising_edge(irq) and count /= "111") then
            count <= count + 1;
        end if;      
    end process;
    bit0 <= count(0);
    bit1 <= count(1);
    bit2 <= count(2);
end Behavioral;
