----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/30/2018 02:17:49 PM
-- Design Name: 
-- Module Name: collaps_vect - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity collaps_vect is
    Port ( bit0 : in STD_LOGIC;
           bit1 : in STD_LOGIC;
           bit2 : in STD_LOGIC;
           bit3 : in STD_LOGIC;
           vect : out STD_LOGIC_VECTOR (3 downto 0) 
           );
end collaps_vect;

architecture Behavioral of collaps_vect is

begin

    vect(0) <= bit0; 
    vect(1) <= bit1; 
    vect(2) <= bit2; 
    vect(3) <= bit3;
    
end Behavioral;
