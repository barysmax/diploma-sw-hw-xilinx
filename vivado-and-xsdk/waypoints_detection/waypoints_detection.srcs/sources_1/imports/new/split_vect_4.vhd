----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/21/2018 11:32:31 AM
-- Design Name: 
-- Module Name: split_vect_4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity split_vect_4 is
    Port ( vect : in STD_LOGIC_VECTOR (3 downto 0);
           bit0 : out STD_LOGIC;
           bit1 : out STD_LOGIC;
           bit2 : out STD_LOGIC;
           bit3 : out STD_LOGIC);
end split_vect_4;

architecture Behavioral of split_vect_4 is
   
begin
    bit0 <= vect(0);
    bit1 <= vect(1);
    bit2 <= vect(2);
    bit3 <= vect(3);

end Behavioral;
