#!/bin/bash
BUILDROOT_TAG="2017.11-rc2"
JAILHOUSE_COMMIT="be5f8b9"
LINUX_BRANCH="queues/jailhouse"
JAILHOUSE_PATCH="../jailhouse_overlay_compatibility.patch"

cd ./buildroot
git checkout on-tag-$BUILDROOT_TAG

cd ../jailhouse
git checkout on-commit-$JAILHOUSE_COMMIT
if ! patch -R -p1 -s -f --dry-run < $JAILHOUSE_PATCH ; then
    patch -p1 < $JAILHOUSE_PATCH
fi

cd ../linux-xlnx
git checkout $LINUX_BRANCH

