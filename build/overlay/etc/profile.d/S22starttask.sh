#!/bin/bash
modprobe jailhouse
jailhouse enable configs/zynqmp-zcu102.cell
jailhouse cell create configs/zynqmp-zcu102-gic-demo.cell
jailhouse cell list
jailhouse cell load 1 inmates/hyp-demo.bin
jailhouse cell list
jailhouse cell start 1
jailhouse cell list
sleep 5
jailhouse disable
